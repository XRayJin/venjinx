package de.venjinx.util;

public class Materials {
    public static final String UNSHADED = "Common/MatDefs/Misc/Unshaded.j3md";

    public static final String SKY = "Common/MatDefs/Misc/Sky.j3md ";

    public static final String TERRAIN = "Common/MatDefs/Terrain/Terrain.j3md";

    public static final String HEIGHT_TERRAIN = "Common/MatDefs/Terrain/HeightBasedTerrain.j3md";

    public static final String LIGHTING = "Common/MatDefs/Light/Lighting.j3md";

    public static final String TERRAIN_LIGHTING = "Common/MatDefs/Terrain/TerrainLighting.j3md";

    public static final String REFLECTION = "Common/MatDefs/Light/Reflection.j3md";

    public static final String NORMALS = "Common/MatDefs/Misc/ShowNormals.j3md";
}