package de.venjinx.util;

import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.input.controls.MouseButtonTrigger;

public class Keys {

    // Input trigger identifications
    public static final String EXIT_GAME = "Exit game";

    public static final String CAM_LOOK_UP = "Camera look up";
    public static final String CAM_LOOK_LEFT = "Camera look left";
    public static final String CAM_LOOK_DOWN = "Camera look down";
    public static final String CAM_LOOK_RIGHT = "Camera look right";
    public static final String CAM_ZOOM_IN = "Camera zoom in";
    public static final String CAM_ZOOM_OUT = "Camera zoom out";
    public static final String CAM_MOVE_UP = "Camera up";
    public static final String CAM_MOVE_DOWN = "Camera down";
    public static final String CAM_MOVE_LEFT = "Camera left";
    public static final String CAM_MOVE_RIGHT = "Camera right";
    public static final String CAM_MOVE_FORWARD = "Camera forward";
    public static final String CAM_MOVE_BACKWARD = "Camera backward";
    public static final String CAM_RISE = "Rise camera";
    public static final String CAM_LOWER = "Lower camera";
    public static final String CAM_DRAG_ROTATE = "Rotate camera";
    public static final String CAM_INVERTY = "InvertY";

    public static final String DEBUG_CAM_ACTIVATE = "Activate debug camera";

    public static final String SHOW_COORD_GRID = "Show coordination grid";
    public static final String SHOW_NORMALS = "Show all vertice normals";
    public static final String SHOW_WIREFRAME = "Show wireframe";

    public static final String SELECT_NODE = "Select node";
    public static final String SHOW_CONTEXT_MENU = "Show context menu";

    // Keyboard triggers
    public static final KeyTrigger KEY_A = new KeyTrigger(KeyInput.KEY_A);
    public static final KeyTrigger KEY_D = new KeyTrigger(KeyInput.KEY_D);
    public static final KeyTrigger KEY_W = new KeyTrigger(KeyInput.KEY_W);
    public static final KeyTrigger KEY_S = new KeyTrigger(KeyInput.KEY_S);
    public static final KeyTrigger KEY_SPACE = new KeyTrigger(KeyInput.KEY_SPACE);
    public static final KeyTrigger KEY_LSHIFT = new KeyTrigger(KeyInput.KEY_LSHIFT);
    public static final KeyTrigger KEY_LCONTROL = new KeyTrigger(KeyInput.KEY_LCONTROL);
    public static final KeyTrigger KEY_G = new KeyTrigger(KeyInput.KEY_G);
    public static final KeyTrigger KEY_1 = new KeyTrigger(KeyInput.KEY_1);
    public static final KeyTrigger KEY_2 = new KeyTrigger(KeyInput.KEY_2);
    public static final KeyTrigger KEY_ESCAPE = new KeyTrigger(KeyInput.KEY_ESCAPE);

    // Mouse buttons triggers
    public static final MouseButtonTrigger L_MOUSE =
            new MouseButtonTrigger(MouseInput.BUTTON_LEFT);
    public static final MouseButtonTrigger R_MOUSE =
            new MouseButtonTrigger(MouseInput.BUTTON_RIGHT);

    // Mouse motion triggers
    public static final MouseAxisTrigger MOUSE_UP =
            new MouseAxisTrigger(MouseInput.AXIS_Y, false);
    public static final MouseAxisTrigger MOUSE_LEFT =
            new MouseAxisTrigger(MouseInput.AXIS_X, true);
    public static final MouseAxisTrigger MOUSE_DOWN =
            new MouseAxisTrigger(MouseInput.AXIS_Y, true);
    public static final MouseAxisTrigger MOUSE_RIGHT =
            new MouseAxisTrigger(MouseInput.AXIS_X, false);
    public static final MouseAxisTrigger WHEEL_UP =
            new MouseAxisTrigger(MouseInput.AXIS_WHEEL, false);
    public static final MouseAxisTrigger WHEEL_DOWN =
            new MouseAxisTrigger(MouseInput.AXIS_WHEEL, true);
}
