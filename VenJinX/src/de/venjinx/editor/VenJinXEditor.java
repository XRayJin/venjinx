/*
 * Copyright (C) 2014 Torge Rothe, VenJinX <http://www.venjinx.de/>
 *
 * This file is part of VenJinX.
 *
 * VenJinX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this VenJinX.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von VenJinX.
 *
 * VenJinX ist Freie Software: Sie können es unter den Bedingungen
 * der GNU Lesser General Public License, wie von der Free Software
 * Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder
 * neueren veröffentlichten Version, weiterverbreiten und/oder
 * modifizieren.
 *
 * Dieses Programm wird in der Hoffnung, dass es nützlich sein wird,
 * aber OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die
 * implizite Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR
 * EINEN BESTIMMTEN ZWECK. Siehe die GNU Lesser General Public
 * License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU Lesser General Public License
 * zusammen mit VenJinX erhalten haben.
 * Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */
package de.venjinx.editor;

import com.jme3.material.Material;
import com.jme3.math.*;
import com.jme3.renderer.RenderManager;
import de.venjinx.core.HermiteData;
import de.venjinx.jme3.*;
import de.venjinx.jme3.debug.*;
import de.venjinx.jme3.scenegraph.Scenegraph;
import de.venjinx.util.Materials;

public class VenJinXEditor extends VoxelApplication {

    protected DebugMouse mouse;
//    protected DebugState debugState;
    protected VoxelState voxelState;

//    private ViewController viewController;

    private Scenegraph scenegraph = new Scenegraph();

    public VenJinXEditor() {
//        super(new DebugState());
        super();

//        debugState = stateManager.getState(DebugState.class);
        voxelState = stateManager.getState(VoxelState.class);

        rootNode = scenegraph.getRootNode();
    }

    public Scenegraph getScenegraph() {
        return scenegraph;
    }

    @Override
    public void simpleInitApp() {
//        viewController = new ViewController(this);

        // cam.setLocation(new Vector3f(0, 2, 0));
        cam.setLocation(new Vector3f(7, 7, 7));

        cam.setFrustumFar(1000);
        viewPort.setBackgroundColor(new ColorRGBA(0.3f, 0.3f, 0.3f, 1));

        Mouse3D.setApp(this, true);
        Material mat = new Material(assetManager, Materials.UNSHADED);
        mat.setColor("Color", ColorRGBA.Orange);
        Mouse3D.setMaterial(mat);
        rootNode.attachChild(Mouse3D.getPointer());

        /*
         * load data
         */
        String path = "assets/textures/terrain/heightMaps/";
        HermiteData hermiteData =
                                                  new HermiteData(path + "heightMap4x4_02.gif", 4);
                //                                  new HermiteData(path + "heightMap8x8_02.gif", 8);
        //                new HermiteData(path + "heightMap16x16_02.gif", 16);
        //                                  new HermiteData(path + "heightMap32x32_02.gif", 32);
//        new HermiteData(path + "heightMap64x64_01.gif", 32);
//                        new HermiteData(path + "heightMap128x128_02.gif", 64);
        //        new HermiteData(path + "heightMap256x256_01.gif");
        //        new HermiteData(path + "heightMap512x512_02.gif");

        /*
         * load heightMap
         */
        //        Texture heightMapImage =
        //                                 assetManager.loadTexture("textures/terrain/heightMaps/asd.gif");
        // assetManager.loadTexture("textures/terrain/heightMaps/heightMap4x4_01.gif");
        // assetManager.loadTexture("textures/terrain/heightMaps/heightMap8x8_01.gif");
        //                assetManager.loadTexture("textures/terrain/heightMaps/heightMap16x16_03.gif");
        //                assetManager.loadTexture("textures/terrain/heightMaps/heightMap32x32_02.gif");
        //                                 assetManager.loadTexture("textures/terrain/heightMaps/heightMap64x64_01.gif");
        //                                 assetManager.loadTexture("textures/terrain/heightMaps/heightMap128x128_02.gif");
        // assetManager.loadTexture("textures/terrain/heightMaps/heightMap256x256_01.gif");
        // assetManager.loadTexture("textures/terrain/heightMaps/heightMap512x512_02.gif");

        //        ImageBasedHeightMap heightMap;
        //        heightMap = new ImageBasedHeightMap(heightMapImage.getImage());
        //        heightMap.load(false, false);

        //        float[][][] data = hermiteData.getDensityField();
        //        float[][][] data = VoxelState.loadFromHeightMap(heightMap, VoxelMode.LINE_2D);

        /*
         * create terrain material
         */
        mat = new Material(assetManager, Materials.LIGHTING);
        // Material mat = new Material(assetManager, Materials.NORMALS);
        //        mat.setTexture("DiffuseMap",
        //                       assetManager.loadTexture("textures/terrain/ground/grass512x512_01.gif"));
        //
        //        mat.setTexture("NormalMap",
        //                       assetManager.loadTexture("textures/terrain/ground/grass512x512_01_normal.gif"));

        // mat.setBoolean("UseMaterialColors", true);
        // mat.setColor("Diffuse", ColorRGBA.White);

        VoxelObjectNode terrain = new VoxelObjectNode(hermiteData);
        terrain.setName("VoxelTerrain");
        terrain.setMaterial(mat);
        //        terrain.setMode(VoxelMode.LINE_2D);
        //                terrain.setMode(VoxelMode.SURFACE_2D);
        rootNode.attachChild(terrain);
        voxelState.addObject(terrain);

//        CopyOfVoxelObjectNode terrain2 = new CopyOfVoxelObjectNode(hermiteData);
//        terrain2.setName("VoxelTerrain2");
//        terrain2.setMaterial(mat);
        //        terrain2.setMode(VoxelMode.LINE_2D);
        //        terrain2.setMode(VoxelMode.SURFACE_2D);
        //        terrain2.setMode(VoxelMode.PSEUDO_3D_LINE);
//        rootNode.attachChild(terrain2);
//        voxelState.addObject2(terrain2);
        //        terrain2.setLocalTranslation(0, 5, 0);
    }

    public static int loopCount = 0;

    @Override
    public void simpleUpdate(float tpf) {
        loopCount++;
        Mouse3D.getPointer().setLocalTranslation(Mouse3D.getHitPoint());
        secondCounter += getTimer().getTimePerFrame();
        frameCounter++;
    }

    @Override
    public void simpleRender(RenderManager rm) {
    }

    private float secondCounter;
    private int frameCounter;

    public void printFPS() {
        if (secondCounter >= 1.0f) {
            int fps = (int) (frameCounter / secondCounter);
            System.out.println(fps);
            secondCounter = 0.0f;
            frameCounter = 0;
        }
    }

    public static void main(String[] args) {
        VenJinXEditor app = new VenJinXEditor();
        app.start();
    }
}