//package de.venjinx.editor.gui;
//
//import com.jme3.app.Application;
//import com.jme3.niftygui.NiftyJmeDisplay;
//
//import de.lessvoid.nifty.elements.Element;
//import de.lessvoid.nifty.screen.Screen;
//import de.lessvoid.nifty.screen.ScreenController;
//import de.venjinx.editor.gui.debug.DebugView;
//
//public class MainView extends NiftyJmeDisplay {
//
//    ScreenController controller;
//    Screen screen;
//    Element bgLayer;
//    Element windowLayer;
//    Element fgLayer;
//
//    SGView sgView;
//    DebugView debugView;
//
//    public MainView(Application app, ViewController controller) {
//        super(app.getAssetManager(), app.getInputManager(),
//              app.getAudioRenderer(), app.getGuiViewPort());
//
//        nifty.fromXml("interface/editorInterface.xml", controller);
//        //        nifty.setDebugOptionPanelColors(true);
//        nifty.setIgnoreKeyboardEvents(true);
//
//        screen = nifty.getScreen("editorMainScreen");
//
//        bgLayer = screen.findElementById("background");
//        windowLayer = screen.findElementById("windows");
//        fgLayer = screen.findElementById("foreground");
//
//        sgView = new SGView("sgView", "Scenegraph View");
//        sgView.createView(nifty, screen, windowLayer, true);
//
//        //        debugView = new DebugView("debugView", "Debug View");
//        //        debugView.createView(nifty, screen, windowLayer, true);
//
//        nifty.gotoScreen("editorMainScreen");
//    }
//
//    public Element getBackgroundLayer() {
//        return bgLayer;
//    }
//
//    public Element getWindowLayer() {
//        return windowLayer;
//    }
//
//    public Element getForegroundLayer() {
//        return fgLayer;
//    }
//
//    public Screen getScreen(String id) {
//        return nifty.getScreen(id);
//    }
//
//    public Element getElement(String id) {
//        return nifty.getCurrentScreen().findElementById(id);
//    }
//
//    public SGView getSGView() {
//        return sgView;
//    }
//}