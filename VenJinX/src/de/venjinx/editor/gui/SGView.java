//package de.venjinx.editor.gui;
//
//import java.util.HashMap;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
//import com.jme3.scene.Geometry;
//import com.jme3.scene.Node;
//import com.jme3.scene.Spatial;
//
//import de.lessvoid.nifty.Nifty;
//import de.lessvoid.nifty.controls.TreeBox;
//import de.lessvoid.nifty.controls.TreeItem;
//import de.lessvoid.nifty.controls.treebox.builder.TreeBoxBuilder;
//import de.lessvoid.nifty.controls.window.builder.WindowBuilder;
//import de.lessvoid.nifty.elements.Element;
//import de.lessvoid.nifty.screen.Screen;
//import de.venjinx.jme3.scenegraph.*;
//
//public class SGView implements SGListener {
//
//    private String elementID;
//    private String windowTitle;
//
//    private Scenegraph scenegraph;
//    private TreeItem<String> rootItem;
//    private TreeBox<String> treeBox;
//    private HashMap<Spatial, TreeItem<String>> treeItems = new HashMap<>();
//
//    public SGView(String id, String title) {
//        elementID = id;
//        windowTitle = title;
//    }
//
//    public void setScenegraph(Scenegraph sg) {
//        if (scenegraph == sg)
//            return;
//
//        if (scenegraph != null)
//            scenegraph.removeListener(this);
//
//        scenegraph = sg;
//
//        if (scenegraph != null) {
//            scenegraph.addScenegraphListener(this);
//
//            TreeItem<String> sceneRootItem = createTreeItem(scenegraph.getRootNode());
//
//            rootItem = new TreeItem<String>();
//            rootItem.setExpanded(true);
//            rootItem.addTreeItem(sceneRootItem);
//            treeBox.setTree(rootItem);
//        }
//    }
//
//    @SuppressWarnings("unchecked")
//    public Element createView(Nifty nifty, Screen screen, Element parent, boolean asWindow) {
//        Element e;
//        if (asWindow) {
//            WindowBuilder wb = new WindowBuilder(elementID, windowTitle) {{
//                backgroundColor("#8888");
//                width("300px");
//                height("300");
//                x("500");
//                closeable(false);
//                minimized(true);
//                control(new TreeBoxBuilder("treeView") {{
//                    displayItems(10);
//                }});
//            }};
//            e = wb.build(nifty, screen, parent);
//            treeBox = screen.findNiftyControl("treeView", TreeBox.class);
//        } else {
//            TreeBoxBuilder tb = new TreeBoxBuilder(elementID) {{
//                width("300px");
//                displayItems(10);
//            }};
//            e = tb.build(nifty, screen, parent);
//            treeBox = e.getNiftyControl(TreeBox.class);
//        }
//        return e;
//    }
//
//    public TreeBox<String> getTreeBox() {
//        if (treeBox == null) {
//            String msg = "SGView has not been created yet. Returning null.";
//            Logger.getLogger(getClass().getName()).log(Level.WARNING, msg);
//        }
//        return treeBox;
//    }
//
//    public HashMap<Spatial, TreeItem<String>> getTreeItems() {
//        return treeItems;
//    }
//
//    public void selectItem(Geometry geom) {
//        treeBox.select(treeItems.get(geom));
//    }
//
//    @Override
//    public void onGraphChanged(GraphChangedEvent e) {
//        TreeItem<String> parent = treeItems.get(e.getParent());
//        TreeItem<String> child = treeItems.get(e.getChild());
//        String value = "";
//        switch (e.getType()) {
//            case ATTACHED:
//                if (child == null)
//                    child = createTreeItem(e.getChild());
//
//                treeBox.insert(parent, child);
//                value += "(" + (e.getParent().getChildren().size() + 1) + ") ";
//                break;
//            case DETACHED:
//                treeBox.remove(child);
//                value += "(" + (e.getParent().getChildren().size() - 1) + ") ";
//                break;
//        }
//        value += e.getParent().toString();
//        parent.setValue(value);
//        treeBox.refresh();
//    }
//
//    private TreeItem<String> createTreeItem(Spatial spatial) {
//        TreeItem<String> newItem, tmpItem;
//        String value = "";
//        if (spatial instanceof Node)
//            value += "(" + ((Node) spatial).getChildren().size() + ") ";
//        value += spatial.toString();
//
//        newItem = new TreeItem<>(value);
//        treeItems.put(spatial, newItem);
//
//        if (spatial instanceof Node) {
//            Node sNode = (Node) spatial;
//
//            for (Spatial sp : sNode.getChildren()) {
//                tmpItem = treeItems.get(sp);
//                if (tmpItem == null)
//                    tmpItem = createTreeItem(sp);
//                newItem.addTreeItem(tmpItem);
//            }
//        }
//        return newItem;
//    }
//}