//package de.venjinx.editor.gui;
//
//import java.util.HashMap;
//
//import org.bushe.swing.event.EventTopicSubscriber;
//
//import com.jme3.app.state.AbstractAppState;
//import com.jme3.asset.AssetManager;
//import com.jme3.collision.CollisionResult;
//import com.jme3.font.BitmapFont;
//import com.jme3.font.BitmapText;
//import com.jme3.input.InputManager;
//import com.jme3.input.controls.ActionListener;
//import com.jme3.input.controls.AnalogListener;
//import com.jme3.material.Material;
//import com.jme3.math.ColorRGBA;
//import com.jme3.math.Vector3f;
//import com.jme3.renderer.Camera;
//import com.jme3.scene.Geometry;
//import com.jme3.scene.Node;
//import com.jme3.scene.Spatial;
//import com.jme3.scene.control.BillboardControl;
//import com.jme3.scene.debug.Arrow;
//import com.jme3.scene.debug.Grid;
//
//import de.lessvoid.nifty.Nifty;
//import de.lessvoid.nifty.NiftyEventSubscriber;
//import de.lessvoid.nifty.controls.CheckBoxStateChangedEvent;
//import de.lessvoid.nifty.controls.DropDownSelectionChangedEvent;
//import de.lessvoid.nifty.controls.MenuItemActivatedEvent;
//import de.lessvoid.nifty.controls.SliderChangedEvent;
//import de.lessvoid.nifty.screen.Screen;
//import de.lessvoid.nifty.screen.ScreenController;
//import de.venjinx.editor.VenJinXEditor;
//import de.venjinx.editor.gui.debug.DebugView;
//import de.venjinx.jme3.debug.Mouse3D;
//import de.venjinx.jme3.scenegraph.SGNode;
//import de.venjinx.util.Keys;
//import de.venjinx.util.Materials;
//
//public class ViewController implements ActionListener,
//AnalogListener,
//ScreenController,
//EventTopicSubscriber<MenuItemActivatedEvent<String>> {
//
//    private static String[] inputKeys = { Keys.SELECT_NODE };
//
//    private VenJinXEditor editor;
//    protected AssetManager am;
//    protected InputManager im;
//    private MainView view;
//    private Nifty nifty;
//    private Screen screen;
//
//    private BillboardControl bc = new BillboardControl();
//    private BitmapFont guiFont;
//    private SGView sgWindow;
//    private DebugView debugWindow;
//    private Camera cam;
//
//    private HashMap<String, AbstractAppState> states = new HashMap<>();
//    private SGNode debugNode = new SGNode("Debug");
//    private SGNode coordNode;
//
//    // Flags
//    private boolean showGrid = true;
//    private boolean showWire = false;
//
//    public ViewController(VenJinXEditor app) {
//        editor = app;
//        am = editor.getAssetManager();
//        im = editor.getInputManager();
//
//        view = new MainView(editor, this);
//
//        sgWindow = view.sgView;
//        sgWindow.setScenegraph(editor.getScenegraph());
//
//        //        debugWindow = view.debugView;
//
//        editor.getGuiViewPort().addProcessor(view);
//        editor.getInputManager().addMapping(Keys.SELECT_NODE, Keys.L_MOUSE);
//        editor.getInputManager().addListener(this, inputKeys);
//
//        guiFont = editor.getAssetManager().loadFont("Interface/Fonts/Default.fnt");
//
//        cam = editor.getCamera();
//
//        //        coordNode = createCoordinationNode(256, 1);
//        //        debugNode.attachChild(coordNode);
//        //        debugNode.updateModelBound();
//    }
//
//    @Override
//    public void onAction(String name, boolean isPressed, float tpf) {
//
//        if (name.equals(Keys.SELECT_NODE) && isPressed) {
//            CollisionResult r = Mouse3D.getHitResult();
//            if (r != null)
//                sgWindow.selectItem(r.getGeometry());
//        }
//
//        if (name.equals(Keys.SHOW_COORD_GRID) && isPressed)
//            debugWindow.setGridChecked(!showGrid);
//
//        if (name.equals(Keys.SHOW_WIREFRAME) && isPressed)
//            debugWindow.setWireframeChecked(!showWire);
//    }
//
//    @Override
//    public void onAnalog(String name, float value, float tpf) {
//    }
//
//    @Override
//    public void onEvent(String id, MenuItemActivatedEvent<String> event) {
//
//    }
//
//    @NiftyEventSubscriber(pattern = ".*CheckBox")
//    public void onCheckBoxStateChanged(String id, CheckBoxStateChangedEvent event) {
//        //        boolean checked = event.isChecked();
//        //        switch (id) {
//        //            case "showGridCheckBox":
//        //                showGrid(checked);
//        //                break;
//        //            case "showWireframeCheckBox":
//        //                showWireFrame(editor.getRootNode(), checked);
//        //                break;
//        //            case "pauseStateCheckBox":
//        //                states.get(debugWindow.getSelectedState()).setEnabled(checked);
//        //                break;
//        //        }
//    }
//
//    @NiftyEventSubscriber(pattern = ".*Slider")
//    public void onSliderStateChanged(String id, SliderChangedEvent event) {
//        //        System.out.println("slider change");
//        //        switch (id) {
//        //            case "cameraSpeedSlider":
//        //                editor.getFlyByCamera().setMoveSpeed(event.getValue());
//        //                break;
//        //            case "viewDistanceSlider":
//        //                cam.setFrustumFar(event.getValue());
//        //                cam.update();
//        //                //                if (voxelState != null) {
//        //                //                    voxelState.getCalcBound().setRadius(cam.getFrustumFar() * .9f);
//        //                //                    Sphere s = new Sphere(32, 32, voxelState.getCalcBound().getRadius());
//        //                //                    s.setMode(Mode.Lines);
//        //                //                    worldCalcBoundGeom.setMesh(s);
//        //                //                }
//        //                break;
//        //        }
//    }
//
//    @NiftyEventSubscriber(pattern = ".*DropDown")
//    public void onDropDownSelectionChanged(String id,
//                                           DropDownSelectionChangedEvent<String> event) {
//        //        switch (id) {
//        //            case "stateSelectDropDown":
//        //                AbstractAppState state = states.get(debugWindow.getSelectedState());
//        //                debugWindow.setStatePausedChecked(state.isEnabled());
//        //                break;
//        //        }
//    }
//
//    public void showGrid(boolean enabled) {
//        showGrid = enabled;
//        if (showGrid)
//            debugNode.attachChild(coordNode);
//        else debugNode.detachChild(coordNode);
//    }
//
//    public void showWireFrame(Node node, boolean show) {
//        showWire = show;
//        Geometry geom;
//        for (Spatial s : node.getChildren())
//            if (s instanceof Node) {
//                if (s != debugNode)
//                    showWireFrame((Node) s, show);
//            } else
//                if (s instanceof Geometry) {
//                    geom = (Geometry) s;
//                    geom.getMaterial().getAdditionalRenderState().setWireframe(showWire);
//                }
//    }
//
//    public void addState(AbstractAppState state) {
//        states.put(state.getClass().getSimpleName(), state);
//        debugWindow.addStateToControl(state.getClass().getSimpleName());
//    }
//
//    @Override
//    public void bind(Nifty nifty, Screen screen) {
//        this.nifty = nifty;
//        this.screen = screen;
//    }
//
//    @Override
//    public void onStartScreen() {
//
//    }
//
//    @Override
//    public void onEndScreen() {
//
//    }
//
//    /**
//     * Creates one arrow for each axis colored in x=red, y=green, z=blue and one
//     * grid plane for each plane colored xz=red, xy=green, yz=blue.
//     *
//     * @author Torge Rothe
//     * @param size
//     *            - int - The size of the arrows and grids
//     * @param segSize
//     *            - int - The segment size of the grids.
//     * @return Node - A node with 3 direction arrows for x, y, z axis and 3
//     *         coordination grid planes for xz, xy, yz planes.
//     */
//    private SGNode createCoordinationNode(int size, int segSize) {
//        float offset = size / 2;
//
//        Material mat;
//
//        Geometry xAxis;
//        Geometry yAxis;
//        Geometry zAxis;
//        Geometry xzGrid;
//        Geometry xyGrid;
//        Geometry yzGrid;
//
//        SGNode coordNode = new SGNode("Coodination Grid");
//        SGNode labelNode = new SGNode("Grid labels");
//        coordNode.attachChild(labelNode);
//
//        // create x-axis
//        Arrow arrowX = new Arrow(new Vector3f(size, 0.0f, 0.0f));
//        xAxis = new Geometry("X-Axis", arrowX);
//        mat = new Material(am, Materials.UNSHADED);
//        mat.setColor("Color", ColorRGBA.Red);
//        xAxis.setMaterial(mat);
//
//        // create xy-grid
//        Grid xyPlane = new Grid(size, size, segSize);
//        xyGrid = new Geometry("XY-Plane", xyPlane);
//        xyGrid.setMaterial(mat);
//        xyGrid.rotateUpTo(new Vector3f(0.0f, 0.0f, 1.0f));
//        xyGrid.setLocalTranslation(new Vector3f(-offset, offset, 0.0f));
//
//        // create y-axis
//        Arrow arrowY = new Arrow(new Vector3f(0.0f, size, 0.0f));
//        yAxis = new Geometry("Y-Axis", arrowY);
//        mat = new Material(am, Materials.UNSHADED);
//        mat.setColor("Color", ColorRGBA.Green);
//        yAxis.setMaterial(mat);
//
//        // create yz-grid
//        Grid yzPlane = new Grid(size, size, segSize);
//        yzGrid = new Geometry("YZ-Plane", yzPlane);
//        yzGrid.setMaterial(mat);
//        yzGrid.rotateUpTo(new Vector3f(1.0f, 0.0f, 0.0f));
//        yzGrid.setLocalTranslation(new Vector3f(0.0f, offset, -offset));
//
//        // create z-axis
//        Arrow arrowZ = new Arrow(new Vector3f(0.0f, 0.0f, size));
//        zAxis = new Geometry("Z-Axis", arrowZ);
//        mat = new Material(am, Materials.UNSHADED);
//        mat.setColor("Color", ColorRGBA.Blue);
//        zAxis.setMaterial(mat);
//
//        // create xz-grid
//        Grid xzPlane = new Grid(size, size, segSize);
//        xzGrid = new Geometry("XZ-Plane", xzPlane);
//        xzGrid.setMaterial(mat);
//        xzGrid.rotateUpTo(new Vector3f(0.0f, 1.0f, 0.0f));
//        xzGrid.setLocalTranslation(new Vector3f(-offset, 0.0f, -offset));
//
//        // attach arrows to coordination node
//        coordNode.attachChild(xAxis);
//        coordNode.attachChild(yAxis);
//        coordNode.attachChild(zAxis);
//
//        // attach grids to coordination node
//        coordNode.attachChild(xyGrid);
//        coordNode.attachChild(xzGrid);
//        coordNode.attachChild(yzGrid);
//
//        Vector3f pos = new Vector3f(0, 0, 0);
//
//        BitmapText vertId = new BitmapText(guiFont);
//        vertId.setSize(.1f);
//        vertId.setText("x0, y0, z0");
//        vertId.setName("Gridlabel - x0, y0, z0");
//        vertId.setLocalTranslation(pos);
//        vertId.addControl(bc.cloneForSpatial(vertId));
//        labelNode.attachChild(vertId);
//
//        for (int x = 1; x < offset; x++) {
//            pos.set(x, 0, 0);
//            vertId = new BitmapText(guiFont);
//            vertId.setSize(.2f);
//            vertId.setText("x" + x);
//            vertId.setName("Gridlabel - x" + x);
//            vertId.setLocalTranslation(pos);
//            vertId.addControl(bc.cloneForSpatial(vertId));
//            labelNode.attachChild(vertId);
//        }
//        for (int y = 1; y < offset; y++) {
//            pos.set(0, y, 0);
//            vertId = new BitmapText(guiFont);
//            vertId.setSize(.2f);
//            vertId.setText("y" + y);
//            vertId.setName("Gridlabel - y" + y);
//            vertId.setLocalTranslation(pos);
//            vertId.addControl(bc.cloneForSpatial(vertId));
//            labelNode.attachChild(vertId);
//        }
//        for (int z = 1; z < offset; z++) {
//            pos.set(0, 0, z);
//            vertId = new BitmapText(guiFont);
//            vertId.setSize(.2f);
//            vertId.setText("z" + z);
//            vertId.setName("Gridlabel - z" + z);
//            vertId.setLocalTranslation(pos);
//            vertId.addControl(bc.cloneForSpatial(vertId));
//            labelNode.attachChild(vertId);
//        }
//        return coordNode;
//    }
//}