//package de.venjinx.editor.gui.debug;
//
//import com.jme3.renderer.Statistics;
//
//import de.lessvoid.nifty.Nifty;
//import de.lessvoid.nifty.controls.CheckBox;
//import de.lessvoid.nifty.controls.DropDown;
//import de.lessvoid.nifty.controls.Slider;
//import de.lessvoid.nifty.controls.treebox.builder.TreeBoxBuilder;
//import de.lessvoid.nifty.controls.window.builder.WindowBuilder;
//import de.lessvoid.nifty.elements.Element;
//import de.lessvoid.nifty.screen.Screen;
//
//
//public class DebugView {
//
//    private String elementID;
//    private String windowTitle;
//
//    private Element tmpElement;
//
//    CheckBox showGridCheckBox;
//    CheckBox showWireframeCheckBox;
//    CheckBox pauseStateCheckBox;
//
//    Slider cameraSpeedSlider;
//    Slider viewDistanceSlider;
//
//    DropDown<String> stateDropDown;
//
//    private Statistics stats;
//    private int[] statsData;
//    private float secondCounter;
//    private int frameCounter;
//
//    public DebugView(String id, String title) {
//        elementID = id;
//        windowTitle = title;
//    }
//
//    @SuppressWarnings("unchecked")
//    public Element createView(Nifty nifty, Screen screen, Element parent, boolean asWindow) {
//        Element e;
//        if (asWindow) {
//            WindowBuilder wb = new WindowBuilder(elementID, windowTitle) {{
//                backgroundColor("#8888");
//                width("300px");
//                height("300");
//                x("500");
//                closeable(false);
//                minimized(false);
//            }};
//            e = wb.build(nifty, screen, parent);
//        } else {
//            TreeBoxBuilder tb = new TreeBoxBuilder(elementID) {{
//                width("300px");
//                displayItems(10);
//            }};
//            e = tb.build(nifty, screen, parent);
//        }
//        return e;
//    }
//
//    public void addStateToControl(String className) {
//        stateDropDown.addItem(className);
//        if (stateDropDown.itemCount() == 1)
//            stateDropDown.selectItemByIndex(0);
//    }
//
//    public String getSelectedState() {
//        return stateDropDown.getSelection();
//    }
//
//    public void setGridChecked(boolean checked) {
//        showGridCheckBox.setChecked(checked);
//    }
//
//    public void setWireframeChecked(boolean checked) {
//        showWireframeCheckBox.setChecked(checked);
//    }
//
//    public void setStatePausedChecked(boolean checked) {
//        pauseStateCheckBox.setChecked(checked);
//    }
//}
