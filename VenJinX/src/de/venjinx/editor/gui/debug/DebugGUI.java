//package de.venjinx.editor.gui.debug;
//
//import java.text.DecimalFormat;
//
//import com.jme3.app.Application;
//import com.jme3.math.Vector3f;
//import com.jme3.niftygui.NiftyJmeDisplay;
//import com.jme3.renderer.Statistics;
//import com.jme3.scene.Geometry;
//import de.lessvoid.nifty.controls.*;
//import de.lessvoid.nifty.elements.Element;
//import de.lessvoid.nifty.elements.render.TextRenderer;
//import de.lessvoid.nifty.screen.Screen;
//import de.lessvoid.nifty.tools.SizeValue;
//import de.venjinx.jme3.debug.Mouse3D;
//
//public class DebugGUI extends NiftyJmeDisplay {
//
//    public static final String MENU_SHOW_BOUND = "menu_show_bound";
//    public static final String MENU_SHOW_CHUNK_BOUNDS = "menu_show_chunk_bounds";
//    public static final String MENU_SHOW_CHUNK_LODS = "menu_show_chunk_lods";
//    public static final String MENU_SHOW_NORMALS = "menu_show_normals";
//    public static final String MENU_SHOW_WIREFRAME = "menu_show_wireframe";
//    public static final String MENU_SHOW_VOXEL_GRID = "menu_show_voxel_grid";
//
//    private Application application;
//    private DebugState debugState;
//
//    private Screen currentScreen;
//    private Element tmpElement;
//
//    private Element geometryPopupMenu;
//    private Menu<String> geometryMenu;
//    private Element voxelPopupMenu;
//    private Menu<String> voxelMenu;
//
//    CheckBox showGridCheckBox;
//    CheckBox showWireframeCheckBox;
//    CheckBox pauseStateCheckBox;
//
//    Slider cameraSpeedSlider;
//    Slider viewDistanceSlider;
//
//    DropDown<String> stateDropDown;
//
//    private Statistics stats;
//    private int[] statsData;
//    private float secondCounter;
//    private int frameCounter;
//
//    public DebugGUI(Application app, DebugState controller) {
//        super(app.getAssetManager(), app.getInputManager(),
//              app.getAudioRenderer(), app.getGuiViewPort());
//        application = app;
//        debugState = controller;
//        stats = application.getRenderer().getStatistics();
//        stats.setEnabled(true);
//        statsData = new int[stats.getLabels().length];
//
//        // LwjglBatchRenderBackendFactory.create();
//        // BatchRenderDevice rd = new BatchRenderDevice(lwjglb);
//        //
//        // BatchRenderDevice rd = new BatchRenderDevice(renderBackend,
//        // renderConfig);
//
//        application.getGuiViewPort().addProcessor(this);
//    }
//
//    public void showContextMenu(boolean show, Geometry geometry) {
//        if (show) {
//            if (geometry.getName().contains("Voxel")) {
//                nifty.showPopup(currentScreen, voxelPopupMenu.getId(), null);
////                nifty.subscribe(currentScreen, voxelMenu.getId(),
////                                MenuItemActivatedEvent.class, debugState);
//            } else {
//                nifty.showPopup(currentScreen, geometryPopupMenu.getId(), null);
////                nifty.subscribe(currentScreen, geometryMenu.getId(),
////                                MenuItemActivatedEvent.class, debugState);
//            }
//        } else
//            if (geometry.getName().contains("Voxel"))
//                nifty.closePopup(voxelPopupMenu.getId());
//            else
//                nifty.closePopup(geometryPopupMenu.getId());
//    }
//
//    public void addStateToControl(String className) {
//        stateDropDown.addItem(className);
//        if (stateDropDown.itemCount() == 1)
//            stateDropDown.selectItemByIndex(0);
//    }
//
//    public void updateInterface() {
//        int x = (int) application.getCamera().getLocation().x;
//        int y = (int) application.getCamera().getLocation().y;
//        int z = (int) application.getCamera().getLocation().z;
//
//        //        Vector3f mp = debugState.getMouse().getHitPoint();
//        Vector3f mp = Mouse3D.getHitPoint();
//        DecimalFormat df = new DecimalFormat("0.0000");
//        stats.getData(statsData);
//
//        secondCounter += application.getTimer().getTimePerFrame();
//        frameCounter++;
//
//        tmpElement = currentScreen.findElementById("mouse3D");
//        tmpElement.getRenderer(TextRenderer.class).setText("Mouse3D(" + df.format(mp.x)
//                                                           + ", " + df.format(mp.y)
//                                                           + ", " + df.format(mp.z)
//                                                           + ")");
//
//        if (secondCounter >= 1.0f) {
//            int fps = (int) (frameCounter / secondCounter);
//            tmpElement = currentScreen.findElementById("fps");
//            tmpElement.getRenderer(TextRenderer.class).setText("FPS: " + fps);
//            secondCounter = 0.0f;
//            frameCounter = 0;
//        }
//
//        tmpElement = currentScreen.findElementById("camPos");
//        tmpElement.getRenderer(TextRenderer.class).setText("Camera: ("
//                + x + ", " + y + ", " + z
//                + ")");
//
//        tmpElement = currentScreen.findElementById("vertCount");
//        tmpElement.getRenderer(TextRenderer.class).setText("Vertices: " + statsData[0]);
//
//        tmpElement = currentScreen.findElementById("triCount");
//        tmpElement.getRenderer(TextRenderer.class).setText("Triangles: " + statsData[1]);
//
//        tmpElement = currentScreen.findElementById("uniCount");
//        tmpElement.getRenderer(TextRenderer.class).setText("Uniforms: " + statsData[2]);
//
//        tmpElement = currentScreen.findElementById("objCount");
//        tmpElement.getRenderer(TextRenderer.class).setText("Objects: " + statsData[3]);
//
//        tmpElement = currentScreen.findElementById("cameraSpeedLabel");
//        tmpElement.getRenderer(TextRenderer.class)
//        .setText("Camera speed = " + debugState.getDebugCamera().getMoveSpeed());
//
//        tmpElement = currentScreen.findElementById("viewDistanceLabel");
//        tmpElement.getRenderer(TextRenderer.class)
//        .setText("View distance = " + application.getCamera().getFrustumFar());
//
//        // shadersLabel.setText("Shaders            :              " + data[4]
//        // + "                   " + data[5] + "               "
//        // + data[6]);
//        // texturesLabel.setText("Textures           :              " + data[7]
//        // + "                   " + data[8] + "               "
//        // + data[9]);
//        // fBufferLabel.setText("Framebuffers   :              " + data[10]
//        // + "                   " + data[11] + "               "
//        // + data[12]);
//    }
//
//    @SuppressWarnings("unchecked")
//    public void init(Screen screen) {
//        currentScreen = screen;
//
//        showGridCheckBox = screen.findNiftyControl("showGridCheckBox", CheckBox.class);
//        showWireframeCheckBox = screen.findNiftyControl("showWireframeCheckBox",
//                                                        CheckBox.class);
//        pauseStateCheckBox = screen.findNiftyControl("pauseStateCheckBox", CheckBox.class);
//
//        cameraSpeedSlider = screen.findNiftyControl("cameraSpeedSlider", Slider.class);
//        viewDistanceSlider = screen.findNiftyControl("viewDistanceSlider", Slider.class);
//
//        stateDropDown = screen.findNiftyControl("stateSelectDropDown",
//                                                DropDown.class);
//
//        geometryPopupMenu = nifty.createPopup("niftyPopupMenu");
//        voxelPopupMenu = nifty.createPopup("niftyPopupMenu");
//
//        createGeometryMenu();
//        createVoxelMenu();
//    }
//
//    @SuppressWarnings("unchecked")
//    private void createGeometryMenu() {
//        geometryMenu = geometryPopupMenu.findNiftyControl("#menu", Menu.class);
//        geometryMenu.setWidth(new SizeValue("175px"));
//        geometryMenu.addMenuItem("Show bound", null, MENU_SHOW_BOUND);
//        geometryMenu.addMenuItem("Show normals", null, MENU_SHOW_NORMALS);
//        geometryMenu.addMenuItem("Show wireframe", null, MENU_SHOW_WIREFRAME);
//    }
//
//    @SuppressWarnings("unchecked")
//    private void createVoxelMenu() {
//        voxelMenu = voxelPopupMenu.findNiftyControl("#menu", Menu.class);
//        voxelMenu.setWidth(new SizeValue("175px"));
//        voxelMenu.addMenuItem("Show surface bound", null, MENU_SHOW_BOUND);
//        voxelMenu.addMenuItem("Show chunk bounds", null, MENU_SHOW_CHUNK_BOUNDS);
//        voxelMenu.addMenuItem("Show chunk lods", null, MENU_SHOW_CHUNK_LODS);
//        voxelMenu.addMenuItem("Show normals", null, MENU_SHOW_NORMALS);
//        voxelMenu.addMenuItem("Show wireframe", null, MENU_SHOW_WIREFRAME);
//        // voxelMenu.addMenuItem("Show voxel grid", null, MENU_SHOW_VOXEL_GRID);
//    }
//}
