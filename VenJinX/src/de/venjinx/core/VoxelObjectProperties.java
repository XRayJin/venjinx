package de.venjinx.core;

import de.venjinx.core.VoxelWorld.VoxelMode;


public class VoxelObjectProperties {

    VoxelObject object;

    VoxelMode mode;
    int maxLOD, chunkSize, extractionLvl;
    float lvlHeight;
    boolean smooth, fill;
}