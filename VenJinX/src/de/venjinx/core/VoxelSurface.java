package de.venjinx.core;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import de.venjinx.core.VoxelQuadTree.QuadCases;
import de.venjinx.core.VoxelWorld.VoxelMode;

public class VoxelSurface {

    private HashMap<Long, Integer> idToIndex = new HashMap<>();
    private ArrayList<Integer> indexes = new ArrayList<>();
    private ArrayList<Vector3> vertices = new ArrayList<>();
    private ArrayList<Vector3> normals = new ArrayList<>();
    private ArrayList<Vector4> tangents = new ArrayList<>();
    private ArrayList<Vector3> binormals = new ArrayList<>();
    private ArrayList<Vector2> texCoords = new ArrayList<>();

    private IntBuffer indexBuffer;
    private FloatBuffer vertexBuffer;
    private FloatBuffer normalBuffer;
    private FloatBuffer tangentBuffer;
    private FloatBuffer binormalBuffer;
    private FloatBuffer texCoordsBuffer;

    private long zipKey;
    int xMin, yMin, zMin, xMax, yMax, zMax;
    private int lod;
    private HermiteData data;

    private Set<Long> surfaceBoundVerts = new HashSet<>();
    private Set<Long> dataBoundVerts = new HashSet<>();
    private LinkedList<VoxelQuadTree> outlineQuads = new LinkedList<>();
    private HashMap<Long, VoxelQuadTree> quads = new HashMap<>();

    public VoxelSurface(int xOff, int yOff, int zOff, int xBound, int yBound,
                        int zBound, long zipKey, int lod, HermiteData data) {
        this.zipKey = zipKey;
        this.lod = lod;
        xMin = xOff;
        yMin = yOff;
        zMin = zOff;
        xMax = xBound;
        yMax = yBound;
        zMax = zBound;
        this.data = data;
    }

    public static long genQuads = 0;
    public static long genMesh = 0;
    public static long createMesh = 0;
    public static long vertCount = 0;
    public static long triCount = 0;

    public long generate(float lvlHeight, VoxelMode mode,
                         boolean smooth, boolean[] sides) {
        long t1 = System.nanoTime();
        switch (mode) {
            case LINE_2D:
                for (VoxelQuadTree quad : quads.values()) {
                    genQuads += quad.generate(-1, false, smooth, lod, sides);
                    genMesh += addQuadTree(quad, smooth);
                }
                break;
            case SURFACE_2D:
                for (VoxelQuadTree quad : quads.values()) {
                    genQuads += quad.generate(-1, true, false, lod, sides);
                    genMesh += addQuadTree(quad, smooth);
                }
                break;
            case PSEUDO_3D_LINE:
                for (VoxelQuadTree quad : quads.values()) {
                    genQuads += quad.generate(lvlHeight, false, smooth, lod, sides);
                    genMesh += addQuadTree(quad, smooth);
                }
                break;
            case PSEUDO_3D_SURFACE:
                for (VoxelQuadTree quad : quads.values()) {
                    genQuads += quad.generate(lvlHeight, true, smooth, lod, sides);
                    genMesh += addQuadTree(quad, smooth);
                }
                break;
            case SURFACE_3D:
                break;
        }

        long t = System.nanoTime();
        createBuffers();
        createMesh += System.nanoTime() - t;
        vertCount += vertices.size();
        triCount += indexes.size() / 3;
        return System.nanoTime() - t1;
    }

    public void addQuad(VoxelQuadTree quad) {
        quad.surface = this;
        quad.lod = lod;
        quad.key = zipKey;
        quads.put(quad.id, quad);
        if (quad.quadCase != 0)
            outlineQuads.add(quad);
    }

    public VoxelQuadTree getQuad(long id) {
        return quads.get(id);
    }

    public long addQuadTree(VoxelQuadTree tree, boolean smooth) {
        long t = System.nanoTime();
        tree.surface = this;

        if (tree.isLeaf())
            addTreeLeaf(tree, smooth);
        else for (VoxelQuadTree q : tree.trees)
            if (q != null)
                addQuadTree(q, smooth);
        return System.nanoTime() - t;
    }

    private void addTreeLeaf(VoxelQuadTree leaf, boolean smooth) {
        Vector3 vert, norm, v = new Vector3();
        Vector4 tang;
        long vID;
        int index;

        int[] tIndexes = leaf.getIndexes();
        int[] newIndexes = new int[tIndexes.length];
        Vector3[] tVerts = leaf.getVertices();
        Vector3[] tNorms = leaf.getNormals();
        Vector4[] tTangs = leaf.getTangents();

        if (smooth) {
            for (int i = 0; i < tVerts.length; i++) {
                vert = tVerts[i];
                norm = tNorms[i];
                vID = generateVertexID(vert, QuadCases.getNormalID(norm));

                if (idToIndex.containsKey(vID)) {
                    index = idToIndex.get(vID);
                    norm = normals.get(index);
                    tang = tangents.get(index);
                    norm.addL(tNorms[i]).normL();
                    tang.addL(tTangs[i].x, tTangs[i].y,
                              tTangs[i].z, 0).normL();

                    v.set(tang.x, tang.y, tang.z);
                    binormals.get(index).set(norm.cross(v).normL());
                } else {
                    index = vertices.size();
                    idToIndex.put(vID, index);
                    tang = tTangs[i];
                    v.set(tang.x, tang.y, tang.z);

                    if (leaf.isBoundQuad())
                        if (vert.x == xMin || vert.x == xMax
                        || vert.z == zMin || vert.z == zMax)
                            surfaceBoundVerts.add(vID);

                    vertices.add(vert);
                    normals.add(norm);
                    tangents.add(tang);
                    binormals.add(norm.cross(v).normL());
                    texCoords.add(new Vector2(vert.x / xMax, 1 - vert.z / zMax));
                }

                for (int j = 0; j < tIndexes.length; j++)
                    if (tIndexes[j] == i)
                        newIndexes[j] = index;
            }
            //            System.out.println(leaf);
            //            System.out.println("---------------------");

            for (int i : newIndexes)
                indexes.add(i);
        } else {
            int off = vertices.size();
            for (int i : tIndexes)
                indexes.add(i + off);

            for (int i = 0; i < tVerts.length; i++) {
                tang = tTangs[i];
                v.set(tang.x, tang.y, tang.z);

                vertices.add(tVerts[i]);
                normals.add(tNorms[i]);
                tangents.add(tang);
                binormals.add(tNorms[i].cross(v).normL());
                texCoords.add(new Vector2(tVerts[i].x / xMax, 1 - tVerts[i].z / zMax));
            }
        }
    }

    // public void merge(VoxelSurface otherSurface, boolean filter) {
    // Vector3 vert;
    // Vector3 norm;
    // long vID;
    // int index, vertCount;
    //
    // int[] tIndexes = otherSurface.getIndexes();
    // Vector3[] tVerts = otherSurface.getVertices();
    // Vector3[] tNorms = otherSurface.getNormals();
    // Vector4[] tTangs = otherSurface.getTangents();
    // Vector3[] tBinorms = otherSurface.getBinormals();
    // Vector2[] texCoordsArray = otherSurface.getTexCoords();
    // if (filter)
    // for (int tIndex : tIndexes) {
    // vert = tVerts[tIndex];
    // norm = tNorms[tIndex];
    // vID = generateVertexID(vert, QuadCases.getNormalID(norm));
    //
    // if (idToIndex.containsKey(vID)) {
    // index = idToIndex.get(vID);
    // indexes.add(index);
    // if (!normals.get(index).equals(tNorms[tIndex])) {
    // normals.get(index).addL(tNorms[tIndex]).normL();
    // tangents.get(index).addL(tTangs[tIndex].x, tTangs[tIndex].y,
    // tTangs[tIndex].z, 0).normL();
    // binormals.get(index).addL(tBinorms[tIndex]).normL();
    //
    // tNorms[tIndex].set(0, 0, 0);
    // tTangs[tIndex].set(0, 0, 0, 0);
    // tBinorms[tIndex].set(0, 0, 0);
    // }
    // } else {
    // index = vertices.size();
    // idToIndex.put(vID, index);
    //
    // indexes.add(index);
    // vertices.add(vert);
    // normals.add(tNorms[tIndex]);
    // tangents.add(tTangs[tIndex]);
    // binormals.add(tBinorms[tIndex]);
    // texCoords.add(texCoordsArray[tIndex]);
    // }
    // }
    // else {
    // for (int i : tIndexes)
    // indexes.add(i + vertices.size());
    //
    // vertCount = tVerts.length;
    // for (int i = 0; i < vertCount; i++) {
    // vertices.add(tVerts[i]);
    // normals.add(tNorms[i]);
    // tangents.add(tTangs[i]);
    // binormals.add(tBinorms[i]);
    // texCoords.add(texCoordsArray[i]);
    // }
    // }
    // }

    public void createBuffers() {
        ByteBuffer b;

        b = ByteBuffer.allocateDirect(4 * indexes.size());
        indexBuffer = b.order(ByteOrder.nativeOrder()).asIntBuffer();
        indexBuffer.clear();
        int[] tIndexes = new int[indexes.size()];
        for (int i = 0; i < tIndexes.length; i++)
            tIndexes[i] = indexes.remove(0);
        indexBuffer.put(tIndexes);
        indexBuffer.flip();

        b = ByteBuffer.allocateDirect(4 * vertices.size() * 3);
        vertexBuffer = b.order(ByteOrder.nativeOrder()).asFloatBuffer();
        vertexBuffer.clear();
        for (Vector3 v3 : vertices)
            if (v3 != null)
                vertexBuffer.put(v3.x).put(v3.y).put(v3.z);
            else vertexBuffer.put(0).put(0).put(0);
        vertexBuffer.flip();

        b = ByteBuffer.allocateDirect(4 * normals.size() * 3);
        normalBuffer = b.order(ByteOrder.nativeOrder()).asFloatBuffer();
        normalBuffer.clear();
        for (Vector3 v3 : normals)
            if (v3 != null)
                normalBuffer.put(v3.x).put(v3.y).put(v3.z);
            else normalBuffer.put(0).put(0).put(0);
        normalBuffer.flip();

        b = ByteBuffer.allocateDirect(4 * tangents.size() * 4);
        tangentBuffer = b.order(ByteOrder.nativeOrder()).asFloatBuffer();
        tangentBuffer.clear();
        for (Vector4 v4 : tangents)
            if (v4 != null)
                tangentBuffer.put(v4.x).put(v4.y).put(v4.z).put(v4.w);
            else tangentBuffer.put(0).put(0).put(0);
        tangentBuffer.flip();

        b = ByteBuffer.allocateDirect(4 * binormals.size() * 3);
        binormalBuffer = b.order(ByteOrder.nativeOrder()).asFloatBuffer();
        binormalBuffer.clear();
        for (Vector3 v3 : binormals)
            if (v3 != null)
                binormalBuffer.put(v3.x).put(v3.y).put(v3.z);
            else binormalBuffer.put(0).put(0).put(0);
        binormalBuffer.flip();

        b = ByteBuffer.allocateDirect(4 * texCoords.size() * 2);
        texCoordsBuffer = b.order(ByteOrder.nativeOrder()).asFloatBuffer();
        texCoordsBuffer.clear();
        for (Vector2 v2 : texCoords)
            if (v2 != null)
                texCoordsBuffer.put(v2.x).put(v2.y);
            else texCoordsBuffer.put(0).put(0);
        texCoordsBuffer.flip();
    }

    public IntBuffer getIndexes() {
        return indexBuffer;
    }

    public FloatBuffer getVertices() {
        return vertexBuffer;
    }

    public FloatBuffer getNormals() {
        return normalBuffer;
    }

    public FloatBuffer getTangents() {
        return tangentBuffer;
    }

    public FloatBuffer getBinormals() {
        return binormalBuffer;
    }

    public FloatBuffer getTexCoords() {
        return texCoordsBuffer;
    }

    private long generateVertexID(Vector3 vertex, int normalID) {
        long vertID;
        long x = (long) (vertex.x * Math.pow(10, lod));
        long y = (long) (vertex.y * Math.pow(10, lod));
        long z = (long) (vertex.z * Math.pow(10, lod));
        //        vertID = x * zipKey * zipKey * 16 + y * zipKey * 16 + z * 16 + normalID;
        vertID = x * zipKey * zipKey * 16 + y * zipKey * 16 + z * 16;
        return vertID;
    }

    public boolean isComplete() {
        return surfaceBoundVerts.size() == 0;
    }

    public int getXMin() {
        return xMin;
    }

    public int getYMin() {
        return yMin;
    }

    public int getZMin() {
        return zMin;
    }

    public int getXMax() {
        return xMax;
    }

    public int getYMax() {
        return yMax;
    }

    public int getZMax() {
        return zMax;
    }

    public Set<Long> getSufaceBoundVerts() {
        return surfaceBoundVerts;
    }

    public Set<Long> getDataBoundVerts() {
        return dataBoundVerts;
    }

    public long getZipKey() {
        return zipKey;
    }

    public int getLOD() {
        return lod;
    }

    public HermiteData getData() {
        return data;
    }

    @Override
    public String toString() {
        String str = "";

        str += "VoxelSurface" + "(" + xMin + ", " + yMin + ", " + zMin + ")";
        str += " - (" + xMax + ", " + yMax + ", " + zMax + ") - " + lod;

        return str;
    }
}