package de.venjinx.core;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

import de.venjinx.core.CopyOfVoxelObject.VoxelObjectProperties;
import de.venjinx.core.VoxelOperations.SegmentBuilder;
import de.venjinx.core.VoxelOperations.VertexCreator;
import de.venjinx.core.VoxelWorld.VoxelMode;

public class CopyOfVoxelQuadTree {

    /**
     * <code>QuadCases</code>
     *
     * Base definition for a quad.
     *
     *    points          triangles         rotated
     * 3-----6-----2     -----------      -----------
     * |   / | \   |    | 3 / | \ 2 |    | \ 7 | 2 / |
     * | /   |   \ |    | / 7 | 6 \ |    | 3 \ | / 6 |
     * 7-----8-----5     -----------      -----------
     * | \   |   / |    | \ 4 | 5 / |    | 4 / | \ 1 |
     * |   \ | /   |    | 0 \ | / 1 |    | / 0 | 5 \ |
     * 0-----4-----1     -----------      -----------
     *
     * @author Torge Rothe (X-Ray-Jin)
     */
    static class QuadCases {
        static final int[][] edgePts = new int[][]
                { { },                                                          // 0
                  { 3, 0 },                                                     // 1
                  { 0, 1 },                                                     // 2
                  { 3, 1 },                                                     // 3
                  { 1, 2 },                                                     // 4
                  { 1, 0, 3, 2 },                                               // 5
                  { 0, 2 },                                                     // 6
                  { 3, 2 },                                                     // 7

                  { 2, 3 },                                                     // 8
                  { 2, 0 },                                                     // 9
                  { 0, 3, 2, 1 },                                               // 10
                  { 2, 1 },                                                     // 11
                  { 1, 3 },                                                     // 12
                  { 1, 0 },                                                     // 13
                  { 0, 3 },                                                     // 14
                  { } };                                                        // 15

        static final int[][] edgeQuads = new int[][]
                { { 0, 1 },                                                     // 0
                  { 1, 2 },                                                     // 1
                  { 2, 3 },                                                     // 2
                  { 3, 0 } };                                                   // 3

        static final int[][] fillIndexes = new int[][]
                { { 0, 1, 3, 3, 1, 2 },                                         // 0
                  { 7, 4, 8, 4, 1, 8, 7, 8, 3, 1, 2, 3 },                       // 1
                  { 4, 5, 8, 5, 2, 8, 4, 8, 0, 2, 3, 0 },                       // 2
                  { 7, 5, 3, 3, 5, 2 },                                         // 3
                  { 5, 6, 8, 6, 3, 8, 5, 8, 1, 3, 0, 1 },                       // 4
                  { 5, 4, 1, 7, 6, 3, 4, 5, 7, 7, 5, 6 },                       // 5
                  { 4, 6, 0, 0, 6, 3 },                                         // 6
                  { 7, 6, 3 },                                                  // 7

                  { 6, 7, 8, 7, 0, 8, 6, 8, 2, 0, 1, 2 },                       // 8
                  { 6, 4, 2, 2, 4, 1 },                                         // 9
                  { 4, 7, 0, 6, 5, 2, 4, 5, 7, 7, 5, 6 },                       // 10
                  { 6, 5, 2 },                                                  // 11
                  { 5, 7, 1, 1, 7, 0 },                                         // 12
                  { 5, 4, 1 },                                                  // 13
                  { 4, 7, 0 },                                                  // 14
                  { } };                                                        // 15

        static final int[][] outlineIndexes = new int[][]
                { { },                                                          // 0
                  { 16, 13, 7, 4, 7, 13 },                                      // 1
                  { 13, 14, 4, 5, 4, 14 },                                      // 2
                  { 16, 14, 7, 5, 7, 14 },                                      // 3
                  { 14, 15, 5, 6, 5, 15 },                                      // 4
                  { 14, 13, 5, 4, 5, 13, 16, 15, 7, 6, 7, 15 },                 // 5
                  { 13, 15, 4, 6, 4, 15 },                                      // 6
                  { 16, 15, 7, 6, 7, 15 },                                      // 7

                  { 15, 16, 6, 7, 6, 16 },                                      // 8
                  { 15, 13, 6, 4, 6, 13 },                                      // 9
                  { 13, 16, 4, 7, 4, 16, 15, 14, 6, 5, 6, 14 },                 // 10
                  { 15, 14, 6, 5, 6, 14 },                                      // 11
                  { 14, 16, 5, 7, 5, 16 },                                      // 12
                  { 14, 13, 5, 4, 5, 13 },                                      // 13
                  { 13, 16, 4, 7, 4, 16 },                                      // 14
                  { } };                                                        // 15

        static final int[] patchTriangles = new int[]
                // hex         case         tris           binary       dec
                { 0xff,     //   0    0 1 2 3 4 5 6 7   [ 1111 1111 ]   255
                  0xfe,     //   1    - 1 2 3 4 5 6 7   [ 1111 1110 ]   254
                  0xfd,     //   2    0 - 2 3 4 5 6 7   [ 1111 1101 ]   253
                  0xcc,     //   3    - - 2 3 - - 6 7   [ 1100 1100 ]   204
                  0xfb,     //   4    0 1 - 3 4 5 6 7   [ 1111 1011 ]   251
                  0x12,     //   5    - 1 - 3 - - - -   [ 0000 1010 ]    12
                  0x99,     //   6    0 - - 3 4 - - 7   [ 1001 1001 ]   153
                  0x8,      //   7    - - - 3 - - - -   [ 0000 1000 ]     8

                  0xf7,     //   8    0 1 2 3 - 5 6 7   [ 1111 0111 ]   247
                  0x66,     //   9    - 1 2 - - 5 6 -   [ 0110 0110 ]   102
                  0x5,      //  10    0 - 2 - - - - -   [ 0000 0101 ]     5
                  0x4,      //  11    - - 2 - - - - -   [ 0000 0100 ]     4
                  0x33,     //  12    0 1 - - 4 5 - -   [ 0011 0011 ]    51
                  0x2,      //  13    - 1 0 - - - - -   [ 0000 0010 ]     2
                  0x1,      //  14    0 - - - - - - -   [ 0000 0001 ]     1
                  0x0 };    //  15    - - - - - - - -   [ 0000 0000 ]     0

        //        static final int[] triangles = new int[]
        //                // hex         case      base tris              binary            dec
        //                { 0x500,    //   0    - - - - - - - -   [ 0000 0101 00000000 ]    1280
        //                  0x438,    //   1    - - - 3 4 5 - -   [ 0000 0100 00111000 ]    1080
        //                  0x861,    //   2    0 - - - - 5 6 -   [ 0000 1000 01100001 ]    2145
        //                  0x400c,   //   3    - - 2 3 - - - -   [ 0100 0000 00001100 ]   16396
        //                  0x1c2,    //   4    - 1 - - - - 6 7   [ 0000 0001 11000010 ]     450
        //                  0xa,      //   5    - 1 - 3 - - - -   [ 0000 0000 00001010 ]      10
        //                  0x8009,   //   6    0 - - 3 - - - -   [ 1000 0000 00001001 ]   32777
        //                  0x8,      //   7    - - - 3 - - - -   [ 0000 0000 00001000 ]       8
        //
        //                  0x294,    //   8    - - 2 - 4 - - 7   [ 0000 0010 10010100 ]     660
        //                  0x2006,   //   9    - - 2 - - 5 - -   [ 0010 0000 00000110 ]    8198
        //                  0x5,      //  10    0 - 2 - - - - -   [ 0000 0000 00000101 ]       5
        //                  0x4,      //  11    - - 2 - - - - -   [ 0000 0000 00000100 ]       4
        //                  0x1003,   //  12    - 1 - - 4 - - -   [ 0001 0000 00000011 ]    4099
        //                  0x2,      //  13    - 1 - - - - - -   [ 0000 0000 00000010 ]       2
        //                  0x1,      //  14    0 - - - - - - -   [ 0000 0000 00000001 ]       1
        //                  0x0 };    //  15    - - - - - - - -   [ 0000 0000 00000000 ]       0

        static final int[] triangles = new int[]
                // hex         case      base tris              binary            dec
                { 0xf00,    //   0    - - - - - - - -   [ 0000 1111 00000000 ]    3840
                  0x638,    //   1    - - - 3 4 5 - -   [ 0000 0110 00111000 ]    1592
                  0xc61,    //   2    0 - - - - 5 6 -   [ 0000 1100 01100001 ]    3169
                  0x400c,   //   3    - - 2 3 - - - -   [ 0100 0000 00001100 ]   16396
                  0x9c2,    //   4    - 1 - - - - 6 7   [ 0000 1001 11000010 ]    2498
                  0xa,      //   5    - 1 - 3 - - - -   [ 0000 0000 00001010 ]      10
                  0x8009,   //   6    0 - - 3 - - - -   [ 1000 0000 00001001 ]   32777
                  0x8,      //   7    - - - 3 - - - -   [ 0000 0000 00001000 ]       8

                  0x394,    //   8    - - 2 - 4 - - 7   [ 0000 0011 10010100 ]     916
                  0x2006,   //   9    - - 2 - - 5 - -   [ 0010 0000 00000110 ]    8198
                  0x5,      //  10    0 - 2 - - - - -   [ 0000 0000 00000101 ]       5
                  0x4,      //  11    - - 2 - - - - -   [ 0000 0000 00000100 ]       4
                  0x1003,   //  12    - 1 - - 4 - - -   [ 0001 0000 00000011 ]    4099
                  0x2,      //  13    - 1 - - - - - -   [ 0000 0000 00000010 ]       2
                  0x1,      //  14    0 - - - - - - -   [ 0000 0000 00000001 ]       1
                  0x0 };    //  15    - - - - - - - -   [ 0000 0000 00000000 ]       0

        static final int[][] triangle = new int[][]
                // hex      ids   rotate       triID    pts        binary       dec         rotate
                { { 0x91, 0, 4, 7, 0x0 },   //   0    0, 4, 7  [ 0 1001 0001 ]  145  - - [ 0000 0000 ]   0
                  { 0x32, 1, 5, 4, 0x0 },   //   1    5, 4, 1  [ 0 0011 0010 ]   50  - - [ 0000 0000 ]   0
                  { 0x64, 2, 6, 5, 0x0 },   //   2    6, 5, 2  [ 0 0110 0100 ]  100  - - [ 0000 0000 ]   0
                  { 0xc8, 3, 7, 6, 0x0 },   //   3    7, 6, 3  [ 0 1100 1000 ]  200  - - [ 0000 0000 ]   0
                  { 0x190, 8, 7, 4, 0x0 },  //   4    8, 7, 4  [ 1 1001 0000 ]  400  - - [ 0000 0000 ]   0
                  { 0x130, 8, 4, 5, 0x0 },  //   5    4, 5, 8  [ 1 0011 0000 ]  304  - - [ 0000 0000 ]   0
                  { 0x160, 8, 5, 6, 0x0 },  //   6    5, 6, 8  [ 1 0110 0000 ]  352  - - [ 0000 0000 ]   0
                  { 0x1c0, 8, 6, 7, 0x0 },  //   7    6, 7, 8  [ 1 1100 0000 ]  448  - - [ 0000 0000 ]   0

                  { 0x113, 0, 1, 8, 0x12 }, //   8    0, 1, 8  [ 1 0001 0011 ]  275  1 7 [ 0001 0010 ]  18
                  { 0x126, 1, 2, 8, 0x24 }, //   9    1, 2, 8  [ 1 0010 0110 ]  294  2 4 [ 0010 0100 ]  36
                  { 0x14c, 2, 3, 8, 0x48 }, //  10    2, 3, 8  [ 1 0100 1100 ]  332  3 5 [ 0100 1000 ]  72
                  { 0x189, 3, 0, 8, 0x81 }, //  11    0, 3, 8  [ 1 1000 1001 ]  393  0 6 [ 1000 0001 ] 129

                  //                  { 0x9b, 0, 1, 3, 0x82 },  //  12    0, 1, 3  [ 0 1001 1011 ]  155  1 7 [ 1000 0010 ] 130
                  //                  { 0x37, 1, 2, 0, 0x14 },  //  13    0, 1, 2  [ 0 0011 0111 ]   55  2 4 [ 0010 0100 ]  20
                  //                  { 0x6e, 2, 3, 1, 0x28 },  //  14    1, 2, 3  [ 0 0110 1110 ]  110  3 5 [ 0010 1000 ]  40
                  //                  { 0xcd, 3, 0, 2, 0x41 },  //  15    0, 2, 3  [ 0 1100 1101 ]  205  0 6 [ 0100 0001 ]  65

                  { 0xb0, 4, 5, 7, 0x0 },   //   8    4, 5, 7  [ 0 1011 0000 ]  176  - - [ 0000 0000 ]   0
                  { 0x70, 5, 6, 4, 0x0 },   //   9    4, 5, 6  [ 0 0111 0000 ]  112  - - [ 0000 0000 ]   0
                  { 0xe0, 6, 7, 5, 0x0 },   //  10    5, 6, 7  [ 0 1110 0000 ]  224  - - [ 0000 0000 ]   0
                  { 0xd0, 7, 4, 6, 0x0 } }; //  11    4, 6, 7  [ 0 1101 0000 ]  208  - - [ 0000 0000 ]   0

        static final int[] mergeTriangles = new int[]
                // hex        triID   base tris     binary       dec
                { 0x39,     //  0    0, 3, 4, 5  [ 0011 1001 ]    57
                  0x63,     //  1    0, 1, 5, 6  [ 0110 0011 ]    99
                  0xc6,     //  2    1, 2, 6, 7  [ 1100 0110 ]   198
                  0x9c,     //  3    2, 3, 4, 7  [ 1001 1100 ]   156

                  0x30,     //  4    4, 5        [ 0011 0000 ]    48
                  0x60,     //  5    5, 6        [ 0110 0000 ]    96
                  0xc0,     //  6    6, 7        [ 1100 0000 ]   192
                  0x90 };   //  7    4, 7        [ 1001 0000 ]   144

        static final int[] mergeTriangles2 = new int[]
                // hex        triID   base tris     binary       dec
                { 0x21,     //  0    0, 3, 4, 5  [ 0010 0001 ]    33
                  0x42,     //  1    0, 1, 5, 6  [ 0100 0010 ]    66
                  0x84,     //  2    1, 2, 6, 7  [ 1000 0100 ]   132
                  0x18,     //  3    2, 3, 4, 7  [ 0001 1000 ]    24

                  0x30,     //  4    4, 5        [ 0011 0000 ]    48
                  0x60,     //  5    5, 6        [ 0110 0000 ]    96
                  0xc0,     //  6    6, 7        [ 1100 0000 ]   192
                  0x90 };   //  7    4, 7        [ 1001 0000 ]   144

        static final Vector3[] normals = new Vector3[]
                { new Vector3(0, 1, 0).normL(),                                 // 0
                  new Vector3(-1, 0, 1).normL(),                                // 1
                  new Vector3(1, 0, 1).normL(),                                 // 2
                  new Vector3(0, 0, 1).normL(),                                 // 3
                  new Vector3(1, 0, -1).normL(),                                // 4
                  new Vector3(-1, 0, -1).normL(),                               // 5
                  new Vector3(1, 0, 0).normL(),                                 // 6
                  new Vector3(1, 0, 1).normL(),                                 // 7

                  new Vector3(-1, 0, -1).normL(),                               // 8
                  new Vector3(-1, 0, 0).normL(),                                // 9
                  new Vector3(1, 0, -1).normL(),                                // 10
                  new Vector3(-1, 0, 1).normL(),                                // 11
                  new Vector3(0, 0, -1).normL(),                                // 12
                  new Vector3(-1, 0, -1).normL(),                               // 13
                  new Vector3(1, 0, -1).normL(),                                // 14
                  new Vector3(0, -1, 0).normL() };                              // 15

        static final Vector3[] ambigousNormals = new Vector3[]
                { new Vector3(-1, 0, -1).normL(),
                  new Vector3(1, 0, 1).normL(),
                  new Vector3(-1, 0, 1).normL(),
                  new Vector3(1, 0, 1).normL() };

        static final Vector3[] tangents = new Vector3[]
                { new Vector3(1, 0, 0).normL(),                                 // 0
                  new Vector3(1, 0, 1).normL(),                                 // 1
                  new Vector3(1, 0, -1).normL(),                                // 2
                  new Vector3(1, 0, 0).normL(),                                 // 3
                  new Vector3(-1, 0, -1).normL(),                               // 4
                  new Vector3(-1, 0, 1).normL(),                                // 5
                  new Vector3(0, 0, -1).normL(),                                // 6
                  new Vector3(1, 0, -1).normL(),                                // 7

                  new Vector3(-1, 0, 1).normL(),                                // 8
                  new Vector3(0, 0, 1).normL(),                                 // 9
                  new Vector3(-1, 0, -1).normL(),                               // 10
                  new Vector3(1, 0, 1).normL(),                                 // 11
                  new Vector3(-1, 0, 0).normL(),                                // 12
                  new Vector3(-1, 0, 1).normL(),                                // 13
                  new Vector3(-1, 0, -1).normL(),                               // 14
                  new Vector3(-1, 0, 0).normL() };                              // 15

        static final boolean[][] neighbors = new boolean[][]
                //  bottom  right    top     left     up     down
                { { false,  false,  false,  false,  false,  false },            // 0
                  { true,   false,  false,  true,   false,  false },            // 1
                  { true,   true,   false,  false,  false,  false },            // 2
                  { false,  true,   false,  true,   false,  false },            // 3
                  { false,  true,   true,   false,  false,  false },            // 4
                  { true,   true,   true,   true,   false,  false },            // 5
                  { true,   false,  true,   false,  false,  false },            // 6
                  { false,  false,  true,   true,   false,  false },            // 7

                  { false,  false,  true,   true,   false,  false },            // 8
                  { true,   true,   false,  false,  false,  false },            // 9
                  { true,   true,   true,   true,   false,  false },            // 10
                  { false,  true,   true,   false,  false,  false },            // 11
                  { false,  true,   false,  true,   false,  false },            // 12
                  { true,   true,   false,  false,  false,  false },            // 13
                  { true,   false,  false,  true,   false,  false },            // 14
                  { false,  false,  false,  false,  false,  false } };          // 15

        static int getCase(int x, int y, int z, float[][][] data) {
            boolean x0, x1, y0, z0, z1;
            x0 = x >= 0 && x < data.length;
            x1 = x >= -1 && x + 1 < data.length;
            y0 = y >= 0 && y < data.length;
            z0 = z >= 0 && z + 1 < data.length;
            z1 = z >= -1 && z + 1 < data.length;

            int qCase = 0;
            if (!x0 || !y0 || !z1 || data[x][y][z + 1] < 0)
                qCase |= 1;
            if (!x1 || !y0 || !z1 || data[x + 1][y][z + 1] < 0)
                qCase |= 2;
            if (!x1 || !y0 || !z0 || data[x + 1][y][z] < 0)
                qCase |= 4;
            if (!x0 || !y0 || !z0 || data[x][y][z] < 0)
                qCase |= 8;

            return qCase;
        }

        static int getCase(float[] data) {
            if (data == null || data.length <= 4)
                return 15;

            int qCase = 0;
            if (data[0] < 0)
                qCase |= 1;
            if (data[1] < 0)
                qCase |= 2;
            if (data[2] < 0)
                qCase |= 4;
            if (data[3] < 0)
                qCase |= 8;
            return qCase;
        }

        static int getCase(float[] data, int depth, int[] pos) {
            if (data == null || data.length <= 4)
                return 15;

            if (pos != null && depth >= 0 && depth < pos.length) {
                float d[] = new float[5];
                switch (pos[depth]) {
                    case 0:
                        d[0] = data[0];
                        d[1] = (data[0] + data[1]) / 2;
                        d[2] = data[4];
                        d[3] = (data[0] + data[3]) / 2;
                        break;
                    case 1:
                        d[0] = (data[1] + data[0]) / 2;
                        d[1] = data[1];
                        d[2] = (data[1] + data[2]) / 2;
                        d[3] = data[4];
                        break;
                    case 2:
                        d[0] = data[4];
                        d[1] = (data[2] + data[1]) / 2;
                        d[2] = data[2];
                        d[3] = (data[2] + data[3]) / 2;
                        break;
                    case 3:
                        d[0] = (data[3] + data[0]) / 2;
                        d[1] = data[4];
                        d[2] = (data[3] + data[2]) / 2;
                        d[3] = data[3];
                        break;
                    default:
                        return getCase(data);
                }
                d[4] = (d[0] + d[1] + d[2] + d[3]) / 4;
                return getCase(d, depth + 1, pos);
            }

            return getCase(data);
        }

        static Vector3 getNormal(int quadCase, int edgeID) {
            if (quadCase >= 0 && quadCase < 16) {
                if (quadCase == 5)
                    return new Vector3(ambigousNormals[edgeID]);

                if (quadCase == 10)
                    return new Vector3(ambigousNormals[edgeID + 2]);

                return new Vector3(normals[quadCase]);
            }
            return null;
        }

        static long generateQuadID(int x, int y, int z, int quadCase,
                                   boolean bound, int yMax, int zMax) {
            long zippedFace = x * yMax * zMax * 16 + y * zMax * 16 + z * 16 + quadCase;

            zippedFace++;
            if (bound)
                zippedFace = -zippedFace;

            return zippedFace;
        }
    }

    private VertexCreator vertCreator;
    //    private SegmentBuilder segBuilder;

    long id = Long.MIN_VALUE;
    CopyOfVoxelChunk chunk;
    CopyOfVoxelQuadTree parent;
    CopyOfVoxelQuadTree[] quadTree;

    final int currentDepth;
    int[] position;

    float[] data = new float[5];
    CopyOfVoxelQuadTree[] neighbors = new CopyOfVoxelQuadTree[6];
    boolean[] patchSides = new boolean[6];

    boolean visible = true;
    float x = 0, y = 0, z = 0, size = 1;
    int quadCase = 0, lod = 0;

    private int iOff = 0, vOff = 0;
    private boolean bound = false;
    private boolean patch = false;
    private boolean fillUp = true;
    private boolean fillDown = false;
    private boolean ambiguousCase = false;
    boolean ambiguous = false;

    Vector3[] tVerts;
    Vector3[] tNorms;
    Vector4[] tTangs;
    int[] tIndexes;

    public CopyOfVoxelQuadTree(int x, int y, int z, int quadCase,
                               boolean bound, float[] dData) {
        currentDepth = 0;
        position = new int[] {};

        this.x = x;
        this.y = y;
        this.z = z;
        this.quadCase = quadCase;

        data[0] = dData[0];
        data[1] = dData[1];
        data[2] = dData[2];
        data[3] = dData[3];
        data[4] = dData[4];

        ambiguousCase = quadCase == 5 || quadCase == 10;
        ambiguous = ambiguousCase && data[4] >= 0;
    }

    public CopyOfVoxelQuadTree(int x, int y, int z, CopyOfVoxelChunk chunk) {
        currentDepth = 0;
        position = new int[] {};

        this.chunk = chunk;
        this.x = x;
        this.y = y;
        this.z = z;
        lod = chunk.lod;

        data[0] = chunk.data.getDensity(x, y, z + 1);
        data[1] = chunk.data.getDensity(x + 1, y, z + 1);
        data[2] = chunk.data.getDensity(x + 1, y, z);
        data[3] = chunk.data.getDensity(x, y, z);
        data[4] = chunk.data.getInterpolatedDensity(x + .5f, y, z + .5f);

        quadCase = QuadCases.getCase(data);

        ambiguousCase = quadCase == 5 || quadCase == 10;
        ambiguous = ambiguousCase && data[4] >= 0;

        bound = chunk.isBound(x, y, z);

        id = QuadCases.generateQuadID(x, y, z, quadCase, bound,
                                      chunk.data.getHeight(),
                                      chunk.data.getDepth());
    }

    public CopyOfVoxelQuadTree(int x, int y, int z, int quadCase,
                               boolean bound, float[][][] dData) {
        currentDepth = 0;
        position = new int[] {};

        this.x = x;
        this.y = y;
        this.z = z;
        this.quadCase = quadCase;

        data[0] = dData[x][y][z + 1];
        data[1] = dData[x + 1][y][z + 1];
        data[2] = dData[x + 1][y][z];
        data[3] = dData[x][y][z];
        data[4] = (data[0] + data[1] + data[2] + data[3]) / 4;

        ambiguousCase = quadCase == 5 || quadCase == 10;
        ambiguous = ambiguousCase && data[4] >= 0;

        this.bound = bound;
    }

    private CopyOfVoxelQuadTree(int pos, CopyOfVoxelQuadTree parent) {
        this(parent.data, pos, parent);
    }

    private CopyOfVoxelQuadTree(float[] data, int pos, CopyOfVoxelQuadTree parent) {
        this.parent = parent;
        id = Long.MIN_VALUE;

        int l = parent.position.length;
        position = new int[l + 1];

        for (int i = 0; i < l; i++)
            position[i] = parent.position[i];

        position[l] = pos;
        lod = parent.lod - 1;
        currentDepth = parent.currentDepth + 1;
        chunk = parent.chunk;
        neighbors = Arrays.copyOf(parent.neighbors, 6);

        if (pos >= 0) {
            size = parent.size / 2;
            switch (pos) {
                case 0:
                    x = parent.x;
                    y = parent.y;
                    z = parent.z + size;
                    this.data[0] = data[0];
                    this.data[1] = (data[0] + data[1]) / 2;
                    this.data[2] = data[4];
                    this.data[3] = (data[0] + data[3]) / 2;
                    neighbors[1] = parent;
                    neighbors[2] = parent;
                    break;
                case 1:
                    x = parent.x + size;
                    y = parent.y;
                    z = parent.z + size;
                    this.data[0] = (data[1] + data[0]) / 2;
                    this.data[1] = data[1];
                    this.data[2] = (data[1] + data[2]) / 2;
                    this.data[3] = data[4];
                    neighbors[2] = parent;
                    neighbors[3] = parent;
                    break;
                case 2:
                    x = parent.x + size;
                    y = parent.y;
                    z = parent.z;
                    this.data[0] = data[4];
                    this.data[1] = (data[2] + data[1]) / 2;
                    this.data[2] = data[2];
                    this.data[3] = (data[2] + data[3]) / 2;
                    neighbors[0] = parent;
                    neighbors[3] = parent;
                    break;
                case 3:
                    x = parent.x;
                    y = parent.y;
                    z = parent.z;
                    this.data[0] = (data[3] + data[0]) / 2;
                    this.data[1] = data[4];
                    this.data[2] = (data[3] + data[2]) / 2;
                    this.data[3] = data[3];
                    neighbors[0] = parent;
                    neighbors[1] = parent;
                    break;
            }
            this.data[4] = (this.data[0] + this.data[1]
                    + this.data[2] + this.data[3]) / 4;
        } else {
            size = parent.size;
            x = parent.x;
            y = parent.y;
            z = parent.z;
            this.data[0] = data[0];
            this.data[1] = data[1];
            this.data[2] = data[2];
            this.data[3] = data[3];
            this.data[4] = data[4];
            neighbors[1] = parent;
            neighbors[2] = parent;
        }

        quadCase = QuadCases.getCase(this.data);

        ambiguousCase = quadCase == 5 || quadCase == 10;
        ambiguous = ambiguousCase && this.data[4] >= 0;

        bound = chunk != null ? chunk.isBound(x, y, z) : false;

        updateVisibility();
    }

    void findNeighbors(LinkedList<Long> idsToCheck,
                       HashMap<Long, CopyOfVoxelQuadTree> quads) {
        patch = false;
        if (chunk != null)
            for (int i = 0; i < 6; i++)
                checkNeighbor(i, idsToCheck, quads);
    }

    private void checkNeighbor(int neighborPos, LinkedList<Long> idsToCheck,
                               HashMap<Long, CopyOfVoxelQuadTree> quads) {
        boolean nBound;
        float[][][] data = chunk.data.getDensityField();
        int maxHeight = chunk.data.getHeight(), maxDepth = chunk.data.getDepth();
        int x = (int) this.x;
        int y = (int) this.y;
        int z = (int) this.z;

        switch (neighborPos) {
            case 0:
                z += 1;
                break;
            case 1:
                x += 1;
                break;
            case 2:
                z -= 1;
                break;
            case 3:
                x -= 1;
                break;
            case 4:
                y += 1;
                break;
            case 5:
                y -= 1;
                break;
        }

        nBound = chunk.isBound(x, y, z);
        patchSides[neighborPos] = bound && !chunk.inBound(x, y, z)
                && chunk.patchSides[neighborPos]
                        && QuadCases.neighbors[quadCase][neighborPos];
        patch |= patchSides[neighborPos];

        long id = QuadCases.generateQuadID(x, y, z, QuadCases.getCase(x, y, z, data),
                                           nBound, maxHeight, maxDepth);

        CopyOfVoxelQuadTree neighbor = quads.get(id);

        if (neighbor == null) {
            neighbor = new CopyOfVoxelQuadTree(x, y, z, chunk);

            if (neighbor.visible && chunk.inBound(x, y, z)) {
                idsToCheck.add(neighbor.id);
                quads.put(neighbor.id, neighbor);
            }
        }
        neighbors[neighborPos] = neighbor;
    }

    long generate(VoxelObjectProperties props, int lod) {
        return generate(props.mode, props.fill, props.smooth, props.height, lod);
    }

    long generate(VoxelMode mode, boolean fill, boolean smooth,
                  float height, int lod) {
        long t = System.nanoTime();
        this.lod = lod;
        quadTree = null;

        updateVisibility();

        quadTree = subdivide();

        if (quadTree == null) {
            if (visible)
                generateComponent(mode, fill, smooth, height);
        } else for (CopyOfVoxelQuadTree q : quadTree)
            if (q.quadCase != 15)
                q.generate(mode, fill, smooth, height, lod - 1);
        return System.nanoTime() - t;
    }

    private void generateComponent(VoxelMode mode, boolean fill,
                                   boolean smooth, float height) {
        boolean extrude = height > 0 && quadCase % 15 != 0;

        if (extrude)
            y = y * height;

        if (height < 0)
            y = 0;

        if (fill)
            fill(mode, fill, smooth, height, extrude);
    }

    Vector3 getNormal(int vID, int[] idMap) {
        int qCase = ambiguous ? 15 - quadCase : quadCase;
        if (!patch)
            return QuadCases.getNormal(qCase, vID);
        else {
            int[] tmpIDs = QuadCases.fillIndexes[qCase];
            vID = vID > 0 ? 6 : 0;
            Vector3 v0 = tVerts[idMap[tmpIDs[vID]]],
                    v1 = tVerts[idMap[tmpIDs[vID + 1]]];
            return v1.sub(v0).normL().cross(0, 1, 0).normL();
        }
    }

    private void fill(VoxelMode mode, boolean fill, boolean smooth,
                      float height, boolean extrude) {
        int[] idMap = new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1,
                                  -1, -1, -1, -1, -1, -1, -1, -1, -1 };
        Vector3 topNormal = new Vector3(0, -1, 0),
                botNormal = new Vector3(0, 1, 0);
        Vector4 tang = new Vector4(QuadCases.tangents[quadCase].x,
                                   QuadCases.tangents[quadCase].y,
                                   QuadCases.tangents[quadCase].z, 1);
        int[] topIDs = new int[0], botIDs = new int[0];

        int verts = 0x0, vCount = 0, iCount = 0;
        int pos, patchTris;

        //        smooth = false;
        if (fillUp) {
            topNormal.set(0, 1, 0);
            patchTris = getTriangles(4);
            topIDs = new int[Integer.bitCount(patchTris) * 3];
            verts = getPatchVerts(patchTris, verts, topIDs) << 9;
        }

        if (fillDown) {
            botNormal.set(0, -1, 0);
            patchTris = getTriangles(5);
            botIDs = new int[Integer.bitCount(patchTris) * 3];
            verts = getPatchVerts(patchTris, verts, botIDs) << 9;
        }

        if (extrude)
            if (smooth) {
                verts = addOutlineVertices(verts, QuadCases.fillIndexes[quadCase]);
                vCount = Integer.bitCount(verts);
            } else {
                verts = addOutlineVertices(verts, QuadCases.fillIndexes[quadCase]);
                vCount = Integer.bitCount(verts) + (ambiguousCase ? 8 : 4);
            }
        else vCount = Integer.bitCount(verts);

        tVerts = new Vector3[vCount];
        tNorms = new Vector3[vCount];
        tTangs = new Vector4[vCount];

        vertCreator = patch ? VoxelOperations.PatchVertexCreator
                            : VoxelOperations.NormalVertexCreator;

        vOff = 0;
        pos = 0x200;
        for (int i = 0; i < 9; i++) {
            if ((verts & pos) != 0) {
                tVerts[vOff] = vertCreator.create(this, i, height);
                tNorms[vOff] = topNormal.copy();
                tTangs[vOff] = tang.copy();
                idMap[i] = vOff++;
            }
            pos = pos << 1;
        }

        pos = 0x1;
        for (int i = 9; i < 18; i++) {
            if ((verts & pos) != 0) {
                tVerts[vOff] = vertCreator.create(this, i, height);
                tNorms[vOff] = botNormal.copy();
                tTangs[vOff] = tang.copy();
                idMap[i] = vOff++;
            }
            pos = pos << 1;
        }

        iOff = 0;
        iCount = topIDs.length + botIDs.length + (extrude ? ambiguousCase ? 12 : 6 : 0);
        tIndexes = new int[iCount];
        for (int i = 0; i < topIDs.length; i++)
            tIndexes[iOff++] = idMap[topIDs[i]];

        for (int i = 0; i < botIDs.length; i++)
            tIndexes[iOff++] = idMap[botIDs[i] + 9];

        if (extrude)
            buildOutline(height, smooth, idMap);
    }

    private void buildOutline(float height, boolean smooth, int[] idMap) {
        int ambFix = ambiguous ? 1 : 0;

        SegmentBuilder segBuilder = smooth ? VoxelOperations.SmoothSegmentBuilder
                                           : VoxelOperations.NormalSegmentBuilder;

        segBuilder.build(this, idMap, ambFix, height, iOff, vOff);
        if (ambiguousCase) {
            iOff += 6;
            vOff += ambiguous ? 4 : 0;
            segBuilder.build(this, idMap, 1 - ambFix, height, iOff, vOff);
        }
    }

    private int getTriangles(int neighborID) {
        int otherCase = QuadCases.getCase(neighbors[neighborID].data, 0, position);

        // if the other quad is case 15 this quad is totally visible, no need to patch
        if (otherCase == 15)
            return QuadCases.triangles[quadCase] | (ambiguous ? 0xf0 : 0);

        // if this quad is case 0 use the complement case of the other quad instead of patching
        if (quadCase == 0) {
            for (int i = 0; i < 6; i++) {
                patchSides[i] |= neighbors[neighborID].patchSides[i];
                patch |= patchSides[i];
            }
            return QuadCases.triangles[15 - otherCase];
        }

        // get base triangles for patching and add ambiguous case triangles:
        // - - - - 4 5 6 7 [ 1111 0000 ] 240 0xf0
        int tris = QuadCases.patchTriangles[quadCase] | (ambiguous ? 0xf0 : 0);
        int otherTris = QuadCases.patchTriangles[otherCase]
                | (neighbors[neighborID].ambiguous ? 0xf0 : 0);

        return mergeTriangles(tris & ~otherTris);
    }

    private int mergeTriangles(int patchTris) {
        int triangles;
        for (int i = 0; i < 4; i++)
            triangles = QuadCases.mergeTriangles2[i];
        //            if ((patchTris & triangles) == triangles)
        //                patchTris &= ~triangles;
        //                patchTris |= 1 << i + 8;

        if ((patchTris & 0xf0) != 0xf0)
            for (int i = 4; i < 8; i++) {
                triangles = QuadCases.mergeTriangles2[i];
                if ((patchTris & triangles) == triangles)
                    //                    patchTris &= ~triangles;
                    //                    patchTris |= 1 << i + 8;
                    return patchTris;
            }

        return patchTris;
    }

    private int getPatchVerts(int patchTris, int verts, int[] ids) {
        boolean rotate = false;
        int[] triangle;
        int triPair = 0x11, iOff = 0;
        int triPos = Integer.numberOfTrailingZeros(patchTris & 0xff00);
        int rotateTris = 0x0;
        int removeVerts = 0x0;

        triPos = 0x1 << 8;
        for (int i = 8; i < 12; i++) {
            if ((patchTris & triPos) != 0) {
                triangle = QuadCases.triangle[i];
                rotateTris |= triangle[4];

                removeVerts |= triangle[0] & 0xf0;
                verts |= triangle[0] & 0xf0f;

                ids[iOff++] = triangle[1];
                ids[iOff++] = triangle[2];
                ids[iOff++] = triangle[3];
            }
            triPos <<= 1;
        }

        triPos = 0x1;
        for (int i = 0; i < 8; i++) {
            triPair = i == 4 ? 0x11 : triPair;
            if ((patchTris & triPos) != 0) {
                triangle = QuadCases.triangle[i];

                verts |= triangle[0];

                ids[iOff++] = triangle[1];
                ids[iOff++] = triangle[2];

                rotate = (triPos & rotateTris) != 0 | (patchTris & triPair) == triPair;

                if (rotate)
                    ids[iOff++] = i < 4 ? 8 : i - 4;
                else ids[iOff++] = triangle[3];
            }
            triPos <<= 1;
            triPair <<= 1;
        }

        triPos = 0x1 << 12;
        for (int i = 12; i < 16; i++) {
            if ((patchTris & triPos) != 0) {
                triangle = QuadCases.triangle[i];

                verts |= triangle[0];

                ids[iOff++] = triangle[1];
                ids[iOff++] = triangle[2];
                ids[iOff++] = triangle[3];
            }
            triPos <<= 1;
        }

        return verts & ~removeVerts;
    }

    private int addOutlineVertices(int verts, int[] ids) {
        verts |= 1 << ids[0] << 9;
        verts |= 1 << ids[1] << 9;
        verts |= 1 << ids[0];
        verts |= 1 << ids[1];

        if (ambiguousCase) {
            verts |= 1 << ids[3] << 9;
            verts |= 1 << ids[4] << 9;
            verts |= 1 << ids[3];
            verts |= 1 << ids[4];
        }

        return verts;
    }

    private CopyOfVoxelQuadTree[] subdivide() {
        if (lod > 1)
            if (quadCase != 0) {
                CopyOfVoxelQuadTree[] trees = new CopyOfVoxelQuadTree[4];
                for (int i = 0; i < 4; i++)
                    trees[i] = new CopyOfVoxelQuadTree(i, this);
                return trees;
            } else
                if (QuadCases.getCase(neighbors[4].data, currentDepth, position) % 15 != 0
                || QuadCases.getCase(neighbors[5].data, currentDepth, position) % 15 != 0) {
                    CopyOfVoxelQuadTree[] trees = new CopyOfVoxelQuadTree[4];
                    for (int i = 0; i < 4; i++)
                        trees[i] = new CopyOfVoxelQuadTree(i, this);
                    return trees;
                }
        return null;
    }

    private void updateVisibility() {
        fillUp = false;
        fillDown = false;

        if (chunk != null)
            visible = quadCase != 15 ? chunk.inBound(x, y, z) : false;
            else visible = quadCase != 15 ? true : false;

        if (visible) {
            fillUp = QuadCases.getCase(neighbors[4].data, 0, position) != 0;
            fillDown = QuadCases.getCase(neighbors[5].data, 0, position) != 0;

            fillDown = false;
        }
    }

    //    private Vector3 createVertex(int index, float height) {
    //        Vector3 v;
    //
    //        switch (index) {
    //            case 0:
    //            case 9:
    //                v = new Vector3(x, y, z + size);
    //                break;
    //            case 1:
    //            case 10:
    //                v = new Vector3(x + size, y, z + size);
    //                break;
    //            case 2:
    //            case 11:
    //                v = new Vector3(x + size, y, z);
    //                break;
    //            case 3:
    //            case 12:
    //                v = new Vector3(x, y, z);
    //                break;
    //            case 4:
    //            case 13:
    //                v = new Vector3(x + size / 2, y, z + size);
    //                break;
    //            case 5:
    //            case 14:
    //                v = new Vector3(x + size, y, z + size / 2);
    //                break;
    //            case 6:
    //            case 15:
    //                v = new Vector3(x + size / 2, y, z);
    //                break;
    //            case 7:
    //            case 16:
    //                v = new Vector3(x, y, z + size / 2);
    //                break;
    //            case 8:
    //            case 17:
    //                v = new Vector3(x + size / 2, y, z + size / 2);
    //                break;
    //            default:
    //                return null;
    //        }
    //
    //        if (index > 8)
    //            v.setY(v.y - height);
    //        return v;
    //    }
    //
    //    private Vector3 createPatchVertex(int index, float height) {
    //        Vector3 v;
    //        CopyOfVoxelQuadTree quad;
    //
    //        switch (index) {
    //            case 0:
    //            case 9:
    //                v = new Vector3(x, y, z + size);
    //                break;
    //            case 1:
    //            case 10:
    //                v = new Vector3(x + size, y, z + size);
    //                break;
    //            case 2:
    //            case 11:
    //                v = new Vector3(x + size, y, z);
    //                break;
    //            case 3:
    //            case 12:
    //                v = new Vector3(x, y, z);
    //                break;
    //            case 4:
    //            case 13:
    //                quad = patchSides[0] ? getQuad(0) : this;
    //                v = new Vector3(quad.x + quad.size / 2, quad.y, quad.z + quad.size);
    //                break;
    //            case 5:
    //            case 14:
    //                quad = patchSides[1] ? getQuad(1) : this;
    //                v = new Vector3(quad.x + quad.size, quad.y, quad.z + quad.size / 2);
    //                break;
    //            case 6:
    //            case 15:
    //                quad = patchSides[2] ? getQuad(2) : this;
    //                v = new Vector3(quad.x + quad.size / 2, quad.y, quad.z);
    //                break;
    //            case 7:
    //            case 16:
    //                quad = patchSides[3] ? getQuad(3) : this;
    //                v = new Vector3(quad.x, quad.y, quad.z + quad.size / 2);
    //                break;
    //            case 8:
    //            case 17:
    //                v = new Vector3(x + size / 2, y, z + size / 2);
    //                break;
    //            default:
    //                return null;
    //        }
    //
    //        if (index > 8)
    //            v.setY(v.y - height);
    //        return v;
    //    }

    CopyOfVoxelQuadTree getQuad(int edgeID) {
        float[] data = null;
        CopyOfVoxelQuadTree q;

        int q0Pos = QuadCases.edgeQuads[edgeID][0];
        int q1Pos = QuadCases.edgeQuads[edgeID][1];

        // check later because they probably have to be handled independent
        if (quadCase == 0) {
            if (fillUp)
                data = neighbors[4].data;

            if (fillDown)
                data = neighbors[5].data;
        } else data = this.data;

        q = new CopyOfVoxelQuadTree(data, q0Pos, this);
        if (q.quadCase % 15 != 0 && QuadCases.neighbors[q.quadCase][edgeID])
            return q;

        q = new CopyOfVoxelQuadTree(data, q1Pos, this);
        if (q.quadCase % 15 != 0 && QuadCases.neighbors[q.quadCase][edgeID])
            return q;

        return this;
    }

    //    private Vector3 intersection(int pt1, int pt2, boolean interpolate) {
    //        Vector3 lineDir = vertices[pt2].sub(vertices[pt1]);
    //        float step = lineDir.length() / 2;
    //        if (interpolate)
    //            return lineDir.normL().multL(step).addL(vertices[pt1]);
    //        else return lineDir.normL().multL(step);
    //    }

    public void invert() {
        for (int i = 0; i < 4; i++)
            data[i] = -data[i];
        quadCase = 15 - quadCase;
    }

    public void complementCase(CopyOfVoxelQuadTree otherQuad) {
        if (otherQuad != null) {
            for (int i = 0; i < 5; i++)
                data[i] = -otherQuad.data[i];
            quadCase = 15 - otherQuad.quadCase;
        }
    }

    public boolean isLeaf() {
        return quadTree == null;
    }

    private String quadToString(int depth) {
        String str = "";
        String off = "";
        String pos = "";

        for (int i = 0; i < depth; i++)
            off += "    ";

        pos = " - [";
        for (int i : position)
            pos += " " + i;
        pos += " ]";

        str += "------------------------------------------------------------\n";

        if (isLeaf()) {
            str += off + "Leaf" + new Vector3(x, y, z) + " - " + currentDepth + pos + "\n";
            str += off + "Case: " + quadCase + ", Size: " + size + ", LOD: " + lod;
            str += ", ID: " + id + ", Visible: " + visible + "\n";
            str += off + "    VoxelValue(" + x + ", " + y + ", " + (z + size) + ") = " + data[0] + "\n";
            str += off + "    VoxelValue(" + (x + size) + ", " + y + ", " + (z + size) + ") = " + data[1] + "\n";
            str += off + "    VoxelValue(" + (x + size) + ", " + y + ", " + z + ") = " + data[2] + "\n";
            str += off + "    VoxelValue(" + x + ", " + y + ", " + z + ") = " + data[3] + "\n";
            str +=
                    off + "    Parent case " + (parent == null ? "none" : parent.quadCase)
                    + "\n";
            str += neighbors(off);

            if (chunk != null)
                str += chunk.toString();
        } else {
            str += off + "QuadTree" + new Vector3(x, y, z) + " - " + currentDepth + pos + "\n";
            str += off + "Case: " + quadCase + ", Size: " + size + ", LOD: " + lod;
            str += ", ID: " + id + ", Visible: " + visible + "\n";
            str += off + "    VoxelValue(" + x + ", " + y + ", " + (z + size) + ") = " + data[0] + "\n";
            str += off + "    VoxelValue(" + (x + size) + ", " + y + ", " + (z + size) + ") = " + data[1] + "\n";
            str += off + "    VoxelValue(" + (x + size) + ", " + y + ", " + z + ") = " + data[2] + "\n";
            str += off + "    VoxelValue(" + x + ", " + y + ", " + z + ") = " + data[3] + "\n";
            str += off + neighbors(off);
            for (int i = 0; i < 4; i++)
                if (quadTree[i] != null)
                    str += off + quadTree[i].quadToString(depth + 1);

            if (chunk != null)
                str += chunk.toString();
        }
        str += "------------------------------------------------------------\n";
        return str;
    }

    private String neighbors(String off) {
        String str = "";
        str += "Neighbors:\n";
        off += "    ";
        for (int i = 0; i < 6; i++)
            if (neighbors[i] != null) {
                str += off + "Quad"
                        + new Vector3(neighbors[i].x, neighbors[i].y, neighbors[i].z)
                + " - case " + neighbors[i].quadCase + ", size " + neighbors[i].size
                + " - " + neighbors[i].lod + ", " + neighbors[i].id + "\n";
                str += off + "    Case: " + neighbors[i].quadCase;
                str += ", Size: " + neighbors[i].size;
                str += ", LOD: " + neighbors[i].lod;
                str += ", ID: " + neighbors[i].id;
                str += ", Visible: " + neighbors[i].visible + "\n";
            } else str += "    none\n";
        return str;
    }

    @Override
    public String toString() {
        return quadToString(0);
    }

    //    public static long zip(int x, int y, int z, int faceCase, boolean bound, long zipKey) {
    //        long zippedFace = x * (zipKey * zipKey * QuadCases.caseCount)
    //                + y * (zipKey * QuadCases.caseCount)
    //                + z * QuadCases.caseCount + faceCase;
    //
    //        zippedFace++;
    //        if (bound)
    //            zippedFace = -zippedFace;
    //
    //        return zippedFace;
    //    }

    //    public static CopyOfVoxelQuadTree unzip(long zippedFace, long zipKey,
    //                                            CopyOfVoxelQuadTree quad) {
    //        long keySqr = zipKey * zipKey;
    //        if (quad == null)
    //            quad = new CopyOfVoxelQuadTree(0, 0, 0, 0, false, null);
    //
    //        if (Math.signum(zippedFace) == Math.signum(-1)) {
    //            quad.bound = true;
    //            zippedFace = -zippedFace;
    //        }
    //        zippedFace--;
    //
    //        quad.x = zippedFace / (keySqr * QuadCases.caseCount);
    //        zippedFace = zippedFace % (keySqr * QuadCases.caseCount);
    //
    //        quad.y = zippedFace / (zipKey * QuadCases.caseCount);
    //        zippedFace = zippedFace % (zipKey * QuadCases.caseCount);
    //
    //        quad.z = zippedFace / QuadCases.caseCount;
    //
    //        quad.quadCase = (int) zippedFace % QuadCases.caseCount;
    //        return quad;
    //    }
}
