package de.venjinx.core;


/**
 * <code>Vector3</code> defines a three dimensional vector using float values.
 * It provides the basic methods for adding, subtracting, multiplying and
 * dividing vectors. Furthermore methods for negating and normalizing a vector,
 * getting the length and the determinant of a vector and calculating
 * dot product, cross product and distance between two vectors are also
 * provided.
 *
 * @author Torge Rothe (X-Ray-Jin)
 */
public final class Vector3 {

    /**
     * This vectors x value.
     */
    public float x;

    /**
     * This vectors y value.
     */
    public float y;

    /**
     * This vectors z value.
     */
    public float z;

    /**
     * Creates a new Vector3 and initializes the x, y, z values with 0.
     */
    public Vector3() {
        x = y = z = 0;
    }

    /**
     * Creates a new Vector3 with the provided initial x, y, z values.
     *
     * @param x
     *            The initial x value.
     * @param y
     *            The initial y value.
     * @param z
     *            The initial z value.
     */
    public Vector3(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Creates a new Vector3 and initializes the x, y, z values with the
     * x, y, z values of the provided vector.
     *
     * @param copy
     *            The vector to copy the values from.
     */
    public Vector3(Vector3 copy) {
        x = copy.x;
        y = copy.y;
        z = copy.z;
    }

    /**
     * Sets this vectors x value to provided x value.
     *
     * @param newX
     *            The new x value.
     * @return This vector with the new x value.
     */
    public Vector3 setX(float newX) {
        x = newX;
        return this;
    }

    /**
     * Sets this vectors y value to the provided y value.
     *
     * @param newY
     *            The new y value.
     * @return This vector with the new y value.
     */
    public Vector3 setY(float newY) {
        y = newY;
        return this;
    }

    /**
     * Sets this vectors z value to the provided z value.
     *
     * @param newZ
     *            The new z value.
     * @return This vector with the new z value.
     */
    public Vector3 setZ(float newZ) {
        z = newZ;
        return this;
    }

    /**
     * Sets this vectors x, y, z values to given x, y, z values.
     *
     * @param newX
     *            The new x value.
     * @param newY
     *            The new y value.
     * @param newZ
     *            The new z value.
     * @return This vector with the new x, y, z values.
     */
    public Vector3 set(float newX, float newY, float newZ) {
        x = newX;
        y = newY;
        z = newZ;
        return this;
    }

    /**
     * Sets this vectors x, y, z values to the other vectors x, y, z values.
     *
     * @param copy
     *            The vector to copy the x, y, z values from.
     * @return This vector with the new x, y, z values.
     */
    public Vector3 set(Vector3 copy) {
        x = copy.x;
        y = copy.y;
        z = copy.z;
        return this;
    }

    /**
     * Adds the provided x, y, z values to this vectors x, y, z values and saves
     * the result in a newly created vector and returns the new vector.
     *
     * @param addX
     *            Value to add to x.
     * @param addY
     *            Value to add to y.
     * @param addZ
     *            Value to add to z.
     * @return A new vector with the added x, y, z values.
     */
    public Vector3 add(float addX, float addY, float addZ) {
        return new Vector3(x + addX, y + addY, z + addZ);
    }

    /**
     * Adds the provided vectors x, y, z values to this vectors x, y, z values
     * and saves the result in a newly created vector and returns the new
     * vector.
     *
     * @param other
     *            The vector to add to this vector.
     * @return A new vector with the added x, y, z values.
     */
    public Vector3 add(Vector3 other) {
        return new Vector3(x + other.x, y + other.y, z + other.z);
    }

    /**
     * Adds the provided x, y, z values to this vectors x, y, z values locally
     * and returns this vector.
     *
     * @param addX
     *            Value to add to x.
     * @param addY
     *            Value to add to y.
     * @param addZ
     *            Value to add to z.
     * @return This vector with the added x, y, z values.
     */
    public Vector3 addL(float addX, float addY, float addZ) {
        x += addX;
        y += addY;
        z += addZ;
        return this;
    }

    /**
     * Adds the provided vectors x, y, z values to this vectors x, y, z values
     * locally and returns this vector.
     *
     * @param other
     *            The vector to add to this vector.
     * @return This vector with the added x, y, z values.
     */
    public Vector3 addL(Vector3 other) {
        x += other.x;
        y += other.y;
        z += other.z;
        return this;
    }

    /**
     * Subtracts the provided x, y, z values from this vectors x, y, z values
     * and saves the result in a newly created vector and returns the new
     * vector.
     *
     * @param subX
     *            Value to subtract from x.
     * @param subY
     *            Value to subtract from y.
     * @param subZ
     *            Value to subtract from z.
     * @return A new vector with the subtracted x, y, z values.
     */
    public Vector3 sub(float subX, float subY, float subZ) {
        return new Vector3(x - subX, y - subY, z - subZ);
    }

    /**
     * Subtracts the provided vectors x, y, z values from this vectors x, y, z
     * values and saves the result in a newly created vector and returns the
     * new vector.
     *
     * @param other
     *            The vector to subtract from this vector.
     * @return A new vector with the subtracted x, y, z values.
     */
    public Vector3 sub(Vector3 other) {
        return new Vector3(x - other.x, y - other.y, z - other.z);
    }

    /**
     * Subtracts the provided x, y, z values from this vectors x, y, z values
     * locally and returns this vector.
     *
     * @param subX
     *            Value to subtract from x.
     * @param subY
     *            Value to subtract from y.
     * @param subZ
     *            Value to subtract from z.
     * @return This vector with the subtracted x, y, z values.
     */
    public Vector3 subL(float subX, float subY, float subZ) {
        x -= subX;
        y -= subY;
        z -= subZ;
        return this;
    }

    /**
     * Subtracts the provided vectors x, y, z values from this vectors x, y, z
     * values locally and returns this vector.
     *
     * @param other
     *            The vector to subtract from this vector.
     * @return This vector with the subtracted x, y, z values.
     */
    public Vector3 subL(Vector3 other) {
        x -= other.x;
        y -= other.y;
        z -= other.z;
        return this;
    }

    /**
     * Multiplies this vectors x, y, z values by a scalar and saves the result
     * in a newly created vector which is returned.
     *
     * @param scalar
     *            The value to multiply this vector by.
     * @return A new vector with the multiplied x, y, z values.
     */
    public Vector3 mult(float scalar) {
        return new Vector3(x * scalar, y * scalar, z * scalar);
    }

    /**
     * Multiplies this vectors x, y, z values by a provided vectors x, y, z
     * values and saves the result in a newly created vector and returns the
     * new vector.
     *
     * @param other
     *            The vector to multiply this vector by.
     * @return A new vector with the multiplied x, y, z values.
     */
    public Vector3 mult(Vector3 other) {
        x *= other.x;
        y *= other.y;
        z *= other.z;
        return this;
    }

    /**
     * Multiplies this vectors x, y, z values locally by a scalar and returns
     * this vector.
     *
     * @param scalar
     *            The value to multiply this vector by.
     * @return This vector with the multiplied x, y, z values.
     */
    public Vector3 multL(float scalar) {
        x *= scalar;
        y *= scalar;
        z *= scalar;
        return this;
    }

    /**
     * Multiplies this vectors x, y, z values by a provided vectors x, y, z
     * values locally and returns this vector.
     *
     * @param other
     *            The vector to multiply this vector by.
     * @return This vector with the multiplied x, y, z values.
     */
    public Vector3 multL(Vector3 other) {
        x *= other.x;
        y *= other.y;
        z *= other.z;
        return this;
    }

    /**
     * Divides this vectors x, y, z values by a scalar and saves the result
     * in a newly created vector which is returned.
     *
     * @param scalar
     *            The value to divide this vector by.
     * @return A new vector with the divided x, y, z values.
     */
    public Vector3 div(float scalar) {
        return new Vector3(x / scalar, y / scalar, z / scalar);
    }

    /**
     * Divides this vectors x, y, z values by a provided vectors x, y, z values
     * and saves the result in a newly created vector and returns the new
     * vector.
     *
     * @param other
     *            The vector to divide this vector by.
     * @return A new vector with the divided x, y, z values.
     */
    public Vector3 div(Vector3 other) {
        return new Vector3(x / other.x, y / other.y, z / other.z);
    }

    /**
     * Divides this vectors x, y, z values locally by a scalar and returns this
     * vector.
     *
     * @param scalar
     *            The value to divide this vector by.
     * @return This vector with the divided x, y, z values.
     */
    public Vector3 divL(float scalar) {
        x /= scalar;
        y /= scalar;
        z /= scalar;
        return this;
    }

    /**
     * Divides this vectors x, y, z values by a provided vectors x, y, z values
     * locally and returns this vector.
     *
     * @param other
     *            The vector to divide this vector by.
     * @return This vector with the divided x, y, z values.
     */
    public Vector3 divL(Vector3 other) {
        x /= other.x;
        y /= other.y;
        z /= other.z;
        return this;
    }

    /**
     * Negates this vectors x, y, z values and saves the result in a newly
     * created vector and returns the new vector.
     *
     * @return A new vector with the negated x, y, z values.
     */
    public Vector3 negate() {
        return new Vector3(-x, -y, -z);
    }

    /**
     * Negates this vectors x, y, z values locally and returns this vector.
     *
     * @return This vector with the negated x, y, z values.
     */
    public Vector3 negateL() {
        x = -x;
        y = -y;
        z = -z;
        return this;
    }

    /**
     * Normalizes this vectors values and saves the normalized values in a
     * newly created vector and returns the new unit vector.
     *
     * @return A new unit vector with the normalized x, y, z values.
     */
    public Vector3 norm() {
        float length = x * x + y * y + z * z;
        if (length != 1f && length != 0f) {
            length = 1f / (float) Math.sqrt(length);
            return new Vector3(x * length, y * length, z * length);
        }
        return new Vector3(this);
    }

    /**
     * Normalizes this vectors values locally and returns this vector as
     * unit vector.
     *
     * @return This unit vector with the normalized x, y, z values.
     */
    public Vector3 normL() {
        float length = x * x + y * y + z * z;
        if (length != 1f && length != 0f) {
            length = 1f / (float) Math.sqrt(length);
            x *= length;
            y *= length;
            z *= length;
        }
        return this;
    }

    /**
     * Calculates the dot product of this vector and a provided vector.
     *
     * @param other
     *            The vector to dot with this vector.
     * @return The result of the dot product.
     */
    public float dot(Vector3 other) {
        return x * other.x + y * other.y + z * other.z;
    }

    /**
     * Calculates the cross product of this vector and a provided vector and
     * saves the result in a newly created Vector3 and returns the new vector.
     * The result vector is perpendicular to this and the provided vector.
     *
     * @param other
     *            The vector to calculate the cross product with.
     * @return A new Vector3 which is perpendicular to this and the provided
     *         vector.
     */
    public Vector3 cross(Vector3 other) {
        return cross(other.x, other.y, other.z);
    }

    /**
     * Calculates the cross product of this vector and the provided x, y, z values
     * and saves the result in a newly created Vector3 and returns the new vector.
     * The result vector is perpendicular to this and the vector represented by
     * the given values.
     *
     * @param x
     *            The x value to cross with.
     * @param y
     *            The y value to cross with.
     * @param z
     *            The z value to cross with.
     * @return A new Vector3 which is perpendicular to this and the provided
     *         vector.
     */
    public Vector3 cross(float cx, float cy, float cz) {
        float resX = y * cz - z * cy;
        float resY = z * cx - x * cz;
        float resZ = x * cy - y * cx;
        return new Vector3(resX, resY, resZ);
    }

    /**
     * Calculates the length/magnitude of this vector.
     *
     * @return The length/magnitude of this vector.
     */
    public float length() {
        float length = x * x + y * y + z * z;
        return length != 1 && length != 0 ? (float) Math.sqrt(length) : length;
    }

    /**
     * Calculates the distance between this vector and a provided vector.
     *
     * @param other
     *            The vector to get the distance to.
     * @return The distance between this and the provided vector.
     */
    public float distance(Vector3 other) {
        float dx = x - other.x;
        float dy = y - other.y;
        float dz = z - other.z;
        return (float) Math.sqrt(dx * dx + dy * dy + dz * dz);
    }

    /**
     * Creates a new Vector and copies over the x, y, z values of this vector.
     *
     * @return A new Vector with the x, y, z values of this vector.
     */
    public Vector3 copy() {
        return new Vector3(this);
    }

    /**
     * Checks whether this and the provided object are equivalent, i.e if the
     * provided object is an instance of Vector3 and the x, y, z values of the
     * vectors are the same or they are the same object.
     *
     * @param o
     *            The object to compare with this vector.
     * @return True if they are equivalent or the same.
     */
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Vector3))
            return false;

        if (this == o)
            return true;

        Vector3 other = (Vector3) o;
        return Float.compare(x, other.x) == 0
                && Float.compare(y, other.y) == 0
                && Float.compare(z, other.z) == 0;
    }

    /**
     * Returns a string containing the textual representation of this vector.
     * Format: (valueX, valueY, valueZ)
     *
     * @return The string representation of this vector.
     */
    @Override
    public String toString() {
        return "(" + x + ", " + y + ", " + z + ")";
    }
}