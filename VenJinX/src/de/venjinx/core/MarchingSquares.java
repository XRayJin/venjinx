package de.venjinx.core;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import de.venjinx.core.VoxelQuadTree.QuadCases;

public class MarchingSquares {

    public static LinkedList<VoxelSurface> findSurfaces(int xOff, int yOff,
                                                        int zOff, int size,
                                                        HermiteData data, int lod,
                                                        boolean calcExact) {
        return findSurfaces(xOff, yOff, zOff, xOff + size, yOff + size, zOff + size,
                            data, lod, calcExact);
    }

    public static LinkedList<VoxelSurface> findSurfaces(int xOff, int yOff,
                                                        int zOff, int xBound,
                                                        int yBound, int zBound,
                                                        HermiteData data, int lod,
                                                        boolean calcExact) {
        //        lod = 2;
        LinkedList<VoxelSurface> surfaces = new LinkedList<>();
        int qCase, dataXBound, dataYBound, dataZBound;
        VoxelQuadTree quad;
        float[][][] dData = data.getDensityField();
        long key = dData.length * (int) Math.pow(2, lod + 1), qID;
        boolean bound;

        LinkedList<Long> idsToCheck = new LinkedList<>();
        HashSet<Long> checkedIDs = new HashSet<>();
        VoxelSurface s;
        dataXBound = data.getWidth();
        dataYBound = data.getHeight();
        dataZBound = data.getDepth();
        xBound -= xBound < dataXBound ? 0 : 1;
        yBound -= yBound < dataYBound ? 0 : 1;                                 // ?? check later
        zBound -= zBound < dataZBound ? 0 : 1;
        //        yOff = 4;
        //        yBound = 5;
        //        System.out.println("yOff = " + yOff + ", yBound" + yBound);
        //        System.out.println("dataYOff = " + dataYBound);
        for (int x = xOff; x < xBound; x++)
            for (int y = yOff; y < yBound; y++)
                for (int z = zOff; z < zBound; z++) {
                    qCase = QuadCases.getQuadCase(x, y, z, dData);
                    if (y + 1 < yBound)
                        qCase = QuadCases.getQuadCase(x, y + 1, z, dData) == 0 ? 15 : qCase;
                    if (qCase != 15) {
                        bound = x == xOff || x == xBound - 1
                                || y == yOff || y == yBound - 1
                                || z == zOff || z == zBound - 1;
                        if (qCase == 5) {
                            qCase = 7;
                            qID = generateQuadID(x, y, z, qCase, bound, key);
                            if (!checkedIDs.contains(qID)) {
                                s = new VoxelSurface(xOff, yOff, zOff,
                                                     xBound, yBound, zBound,
                                                     key, lod, data);

                                quad = new VoxelQuadTree(x, y, z, qCase, bound, dData);
                                quad.id = qID;
                                idsToCheck.add(qID);
                                s.addQuad(quad);

                                extract(s, idsToCheck, checkedIDs);
                                surfaces.add(s);
                            }
                            qCase = 13;
                        } else
                            if (qCase == 10) {
                                qCase = 14;
                                qID = generateQuadID(x, y, z, qCase, bound, key);
                                if (!checkedIDs.contains(qID)) {
                                    s = new VoxelSurface(xOff, yOff, zOff,
                                                         xBound, yBound, zBound,
                                                         key, lod, data);

                                    quad = new VoxelQuadTree(x, y, z, qCase,
                                                             bound, dData);
                                    quad.id = qID;
                                    idsToCheck.add(qID);
                                    s.addQuad(quad);

                                    extract(s, idsToCheck, checkedIDs);
                                    surfaces.add(s);
                                }
                                qCase = 11;
                            }
                        qID = generateQuadID(x, y, z, qCase, bound, key);
                        if (!checkedIDs.contains(qID)) {
                            s = new VoxelSurface(xOff, yOff, zOff,
                                                 xBound, yBound, zBound,
                                                 key, lod, data);

                            quad = new VoxelQuadTree(x, y, z, qCase, bound, dData);
                            quad.id = qID;
                            idsToCheck.add(qID);
                            s.addQuad(quad);

                            extract(s, idsToCheck, checkedIDs);
                            surfaces.add(s);
                        }
                    }
                }
        return surfaces;
    }

    private static void extract(VoxelSurface s, LinkedList<Long> idsToCheck,
                                Set<Long> checkedIDs) {
        VoxelQuadTree quad;

        int xMin = s.xMax, xMax = 0;
        int yMin = s.yMax, yMax = 0;
        int zMin = s.zMax, zMax = 0;

        while (!idsToCheck.isEmpty()) {
            quad = s.getQuad(idsToCheck.removeFirst());
            xMin = quad.x < xMin ? (int) quad.x : xMin;
            yMin = quad.y < yMin ? (int) quad.y : yMin;
            zMin = quad.z < zMin ? (int) quad.z : zMin;
            xMax = quad.x + 1 > xMax ? (int) quad.x + 1 : xMax;
            yMax = quad.y + 1 > yMax ? (int) quad.y + 1 : yMax;
            zMax = quad.z + 1 > zMax ? (int) quad.z + 1 : zMax;
            findNeighbors(quad, idsToCheck, checkedIDs);
        }

        //        s.xMin = xMin;
        //        s.yMin = yMin - 1;
        //        s.zMin = zMin;
        //
        //        s.xMax = xMax;
        //        s.yMax = yMax - 1;
        //        s.zMax = zMax;

        //        System.out.println("Surface ");
        //        for (CopyOfVoxelQuadTree quad : s.outlineQuads)
        //            System.out.println(quad);

        //        System.out.println("min " + xMin + ", " + yMin + ", " + zMin);
        //        System.out.println("max " + xMax + ", " + yMax + ", " + zMax);
        //        System.out.println("------------------------------------------------");
    }

    private static void findNeighbors(VoxelQuadTree q, LinkedList<Long> idsToCheck,
                                      Set<Long> checkedIDs) {

        VoxelSurface s = q.surface;
        float[][][] data = s.getData().getDensityField();
        long key = s.getZipKey();
        int xMin = s.getXMin(), xMax = s.getXMax();
        int yMin = s.getYMin(), yMax = s.getYMax();
        int zMin = s.getZMin(), zMax = s.getZMax();

        int x = (int) q.x;
        int y = (int) q.y;
        int z = (int) q.z;

        int n, nx, ny, nz, nqCase;
        boolean nBound;
        long nID;

        VoxelQuadTree neighbor;

        n = 0;
        if (checkedIDs.add(q.id)) {

            // bottom neighbor
            nz = z + 1;
            if (QuadCases.hasNeighbor(q.quadCase, n))
                if (nz < zMax) {
                    nBound = x == xMin || x == xMax - 1
                            || y == yMin || y == yMax - 1
                            || nz == zMax - 1;
                    nqCase = QuadCases.getQuadCase(x, y, nz, data);
                    if (y + 1 < yMax)
                        nqCase =
                        QuadCases.getQuadCase(x, y + 1, nz, data) == 0 ? 15
                                                                       : nqCase;
                    if (nqCase != 15) {
                        nqCase = nqCase == 5 ? 7 : nqCase == 10 ? 11 : nqCase;
                        nID = generateQuadID(x, y, nz, nqCase, nBound, key);

                        neighbor = s.getQuad(nID);
                        if (neighbor == null) {
                            neighbor = new VoxelQuadTree(x, y, nz, nqCase, nBound, data);
                            neighbor.id = nID;
                            idsToCheck.add(nID);

                            s.addQuad(neighbor);
                        }
                        q.neighbors[n] = neighbor;
                        neighbor.neighbors[n + 1] = q;
                    }
                    //                q.bound &= q.neighbors[n] == null;
                } else q.patchSides[0] = true;

            // top neighbor
            n++;
            nz = z - 1;
            if (QuadCases.hasNeighbor(q.quadCase, n))
                if (nz >= zMin) {
                    nBound = x == xMin || x == xMax - 1
                            || y == yMin || y == yMax - 1
                            || nz == zMin;
                    nqCase = QuadCases.getQuadCase(x, y, nz, data);
                    if (y + 1 < yMax)
                        nqCase =
                        QuadCases.getQuadCase(x, y + 1, nz, data) == 0 ? 15
                                                                       : nqCase;
                    if (nqCase != 15) {
                        nqCase = nqCase == 5 ? 13 : nqCase == 10 ? 14 : nqCase;
                        nID = generateQuadID(x, y, nz, nqCase, nBound, key);

                        neighbor = s.getQuad(nID);
                        if (neighbor == null) {
                            neighbor = new VoxelQuadTree(x, y, nz, nqCase, nBound, data);
                            neighbor.id = nID;
                            idsToCheck.add(nID);
                            s.addQuad(neighbor);
                        }
                        q.neighbors[n] = neighbor;
                        neighbor.neighbors[n - 1] = q;
                    }
                    //                q.bound &= q.neighbors[n] == null;
                } else q.patchSides[1] = true;

            // right neighbor
            n++;
            nx = x + 1;
            if (QuadCases.hasNeighbor(q.quadCase, n))
                if (nx < xMax) {
                    nBound = nx == xMax - 1
                            || y == yMin || y == yMax - 1
                            || z == zMin || z == zMax - 1;
                    nqCase = QuadCases.getQuadCase(nx, y, z, data);
                    if (y + 1 < yMax)
                        nqCase =
                        QuadCases.getQuadCase(nx, y + 1, z, data) == 0 ? 15
                                                                       : nqCase;
                    if (nqCase != 15) {
                        nqCase = nqCase == 5 ? 7 : nqCase == 10 ? 14 : nqCase;
                        nID = generateQuadID(nx, y, z, nqCase, nBound, key);

                        neighbor = s.getQuad(nID);
                        if (neighbor == null) {
                            neighbor = new VoxelQuadTree(nx, y, z, nqCase, nBound, data);
                            neighbor.id = nID;
                            idsToCheck.add(nID);
                            s.addQuad(neighbor);
                        }
                        q.neighbors[n] = neighbor;
                        neighbor.neighbors[n + 1] = q;
                    }
                    //                q.bound &= q.neighbors[n] == null;
                } else q.patchSides[2] = true;

            // left neighbor
            n++;
            nx = x - 1;
            if (QuadCases.hasNeighbor(q.quadCase, n))
                if (nx >= xMin) {
                    nBound = nx == xMin
                            || y == yMin || y == yMax - 1
                            || z == zMin || z == zMax - 1;
                    nqCase = QuadCases.getQuadCase(nx, y, z, data);
                    if (y + 1 < yMax)
                        nqCase =
                        QuadCases.getQuadCase(nx, y + 1, z, data) == 0 ? 15
                                                                       : nqCase;
                    if (nqCase != 15) {
                        nqCase = nqCase == 5 ? 13 : nqCase == 10 ? 11 : nqCase;
                        nID = generateQuadID(nx, y, z, nqCase, nBound, key);

                        neighbor = s.getQuad(nID);
                        if (neighbor == null) {
                            neighbor = new VoxelQuadTree(nx, y, z, nqCase, nBound, data);
                            neighbor.id = nID;
                            idsToCheck.add(nID);
                            s.addQuad(neighbor);
                        }
                        q.neighbors[n] = neighbor;
                        neighbor.neighbors[n - 1] = q;
                    }
                    //                q.bound &= q.neighbors[n] == null;
                } else q.patchSides[3] = true;

            // above neighbor
            n++;
            ny = y + 1;
            if (ny < yMax) {
                nBound = x == xMin || x == xMax - 1
                        || ny == yMax
                        || z == zMin || z == zMax - 1;
                nqCase = QuadCases.getQuadCase(x, ny, z, data);
                if (ny + 1 < yMax)
                    nqCase = QuadCases.getQuadCase(x, ny + 1, z, data) == 0 ? 15 : nqCase;

                if (nqCase != 15) {
                    nqCase = nqCase == 5 ? 13 : nqCase == 10 ? 11 : nqCase;
                    nID = generateQuadID(x, ny, z, nqCase, nBound, key);

                    neighbor = s.getQuad(nID);
                    if (neighbor == null) {
                        neighbor = new VoxelQuadTree(x, ny, z, nqCase, nBound, data);
                        neighbor.id = nID;
                        idsToCheck.add(nID);
                        s.addQuad(neighbor);
                    }
                    q.neighbors[n] = neighbor;
                    neighbor.neighbors[n + 1] = q;
                }
            } else q.patchSides[4] = true;

            // below neighbor
            n++;
            ny = y - 1;
            if (ny >= yMin) {
                nBound = x == xMin || x == xMax - 1
                        || ny == yMin
                        || z == zMin || z == zMax - 1;
                nqCase = QuadCases.getQuadCase(x, ny, z, data);
                nqCase = QuadCases.getQuadCase(x, ny + 1, z, data) == 0 ? 15 : nqCase;
                if (nqCase != 15) {
                    nqCase = nqCase == 5 ? 13 : nqCase == 10 ? 11 : nqCase;
                    nID = generateQuadID(x, ny, z, nqCase, nBound, key);

                    neighbor = s.getQuad(nID);
                    if (neighbor == null) {
                        neighbor = new VoxelQuadTree(x, ny, z, nqCase, nBound, data);
                        neighbor.id = nID;
                        idsToCheck.add(nID);
                        s.addQuad(neighbor);
                    }
                    q.neighbors[n] = neighbor;
                    neighbor.neighbors[n - 1] = q;
                }
            } else q.patchSides[5] = true;
        }
    }

    private static long generateQuadID(int x, int y, int z, int quadCase,
                                       boolean bound, long key) {
        long zippedFace = x * (key * key * QuadCases.caseCount())
                + y * (key * QuadCases.caseCount())
                + z * QuadCases.caseCount() + quadCase;

        zippedFace++;
        if (bound)
            zippedFace = -zippedFace;

        return zippedFace;
    }
}