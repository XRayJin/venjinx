package de.venjinx.core;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

import de.venjinx.core.CopyOfVoxelQuadTree.QuadCases;

public class CopyOfMarchingSquares {

    public static LinkedList<HashMap<Long, CopyOfVoxelQuadTree>>
    findSurfaces(CopyOfVoxelChunk chunk) {
        return findSurfaces(chunk, chunk.yMin, chunk.yMax - chunk.yMin);
    }

    public static LinkedList<HashMap<Long, CopyOfVoxelQuadTree>>
    findSurfaces(CopyOfVoxelChunk chunk, int yOff, int height) {
        LinkedList<HashMap<Long, CopyOfVoxelQuadTree>> meshParts = new LinkedList<>();

        int yMax = yOff + height;

        int qCase;
        long id;
        CopyOfVoxelQuadTree quad;
        float[][][] dData = chunk.data.getDensityField();

        LinkedList<Long> idsToCheck = new LinkedList<>();
        HashSet<Long> checkedIDs = new HashSet<>();
        HashMap<Long, CopyOfVoxelQuadTree> quads;
        int maxHeight = chunk.data.getHeight(), maxDepth = chunk.data.getDepth();
        for (int x = chunk.xMin; x < chunk.xMax; x++)
            for (int y = yOff; y < yMax; y++)
                for (int z = chunk.zMin; z < chunk.zMax; z++) {
                    qCase = QuadCases.getCase(x, y, z, dData);

                    if (qCase != 15) {
                        id = QuadCases.generateQuadID(x, y, z, qCase,
                                                      chunk.isBound(x, y, z),
                                                      maxHeight, maxDepth);

                        if (checkedIDs.add(id)) {
                            quads = new HashMap<>();
                            quad = new CopyOfVoxelQuadTree(x, y, z, chunk);
                            quad.findNeighbors(idsToCheck, quads);

                            quads.put(quad.id, quad);

                            while (!idsToCheck.isEmpty()) {
                                quad = quads.get(idsToCheck.removeFirst());

                                if (checkedIDs.add(quad.id))
                                    quad.findNeighbors(idsToCheck, quads);
                            }
                            meshParts.add(quads);
                        }
                    }
                }
        return meshParts;
    }
}