package de.venjinx.core;

/**
 * <code>Vector2</code> defines a two dimensional vector using float values. It
 * provides the basic methods for adding, subtracting, multiplying and dividing
 * vectors. Furthermore methods for negating and normalizing a vector, getting
 * the length and the determinant of a vector and calculating dot product, cross
 * product and distance between two vectors are also provided.
 * 
 * @author Torge Rothe (X-Ray-Jin)
 */
public final class Vector2 {

    /**
     * This vectors x value.
     */
    public float x;

    /**
     * This vectors y value.
     */
    public float y;

    /**
     * Creates a new Vector2 and initializes the x, y values with 0.
     */
    public Vector2() {
        x = y = 0;
    }

    /**
     * Creates a new Vector2 with the provided initial x, y values.
     * 
     * @param x
     *            The initial x value.
     * @param y
     *            The initial y value.
     */
    public Vector2(float x, float y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Creates a new Vector2 and initializes the x, y values with the
     * x, y values of the provided vector.
     * 
     * @param copy
     *            The vector to copy the values from.
     */
    public Vector2(Vector2 copy) {
        x = copy.x;
        y = copy.y;
    }

    /**
     * Sets this vectors x value to provided x value.
     * 
     * @param newX
     *            The new x value.
     * @return This vector with the new x value.
     */
    public Vector2 setX(float newX) {
        x = newX;
        return this;
    }

    /**
     * Sets this vectors y value to the provided y value.
     * 
     * @param newY
     *            The new y value.
     * @return This vector with the new y value.
     */
    public Vector2 setY(float newY) {
        y = newY;
        return this;
    }

    /**
     * Sets this vectors x, y values to given x, y values.
     * 
     * @param newX
     *            The new x value.
     * @param newY
     *            The new y value.
     * @return This vector with the new x, y values.
     */
    public Vector2 set(float newX, float newY) {
        x = newX;
        y = newY;
        return this;
    }

    /**
     * Sets this vectors x, y values to the other vectors x, y values.
     * 
     * @param copy
     *            The vector to copy the x, y values from.
     * @return This vector with the new x, y values.
     */
    public Vector2 set(Vector2 copy) {
        x = copy.x;
        y = copy.y;
        return this;
    }

    /**
     * Adds the provided x, y values to this vectors x, y values and saves the
     * result in a newly created vector and returns the new vector.
     * 
     * @param addX
     *            Value to add to x.
     * @param addY
     *            Value to add to y.
     * @return A new vector with the added x, y values.
     */
    public Vector2 add(float addX, float addY) {
        return new Vector2(x + addX, y + addY);
    }

    /**
     * Adds the provided vectors x, y values to this vectors x, y values and
     * saves the result in a newly created vector and returns the new vector.
     * 
     * @param other
     *            The vector to add to this vector.
     * @return A new vector with the added x, y values.
     */
    public Vector2 add(Vector2 other) {
        return new Vector2(x + other.x, y + other.y);
    }

    /**
     * Adds the provided x, y values to this vectors x, y values locally and
     * returns this vector.
     * 
     * @param addX
     *            Value to add to x.
     * @param addY
     *            Value to add to y.
     * @return This vector with the added x, y values.
     */
    public Vector2 addL(float addX, float addY) {
        x += addX;
        y += addY;
        return this;
    }

    /**
     * Adds the provided vectors x, y values to this vectors x, y values locally
     * and returns this vector.
     * 
     * @param other
     *            The vector to add to this vector.
     * @return This vector with the added x, y values.
     */
    public Vector2 addL(Vector2 other) {
        x += other.x;
        y += other.y;
        return this;
    }

    /**
     * Subtracts the provided x, y values from this vectors x, y values and
     * saves the result in a newly created vector and returns the new vector.
     * 
     * @param subX
     *            Value to subtract from x.
     * @param subY
     *            Value to subtract from y.
     * @return A new vector with the subtracted x, y values.
     */
    public Vector2 sub(float subX, float subY) {
        return new Vector2(x - subX, y - subY);
    }

    /**
     * Subtracts the provided vectors x, y values from this vectors x, y values
     * and saves the result in a newly created vector and returns the new
     * vector.
     * 
     * @param other
     *            The vector to subtract from this vector.
     * @return A new vector with the subtracted x, y values.
     */
    public Vector2 sub(Vector2 other) {
        return new Vector2(x - other.x, y - other.y);
    }

    /**
     * Subtracts the provided x, y values from this vectors x, y values locally
     * and returns this vector.
     * 
     * @param subX
     *            Value to subtract from x.
     * @param subY
     *            Value to subtract from y.
     * @return This vector with the subtracted x, y values.
     */
    public Vector2 subL(float subX, float subY) {
        x -= subX;
        y -= subY;
        return this;
    }

    /**
     * Subtracts the provided vectors x, y values from this vectors x, y values
     * locally and returns this vector.
     * 
     * @param other
     *            The vector to subtract from this vector.
     * @return This vector with the subtracted x, y values.
     */
    public Vector2 subL(Vector2 other) {
        x -= other.x;
        y -= other.y;
        return this;
    }

    /**
     * Multiplies this vectors x, y values by a scalar and saves the result
     * in a newly created vector which is returned.
     * 
     * @param scalar
     *            The value to multiply this vector by.
     * @return A new vector with the multiplied x, y values.
     */
    public Vector2 mult(float scalar) {
        return new Vector2(x * scalar, y * scalar);
    }

    /**
     * Multiplies this vectors x, y values by a provided vectors x, y values
     * and saves the result in a newly created vector and returns the new
     * vector.
     * 
     * @param other
     *            The vector to multiply this vector by.
     * @return A new vector with the multiplied x, y values.
     */
    public Vector2 mult(Vector2 other) {
        x *= other.x;
        y *= other.y;
        return this;
    }

    /**
     * Multiplies this vectors x, y values locally by a scalar and returns this
     * vector.
     * 
     * @param scalar
     *            The value to multiply this vector by.
     * @return This vector with the multiplied x, y values.
     */
    public Vector2 multL(float scalar) {
        x *= scalar;
        y *= scalar;
        return this;
    }

    /**
     * Multiplies this vectors x, y values by a provided vectors x, y values
     * locally and returns this vector.
     * 
     * @param other
     *            The vector to multiply this vector by.
     * @return This vector with the multiplied x, y values.
     */
    public Vector2 multL(Vector2 other) {
        x *= other.x;
        y *= other.y;
        return this;
    }

    /**
     * Divides this vectors x, y values by a scalar and saves the result
     * in a newly created vector which is returned.
     * 
     * @param scalar
     *            The value to divide this vector by.
     * @return A new vector with the divided x, y values.
     */
    public Vector2 div(float scalar) {
        return new Vector2(x / scalar, y / scalar);
    }

    /**
     * Divides this vectors x, y values by a provided vectors x, y values
     * and saves the result in a newly created vector and returns the new
     * vector.
     * 
     * @param other
     *            The vector to divide this vector by.
     * @return A new vector with the divided x, y values.
     */
    public Vector2 div(Vector2 other) {
        return new Vector2(x / other.x, y / other.y);
    }

    /**
     * Divides this vectors x, y values locally by a scalar and returns this
     * vector.
     * 
     * @param scalar
     *            The value to divide this vector by.
     * @return This vector with the divided x, y values.
     */
    public Vector2 divL(float scalar) {
        x /= scalar;
        y /= scalar;
        return this;
    }

    /**
     * Divides this vectors x, y values by a provided vectors x, y values
     * locally and returns this vector.
     * 
     * @param other
     *            The vector to divide this vector by.
     * @return This vector with the divided x, y values.
     */
    public Vector2 divL(Vector2 other) {
        x /= other.x;
        y /= other.y;
        return this;
    }

    /**
     * Negates this vectors x, y values and saves the result in a newly created
     * vector and returns the new vector.
     * 
     * @return A new vector with the negated x, y values.
     */
    public Vector2 negate() {
        return new Vector2(-x, -y);
    }

    /**
     * Negates this vectors x, y values locally and returns this vector.
     * 
     * @return This vector with the negated x, y values.
     */
    public Vector2 negateL() {
        x = -x;
        y = -y;
        return this;
    }

    /**
     * Normalizes this vectors values and saves the normalized values in a
     * newly created vector and returns the new unit vector.
     * 
     * @return A new unit vector with the normalized x, y values.
     */
    public Vector2 norm() {
        float length = x * x + y * y;
        if (length != 1f && length != 0f) {
            length = 1f / (float) Math.sqrt(length);
            return new Vector2(x * length, y * length);
        }
        return new Vector2(this);
    }

    /**
     * Normalizes this vectors values locally and returns this vector as
     * unit vector.
     * 
     * @return This unit vector with the normalized x, y values.
     */
    public Vector2 normL() {
        float length = x * x + y * y;
        if (length != 1f && length != 0f) {
            length = 1f / (float) Math.sqrt(length);
            x *= length;
            y *= length;
        }
        return this;
    }

    /**
     * Calculates the dot product of this vector and a provided vector.
     * 
     * @param other
     *            The vector to dot with this vector.
     * @return The result of the dot product.
     */
    public float dot(Vector2 other) {
        return x * other.x + y * other.y;
    }

    /**
     * Calculates the cross product of this vector and a provided vector and
     * saves the result in a newly created Vector3 and returns the new vector.
     * The result vector is perpendicular to this and the provided vector.
     * 
     * @param other
     *            The vector to calculate the cross product with.
     * @return A new Vector3 which is perpendicular to this and the provided
     *         vector.
     */
    public Vector3 cross(Vector2 other) {
        return new Vector3(0, 0, det(other));
    }

    /**
     * Calculates the determinant of this vector and a provided vector and
     * returns the result.
     * 
     * @param other
     *            The vector to calculate the determinant with.
     * @return The determinant of this and the provided vector.
     */
    public float det(Vector2 other) {
        return x * other.y - y * other.x;
    }

    /**
     * Calculates the length/magnitude of this vector.
     * 
     * @return The length/magnitude of this vector.
     */
    public float length() {
        float length = x * x + y * y;
        return length != 1 && length != 0 ? (float) Math.sqrt(length) : length;
    }

    /**
     * Calculates the distance between this vector and a provided vector.
     * 
     * @param other
     *            The vector to get the distance to.
     * @return The distance between this and the provided vector.
     */
    public float distance(Vector2 other) {
        float dx = x - other.x;
        float dy = y - other.y;
        return (float) Math.sqrt(dx * dx + dy * dy);
    }

    /**
     * Creates a new Vector and copies over the x, y, z values of this vector.
     * 
     * @return A new Vector with the x, y, z values of this vector.
     */
    public Vector2 copy() {
        return new Vector2(this);
    }

    /**
     * Checks whether this and the provided object are equivalent, i.e if the
     * provided object is an instance of Vector2 and the x, y values of the
     * vectors are the same or they are the same object.
     * 
     * @param o
     *            The object to compare with this vector.
     * @return True if they are equivalent or the same.
     */
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Vector2))
            return false;

        if (this == o)
            return true;

        Vector2 other = (Vector2) o;
        return Float.compare(x, other.x) == 0
                && Float.compare(y, other.y) == 0;
    }

    /**
     * Returns a string containing the textual representation of this vector.
     * Format: (valueX, valueY)
     * 
     * @return The string representation of this vector.
     */
    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}