package de.venjinx.core;

import java.util.LinkedList;
import java.util.concurrent.Callable;

public final class VoxelChunk implements Callable<VoxelChunk> {

    private VoxelObject vObject;
    private int lod = -1;
    private final int xMin, yMin, zMin, xMax, yMax, zMax;
    //    private Vector3 center;

    private VoxelChunk[][][] ocTree;
    private boolean newSurfaces = false;
    private boolean needsGeneration = false;
    private boolean[] patchSides = new boolean[6];

    private LinkedList<VoxelSurface> surfaces = new LinkedList<>();

    private long generateTime = 0L;
    private int vertexCount = 0;

    public VoxelChunk(int xOff, int yOff, int zOff, VoxelObject vObject) {
        this.vObject = vObject;

        xMin = xOff;
        yMin = yOff;
        zMin = zOff;

        int size = vObject.chunkSize;
        xMax = xMin + size;
        yMax = yMin + size;
        zMax = zMin + size;

        //        center = new Vector3(xMin + size / 2f, yMin + size / 2f, zMin + size / 2f);
    }

    public LinkedList<VoxelSurface> getSurfaces() {
        newSurfaces = false;
        return surfaces;
    }

    boolean update(int chunkDistance) {
        // double distance = center.distance(currentRefPoint);
        // int newLOD = 10 - Util.log2(Util.matchPowerOfTwo((int) distance));
        int newLOD = vObject.maxLOD - chunkDistance;
        newLOD = newLOD > vObject.maxLOD ? vObject.maxLOD : newLOD < 1 ? 1 : newLOD;
        if (lod != newLOD) {
            // System.out.print("dist " + distance);
            // System.out.print(", powerOfTwo " + Util.matchPowerOfTwo((int)
            // distance));
            // System.out.print(", log2 " + Util.log2(Util.matchPowerOfTwo((int)
            // distance)));
            // System.out.println(", new lod " + newLOD);
            lod = newLOD;
            needsGeneration = true;
        }
        return needsGeneration;
    }

    public boolean[] getSidesToPatch() {
        return patchSides;
    }

    void setSideToPatch(int sideID, boolean patch) {
        patchSides[sideID] = patch;
    }

    private void generate() {
        try {
            LinkedList<VoxelSurface> surfaces;
            newSurfaces = false;

            if (vObject.extractionLvl >= 0 && vObject.extractionLvl < yMax)
                surfaces = MarchingSquares.findSurfaces(xMin, yMin + vObject.extractionLvl,
                                                        zMin, xMax,
                                                        yMin + vObject.extractionLvl + 1,
                                                        zMax, vObject.data, lod, false);
            else surfaces = MarchingSquares.findSurfaces(xMin, yMin, zMin,
                                                         xMax, yMax + 1, zMax,
                                                         vObject.data, lod, false);


            System.out.println(xMin / 4 + ", " + yMin / 4 + ", " + zMin / 4 + ", "
                    + surfaces.size());

            vertexCount = 0;
            for (VoxelSurface s : surfaces) {
                s.generate(vObject.lvlHeight, vObject.mode, true, patchSides);
                vertexCount += s.getVertices().capacity() / 3;
            }

            this.surfaces = surfaces;
            newSurfaces = surfaces.size() > 0;
            needsGeneration = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean hasNewSurfaces() {
        return newSurfaces;
    }

    public int getLOD() {
        return lod;
    }

    public Vector3 getMin() {
        return new Vector3(xMin, yMin, zMin);
    }

    public Vector3 getMax() {
        return new Vector3(xMax, yMax, zMax);
    }

    //    public Vector3 getCenter() {
    //        return center;
    //    }

    public int getVertextCount() {
        return vertexCount;
    }

    public long getGenerateTime() {
        return generateTime;
    }

    @Override
    public VoxelChunk call() throws Exception {
        generateTime = System.nanoTime();
        generate();
        generateTime = System.nanoTime() - generateTime;
        return this;
    }

    private String treeToString(int depth) {
        String str = "";

        for (int i = 0; i < depth; i++)
            str += "    ";

        str += "VoxelChunk(" + xMin + ", " + yMin + ", " + zMin + ")";
        str += " - (" + xMax + ", " + yMax + ", " + zMax + ")";
        str += ", " + vObject.chunkSize + ", " + lod + ", " + vObject.mode;
        str += " - " + newSurfaces + "\n";
        if (ocTree != null)
            for (int x = 0; x < 2; x++)
                for (int y = 0; y < 2; y++)
                    for (int z = 0; z < 2; z++)
                        str += ocTree[x][y][z].treeToString(depth + 1);

        return str;
    }

    @Override
    public String toString() {
        return treeToString(0);
    }

    // public void mergeSurfaces() {
    // Surface surface, otherSurface;
    // Iterator<Long> iter;
    // double vID;
    // boolean merged = false;
    //
    // for (int i = 0; i < surfaceParts.size(); i++) {
    // surface = surfaceParts.removeFirst();
    //
    // while (!surface.isComplete())
    // for (int j = 0; j < surfaceParts.size(); j++) {
    // otherSurface = surfaceParts.removeFirst();
    // merged = false;
    // iter = surface.getSufaceBoundVerts().iterator();
    // while (iter.hasNext()) {
    // vID = iter.next();
    // if (otherSurface.getSufaceBoundVerts().contains(vID)) {
    // if (surface.vertCount() >= otherSurface.vertCount())
    // merged = surface.merge(otherSurface);
    // else {
    // merged = otherSurface.merge(surface);
    // surface = otherSurface;
    // }
    // j--;
    // break;
    // }
    // }
    // if (!merged)
    // surfaceParts.add(otherSurface);
    // }
    // surfaces.add(surface);
    // i--;
    // }
    // }
}
