package de.venjinx.core;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class VoxelMeshData {
    private Set<Long> boundVerts = new HashSet<>();

    private HashMap<Long, Integer> idToIndex = new HashMap<>();
    private ArrayList<Integer> indexes = new ArrayList<>();
    private ArrayList<Vector3> vertices = new ArrayList<>();
    private ArrayList<Vector3> normals = new ArrayList<>();
    private ArrayList<Vector4> tangents = new ArrayList<>();
    private ArrayList<Vector3> binormals = new ArrayList<>();
    private ArrayList<Vector2> texCoords = new ArrayList<>();

    private IntBuffer indexBuffer;
    private FloatBuffer vertexBuffer;
    private FloatBuffer normalBuffer;
    private FloatBuffer tangentBuffer;
    private FloatBuffer binormalBuffer;
    private FloatBuffer texCoordsBuffer;

    private final int dataYBound, dataZBound, maxLOD;
    private Vector3 min, max;
    private int vertexCount = 0;

    public VoxelMeshData(Vector3 min, Vector3 max,
                         int maxLOD, int dataYBound, int dataZBound) {
        this.min = min;
        this.max = max;
        this.maxLOD = maxLOD;
        this.dataYBound = dataYBound * (int) Math.pow(2, maxLOD) - 1;
        this.dataZBound = dataZBound * (int) Math.pow(2, maxLOD) - 1;
    }

    public long addTree(CopyOfVoxelQuadTree quad, boolean smooth) {
        long t = System.nanoTime();

        //        smooth = false;

        if (quad.visible)
            if (quad.isLeaf())
                addLeaf(quad, smooth);
            else for (CopyOfVoxelQuadTree q : quad.quadTree)
                addTree(q, smooth);
        return System.nanoTime() - t;
    }

    private void addLeaf(CopyOfVoxelQuadTree leaf, boolean smooth) {
        Vector3 vert, norm, v = new Vector3();
        Vector4 tang;
        long vID;
        int index;

        int[] tIndexes = leaf.tIndexes;
        int[] newIndexes = new int[tIndexes.length];
        Vector3[] tVerts = leaf.tVerts;
        Vector3[] tNorms = leaf.tNorms;
        Vector4[] tTangs = leaf.tTangs;

        if (smooth) {
            for (int i = 0; i < tVerts.length; i++) {
                vert = tVerts[i];
                //                norm = tNorms[i];y
                vID = generateVertexID(vert);

                if (idToIndex.containsKey(vID))
                    index = idToIndex.get(vID);
                //                    norm = normals.get(index);
                //                    tang = tangents.get(index);
                //                    norm.addL(tNorms[i]).normL();
                //                    tang.addL(tTangs[i].x, tTangs[i].y,
                //                              tTangs[i].z, 0).normL();
                else {
                    norm = leaf.chunk.data.getNormal(vert);
                    v.set(norm.x, 0, norm.z);
                    v = v.cross(0, 1, 0);

                    v = norm.cross(v);
                    tang = new Vector4(v, 1);

                    index = vertices.size();
                    idToIndex.put(vID, index);
                    tang = tTangs[i];
                    v.set(tang.x, tang.y, tang.z);

                    //                    if (leaf.isBoundQuad())
                    //                        if (vert.x == min.x || vert.x == max.x
                    //                        || vert.z == min.z || vert.z == max.z)
                    //                            boundVerts.add(vID);

                    vertices.add(vert);
                    normals.add(norm);
                    tangents.add(tang);
                    binormals.add(norm.cross(v).normL());
                    texCoords.add(new Vector2(vert.x / max.x, 1 - vert.z / max.z));
                }

                for (int j = 0; j < tIndexes.length; j++)
                    if (tIndexes[j] == i)
                        newIndexes[j] = index;
            }

            for (int i : newIndexes)
                indexes.add(i);
        } else {
            int off = vertices.size();
            for (int i : tIndexes)
                indexes.add(i + off);

            for (int i = 0; i < tVerts.length; i++) {
                tang = tTangs[i];
                v.set(tang.x, tang.y, tang.z);

                vertices.add(tVerts[i]);
                normals.add(tNorms[i]);
                tangents.add(tang);
                binormals.add(tNorms[i].cross(v).normL());
                texCoords.add(new Vector2(tVerts[i].x / max.x, 1 - tVerts[i].z / max.z));
            }
        }
    }

    public void createBuffers() {
        ByteBuffer b;
        vertexCount = vertices.size();

        b = ByteBuffer.allocateDirect(4 * indexes.size());
        indexBuffer = b.order(ByteOrder.nativeOrder()).asIntBuffer();
        indexBuffer.clear();
        int[] tIndexes = new int[indexes.size()];
        for (int i = 0; i < tIndexes.length; i++)
            tIndexes[i] = indexes.remove(0);
        indexBuffer.put(tIndexes);
        indexBuffer.flip();

        b = ByteBuffer.allocateDirect(4 * vertices.size() * 3);
        vertexBuffer = b.order(ByteOrder.nativeOrder()).asFloatBuffer();
        vertexBuffer.clear();
        for (Vector3 v3 : vertices)
            if (v3 != null)
                vertexBuffer.put(v3.x).put(v3.y).put(v3.z);
            else vertexBuffer.put(0).put(0).put(0);
        vertexBuffer.flip();

        b = ByteBuffer.allocateDirect(4 * normals.size() * 3);
        normalBuffer = b.order(ByteOrder.nativeOrder()).asFloatBuffer();
        normalBuffer.clear();
        for (Vector3 v3 : normals)
            if (v3 != null)
                normalBuffer.put(v3.x).put(v3.y).put(v3.z);
            else normalBuffer.put(0).put(0).put(0);
        normalBuffer.flip();

        b = ByteBuffer.allocateDirect(4 * tangents.size() * 4);
        tangentBuffer = b.order(ByteOrder.nativeOrder()).asFloatBuffer();
        tangentBuffer.clear();
        for (Vector4 v4 : tangents)
            if (v4 != null)
                tangentBuffer.put(v4.x).put(v4.y).put(v4.z).put(v4.w);
            else tangentBuffer.put(0).put(0).put(0).put(0);
        tangentBuffer.flip();

        b = ByteBuffer.allocateDirect(4 * binormals.size() * 3);
        binormalBuffer = b.order(ByteOrder.nativeOrder()).asFloatBuffer();
        binormalBuffer.clear();
        for (Vector3 v3 : binormals)
            if (v3 != null)
                binormalBuffer.put(v3.x).put(v3.y).put(v3.z);
            else binormalBuffer.put(0).put(0).put(0);
        binormalBuffer.flip();

        b = ByteBuffer.allocateDirect(4 * texCoords.size() * 2);
        texCoordsBuffer = b.order(ByteOrder.nativeOrder()).asFloatBuffer();
        texCoordsBuffer.clear();
        for (Vector2 v2 : texCoords)
            if (v2 != null)
                texCoordsBuffer.put(v2.x).put(v2.y);
            else texCoordsBuffer.put(0).put(0);
        texCoordsBuffer.flip();
    }

    private long generateVertexID(Vector3 vertex) {
        long vertID;
        long x = (long) (vertex.x * Math.pow(10, maxLOD));
        long y = (long) (vertex.y * Math.pow(10, maxLOD));
        long z = (long) (vertex.z * Math.pow(10, maxLOD));
        vertID = x * dataYBound * dataZBound * 16 + y * dataZBound * 16 + z * 16;
        return vertID;
    }

    public IntBuffer getIndexes() {
        return indexBuffer;
    }

    public FloatBuffer getVertices() {
        return vertexBuffer;
    }

    public FloatBuffer getNormals() {
        return normalBuffer;
    }

    public FloatBuffer getTangents() {
        return tangentBuffer;
    }

    public FloatBuffer getBinormals() {
        return binormalBuffer;
    }

    public FloatBuffer getTexCoords() {
        return texCoordsBuffer;
    }

    public int getVertCount() {
        return vertexCount;
    }
}
