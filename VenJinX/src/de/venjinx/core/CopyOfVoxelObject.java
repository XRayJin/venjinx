package de.venjinx.core;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.Future;

import de.venjinx.core.VoxelWorld.VoxelMode;

public final class CopyOfVoxelObject {

    public class VoxelObjectProperties {
        VoxelMode mode = VoxelMode.PSEUDO_3D_SURFACE;
        int maxLOD = 2, chunkSize = 8, extractionLvl = -1;
        float height = 1;
        boolean smooth = true, fill = true;
    }

    VoxelObjectProperties props = new VoxelObjectProperties();

    int sizeX, sizeY, sizeZ, chunksX, chunksY, chunksZ;
    CopyOfVoxelChunk[][][] chunks;
    HermiteData data;

    private Vector3 currentRefPoint = new Vector3();
    private Vector3 newRefPoint = new Vector3();
    private Vector3 center = new Vector3();

    private boolean needsUpdate, hasChanged = false;

    //    static ScheduledThreadPoolExecutor threadPool = new ScheduledThreadPoolExecutor(1);
    private HashMap<CopyOfVoxelChunk, Future<CopyOfVoxelChunk>> futureChunks = new HashMap<>();
    private LinkedList<CopyOfVoxelChunk> chunksToGenerate = new LinkedList<>();

    private long generateTime = 0L;

    public CopyOfVoxelObject(int size) {
        this(new HermiteData(size, size, size));
    }

    public CopyOfVoxelObject(HermiteData hData) {
        data = hData;

        sizeX = data.getWidth();
        sizeY = data.getHeight();
        sizeZ = data.getDepth();

        createChunks();

        center = new Vector3(sizeX / 2f, sizeY / 2f, sizeZ / 2f);

        needsUpdate = true;
    }

    public void update(float refX, float refY, float refZ) {
        update(newRefPoint.set(refX, refY, refZ));
    }

    public void update(Vector3 refPoint) {
        CopyOfVoxelChunk chunk;
        Future<CopyOfVoxelChunk> futureChunk;

        if (!refPoint.equals(currentRefPoint)) {
            needsUpdate = true;
            currentRefPoint.set(refPoint);
        }

        if (needsUpdate && futureChunks.isEmpty()) {
            updateChunks();
            needsUpdate = false;
        }

        while (!chunksToGenerate.isEmpty()) {
            chunk = chunksToGenerate.removeFirst();
            if (!futureChunks.containsKey(chunk))
                futureChunks.put(chunk, VoxelObject.threadPool.submit(chunk));
        }

        if (!futureChunks.isEmpty())
            for (int x = 0; x < chunksX; x++)
                for (int y = 0; y < chunksY; y++)
                    for (int z = 0; z < chunksZ; z++) {
                        chunk = chunks[x][y][z];
                        futureChunk = futureChunks.get(chunk);
                        if (futureChunk != null && futureChunk.isDone()) {
                            futureChunks.remove(chunk);
                            if (!futureChunk.isCancelled())
                                hasChanged |= chunk.hasNewSurfaces();

                            //                            if (futureChunks.isEmpty())
                            //                                System.out.println("object vertex count: "
                            //                                        + getVertexCount()
                            //                                        + "\ngenerate time: "
                            //                                        + getGenerateTime()
                            //                                        / 1000000
                            //                                        + "\n-----------------------------------");
                        }
                    }
    }

    public void updateChunks() {
        chunksToGenerate.clear();
        CopyOfVoxelChunk chunk;
        Vector3 currentChunkPos = currentRefPoint.div(props.chunkSize);
        int currentX = (int) currentChunkPos.x;
        int currentY = (int) currentChunkPos.y;
        int currentZ = (int) currentChunkPos.z;
        int tmpX, tmpY, tmpZ, distance;
        int oldLOD;
        for (int x = 0; x < chunksX; x++)
            for (int y = 0; y < chunksY; y++)
                for (int z = 0; z < chunksZ; z++) {
                    chunk = chunks[x][y][z];
                    oldLOD = chunk.getLOD();
                    tmpX = Math.abs(x - currentX);
                    tmpY = Math.abs(y - currentY);
                    tmpZ = Math.abs(z - currentZ);
                    distance = Math.max(tmpX, Math.max(tmpY, tmpZ));
                    if (chunk.update(distance))
                        if (oldLOD > chunk.getLOD())
                            chunksToGenerate.add(chunk);
                        else chunksToGenerate.addFirst(chunk);
                }

        boolean newB, update;
        for (int x = 0; x < chunksX; x++)
            for (int y = 0; y < chunksY; y++)
                for (int z = 0; z < chunksZ; z++) {
                    update = false;
                    chunk = chunks[x][y][z];

                    newB = z < chunksZ - 1 && chunk.lod < chunks[x][y][z + 1].lod;
                    update |= chunk.patchSides[0] != newB;
                    chunk.patchSides[0] =  newB;

                    newB = x < chunksX - 1 && chunk.lod < chunks[x + 1][y][z].lod;
                    update |= chunk.patchSides[1] != newB;
                    chunk.patchSides[1] =  newB;

                    newB = z > 0 && chunk.lod < chunks[x][y][z - 1].lod;
                    update |= chunk.patchSides[2] != newB;
                    chunk.patchSides[2] =  newB;

                    newB = x > 0 && chunk.lod < chunks[x - 1][y][z].lod;
                    update |= chunk.patchSides[3] != newB;
                    chunk.patchSides[3] =  newB;

                    newB = y < chunksY - 1 && chunk.lod < chunks[x][y + 1][z].lod;
                    update |= chunk.patchSides[4] != newB;
                    chunk.patchSides[4] =  newB;

                    newB = y > 0 && chunk.lod < chunks[x][y - 1][z].lod;
                    update |= chunk.patchSides[5] != newB;
                    chunk.patchSides[5] =  newB;

                    if (update && !chunksToGenerate.contains(chunk))
                        chunksToGenerate.add(chunk);
                }
    }

    public boolean hasChanged() {
        return hasChanged;
    }

    public void setMode(VoxelMode mode) {
        needsUpdate |= props.mode != mode;

        switch (mode) {
            case LINE_2D:
                props.height = -1;
                props.fill = false;
                break;
            case SURFACE_2D:
                props.height = -1;
                props.fill = true;
                props.smooth = false;
                break;
            case PSEUDO_3D_LINE:
                props.height = props.height < 0 ? 1 : props.height;
                props.fill = false;
                break;
            case PSEUDO_3D_SURFACE:
                props.height = props.height < 0 ? 1 : props.height;
                props.fill = true;
                break;
            case SURFACE_3D:
                break;
        }

        props.mode = mode;
    }

    public VoxelMode getMode() {
        return props.mode;
    }

    public void setMaxLOD(int lod) {
        needsUpdate |= props.maxLOD != lod;
        props.maxLOD = lod;
    }

    public int getMaxLOD() {
        return props.maxLOD;
    }

    public void setChunkSize(int size) {
        needsUpdate |= props.chunkSize != size;
        props.chunkSize = size;
        createChunks();
    }

    public int getChunkSize() {
        return props.chunkSize;
    }

    public void setExtractionLvl(int level) {
        needsUpdate |= props.extractionLvl != level;
        props.extractionLvl = level;
    }

    public int getExtractionLvl() {
        return props.extractionLvl;
    }

    public void setLvlHeight(float height) {
        needsUpdate |= props.height != height;
        props.height = height >= 0 ? height : 0;
    }

    public float getLvlHeight() {
        return props.height;
    }

    public void setSmooth(boolean smooth) {
        needsUpdate |= props.smooth != smooth;
        props.smooth = smooth;
    }

    public boolean getSmooth() {
        return props.smooth;
    }

    public HermiteData getData() {
        return data;
    }

    public CopyOfVoxelChunk[][][] getChunks() {
        return chunks;
    }

    public Vector3 getCenter() {
        return center;
    }

    public int getVertexCount() {
        int count = 0;

        for (int x = 0; x < chunksX; x++)
            for (int y = 0; y < chunksY; y++)
                for (int z = 0; z < chunksZ; z++)
                    count += chunks[x][y][z].getVertextCount();
        return count;
    }

    public long getGenerateTime() {
        long time = 0;

        for (int x = 0; x < chunksX; x++)
            for (int y = 0; y < chunksY; y++)
                for (int z = 0; z < chunksZ; z++)
                    time += chunks[x][y][z].getGenerateTime();
        return time;
    }

    private void createChunks() {
        //        chunksX = (sizeX - 2) / props.chunkSize + 1;
        //        chunksY = (sizeY - 2) / props.chunkSize + 1;
        //        chunksZ = (sizeZ - 2) / props.chunkSize + 1;

        chunksX = sizeX / props.chunkSize;
        chunksY = sizeY / props.chunkSize;
        chunksZ = sizeZ / props.chunkSize;

        chunks = new CopyOfVoxelChunk[chunksX][chunksY][chunksZ];
        for (int x = 0; x < chunksX; x++)
            for (int y = 0; y < chunksY; y++)
                for (int z = 0; z < chunksZ; z++)
                    chunks[x][y][z] = new CopyOfVoxelChunk(x * props.chunkSize,
                                                           y * props.chunkSize,
                                                           z * props.chunkSize,
                                                           this);
    }

    private String treeToString(int depth) {
        String str = "";

        for (int i = 0; i < depth; i++)
            str += "    ";

        // str += "VoxelObject(" + xMin + ", " + yMin + ", " + zMin + ")";
        // str += " - (" + xMax + ", " + yMax + ", " + zMax + ")";
        // str += ", " + size + ", " + lod + ", " + mode + ", " + state;
        // str += " - " + newData + "\n";
        // if (ocTree != null)
        // for (int x = 0; x < 2; x++)
        // for (int y = 0; y < 2; y++)
        // for (int z = 0; z < 2; z++)
        // str += ocTree[x][y][z].treeToString(depth + 1);

        return str;
    }

    @Override
    public String toString() {
        return treeToString(0);
    }
}
