package de.venjinx.core;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.Callable;

import de.venjinx.core.CopyOfVoxelObject.VoxelObjectProperties;

public final class CopyOfVoxelChunk implements Callable<CopyOfVoxelChunk> {

    private VoxelObjectProperties props;
    HermiteData data;

    int lod = -1;
    final int xMin, yMin, zMin, xMax, yMax, zMax;
    //    private Vector3 center;

    private CopyOfVoxelChunk[][][] ocTree;
    private boolean newParts = false;
    private boolean needsGeneration = false;
    boolean[] patchSides = new boolean[6];

    private LinkedList<HashMap<Long, CopyOfVoxelQuadTree>> rawQuads;
    private LinkedList<VoxelMeshData> meshParts = new LinkedList<>();

    private long generateTime = 0L;
    private int vertexCount = 0;

    //    private LinkedList<CopyOfVoxelQuadTree> outlineQuads = new LinkedList<>();

    public CopyOfVoxelChunk(int xOff, int yOff, int zOff, CopyOfVoxelObject vObject) {
        data = vObject.data;
        props = vObject.props;

        xMin = xOff;
        yMin = yOff;
        zMin = zOff;

        int size = props.chunkSize;
        xMax = xMin + size;
        yMax = yMin + size;
        zMax = zMin + size;

        //        center = new Vector3(xMin + size / 2f, yMin + size / 2f, zMin + size / 2f);
    }

    public LinkedList<VoxelMeshData> getMeshParts() {
        newParts = false;
        return meshParts;
    }

    boolean update(int chunkDistance) {
        int newLOD = props.maxLOD - chunkDistance;
        newLOD = newLOD > props.maxLOD ? props.maxLOD : newLOD < 1 ? 1 : newLOD;
        if (lod != newLOD) {
            lod = newLOD;
            needsGeneration = true;
        }
        return needsGeneration;
    }

    private void generate() {
        try {
            LinkedList<VoxelMeshData> parts = new LinkedList<>();
            newParts = false;

            updateParts();

            vertexCount = 0;
            for (HashMap<Long, CopyOfVoxelQuadTree> quads : rawQuads)
                parts.add(generateData(quads));

            meshParts = parts;
            newParts = parts.size() > 0;
            needsGeneration = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateParts() {
        if (props.extractionLvl >= 0 && props.extractionLvl < yMax)
            rawQuads = CopyOfMarchingSquares.findSurfaces(this,
                                                          yMin + props.extractionLvl,
                                                          yMin + props.extractionLvl + 1);
        else rawQuads = CopyOfMarchingSquares.findSurfaces(this);
    }

    //    private void addQuad(CopyOfVoxelQuadTree quad) {
    //        //        quad.chunk = this;
    //        //        quad.lod = lod;
    //        quad.generate(props, lod);
    //    }

    VoxelMeshData generateData(HashMap<Long, CopyOfVoxelQuadTree> quads) {
        VoxelMeshData meshData = new VoxelMeshData(getMin(), getMax(), props.maxLOD,
                                                   data.getHeight(), data.getDepth());
        for (CopyOfVoxelQuadTree quad : quads.values()) {
            quad.generate(props, lod);                                          // maybe causes error
            meshData.addTree(quad, props.smooth);
        }

        meshData.createBuffers();
        vertexCount += meshData.getVertCount();
        return meshData;
    }

    public boolean isBound(float x, float y, float z) {
        return x - 1 < xMin || x + 1 >= xMax
                || y - 1 < yMin || y + 1 >= yMax
                || z - 1 < zMin || z + 1 >= zMax;
    }

    public boolean inBound(float x, float y, float z) {
        return x >= xMin && x < xMax
                && y >= yMin && y < yMax
                && z >= zMin && z < zMax;
    }

    public boolean hasNewSurfaces() {
        return newParts;
    }

    public int getLOD() {
        return lod;
    }

    public Vector3 getMin() {
        return new Vector3(xMin, yMin, zMin);
    }

    public Vector3 getMax() {
        return new Vector3(xMax, yMax, zMax);
    }

    //    public Vector3 getCenter() {
    //        return center;
    //    }

    public int getVertextCount() {
        return vertexCount;
    }

    public long getGenerateTime() {
        return generateTime;
    }

    @Override
    public CopyOfVoxelChunk call() throws Exception {
        generateTime = System.nanoTime();
        generate();
        generateTime = System.nanoTime() - generateTime;
        return this;
    }

    private String treeToString(int depth) {
        String str = "";

        for (int i = 0; i < depth; i++)
            str += "    ";

        str += "VoxelChunk(" + xMin + ", " + yMin + ", " + zMin + ")";
        str += " - (" + xMax + ", " + yMax + ", " + zMax + ")";
        str += ", " + props.chunkSize + ", " + lod + ", " + props.mode;
        str += " - " + newParts + "\n";
        if (ocTree != null)
            for (int x = 0; x < 2; x++)
                for (int y = 0; y < 2; y++)
                    for (int z = 0; z < 2; z++)
                        str += ocTree[x][y][z].treeToString(depth + 1);

        return str;
    }

    @Override
    public String toString() {
        return treeToString(0);
    }

    // public void mergeSurfaces() {
    // Surface surface, otherSurface;
    // Iterator<Long> iter;
    // double vID;
    // boolean merged = false;
    //
    // for (int i = 0; i < surfaceParts.size(); i++) {
    // surface = surfaceParts.removeFirst();
    //
    // while (!surface.isComplete())
    // for (int j = 0; j < surfaceParts.size(); j++) {
    // otherSurface = surfaceParts.removeFirst();
    // merged = false;
    // iter = surface.getSufaceBoundVerts().iterator();
    // while (iter.hasNext()) {
    // vID = iter.next();
    // if (otherSurface.getSufaceBoundVerts().contains(vID)) {
    // if (surface.vertCount() >= otherSurface.vertCount())
    // merged = surface.merge(otherSurface);
    // else {
    // merged = otherSurface.merge(surface);
    // surface = otherSurface;
    // }
    // j--;
    // break;
    // }
    // }
    // if (!merged)
    // surfaceParts.add(otherSurface);
    // }
    // surfaces.add(surface);
    // i--;
    // }
    // }
}
