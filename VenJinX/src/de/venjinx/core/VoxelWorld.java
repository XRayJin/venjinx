package de.venjinx.core;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class VoxelWorld implements Runnable {

    public enum VoxelMode {
        LINE_2D, SURFACE_2D, PSEUDO_3D_LINE, PSEUDO_3D_SURFACE, SURFACE_3D
    };

    int maxLOD = 10;
    float resolution = (float) Math.pow(2, -10);
    float extractThreshold = 0f;
    float viewDistance = 0;
    int maxChunkSize = 32;

    Vector3 refPoint = new Vector3();

    private Set<VoxelObject> invisibleObjects;
    private Set<VoxelObject> visibleObjects;

    // private ScheduledThreadPoolExecutor threadPool;
    // private ExecutorService worldUpdateExecutor;

    private boolean running = false;

    public VoxelWorld() {
        this(1000, new Vector3());
    }

    public VoxelWorld(float distance) {
        this(distance, new Vector3());
    }

    public VoxelWorld(float distance, Vector3 refPos) {
        viewDistance = distance;
        refPoint = refPos;

        invisibleObjects = Collections.synchronizedSet(new LinkedHashSet<VoxelObject>());
        visibleObjects = Collections.synchronizedSet(new LinkedHashSet<VoxelObject>());

        // threadPool = new ScheduledThreadPoolExecutor(1);
        // worldUpdateExecutor = Executors.newSingleThreadExecutor();
        // worldUpdateExecutor.execute(this);
    }

    public void update(float x, float y, float z, float tpf) {
        long t1 = System.currentTimeMillis();
        refPoint.set(x, y, z);
        // updateObjectLists();

        // try {
        // Thread.sleep(500);
        // } catch (InterruptedException e) {
        // e.printStackTrace();
        // }

        // for (CopyOfVoxelObject vObject : objects)
        // vObject.update(refPoint);

        // for (VoxelObject vObject : visibleObjects)
        // // System.out.println(vObject);
        // vObject.update(refPoint, maxChunkSize, maxLOD);
        // System.out.println("update " + (System.currentTimeMillis() - t1) +
        // "ms");
    }

    @Override
    public void run() {
        long t1 = System.currentTimeMillis();
        running = true;
        while (running)
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }

    public void setRefPoint(float x, float y, float z) {
        refPoint.set(x, y, z);
    }

    public Vector3 getRefPoint() {
        return refPoint;
    }

    private void updateObjectLists() {
        VoxelObject object;
        for (Iterator<VoxelObject> iter = invisibleObjects.iterator(); iter.hasNext();) {
            object = iter.next();
            if (object.getCenter().distance(refPoint) <= viewDistance * 1.1d) {
                visibleObjects.add(object);
                iter.remove();
            }
        }
        for (Iterator<VoxelObject> iter = visibleObjects.iterator(); iter.hasNext();) {
            object = iter.next();
            if (object.getCenter().distance(refPoint) > viewDistance * 1.1d) {
                invisibleObjects.add(object);
                iter.remove();
            }
        }
    }

    public void addObject(VoxelObject voxelObject) {
        if (voxelObject.getCenter().distance(refPoint) <= viewDistance * 1.1f)
            visibleObjects.add(voxelObject);
        else invisibleObjects.add(voxelObject);
    }

    public int maxLOD() {
        return maxLOD;
    }

    public float viewDistance() {
        return viewDistance;
    }

    public float matchToResolution(float value) {
        value /= resolution;
        value = Math.round(value);
        value *= resolution;
        return value;
    }

    public void setViewDistance(float distance) {
        viewDistance = distance;
    }

    public void stop() {
        System.out.println("stop world");
        running = false;
        // worldUpdateExecutor.shutdownNow();
        // threadPool.shutdownNow();
        VoxelObject.threadPool.shutdownNow();
    }
}
