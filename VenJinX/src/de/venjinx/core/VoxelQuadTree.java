package de.venjinx.core;


public class VoxelQuadTree {

    public static class QuadCases {

        private static final int[] vertCounts = new int[]
                { 4, 5, 5, 4, 5, 6, 4, 3, 5, 4, 6, 3, 4, 3, 3, 0 };

        private static int caseCount = vertCounts.length;

        private static final int[][] nativePts = new int[][]                    // case
                { { 0, 1, 2, 3 },                                               // 0
                  { 1, 2, 3 },                                                  // 1
                  { 2, 3, 0 },                                                  // 2
                  { 2, 3 },                                                     // 3
                  { 3, 0, 1 },                                                  // 4
                  { 1, 3 },                                                     // 5
                  { 3, 0 },                                                     // 6
                  { 3 },                                                        // 7

                  { 0, 1, 2 },                                                  // 8
                  { 1, 2 },                                                     // 9
                  { 0, 2 },                                                     // 10
                  { 2 },                                                        // 11
                  { 0, 1 },                                                     // 12
                  { 1 },                                                        // 13
                  { 0 },                                                        // 14
                  { } };                                                        // 15

        private static final int[][] edges = new int[][]
                { { },                                                          // 0
                  { 3, 0 },                                                     // 1
                  { 0, 2 },                                                     // 2
                  { 3, 2 },                                                     // 3
                  { 2, 1 },                                                     // 4
                  { 2, 0, 3, 1 },                                               // 5
                  { 0, 1 },                                                     // 6
                  { 3, 1 },                                                     // 7

                  { 1, 3 },                                                     // 8
                  { 1, 0 },                                                     // 9
                  { 0, 3, 1, 2 },                                               // 10
                  { 1, 2 },                                                     // 11
                  { 2, 3 },                                                     // 12
                  { 2, 0 },                                                     // 13
                  { 0, 3 },                                                     // 14
                  { } };                                                        // 15

        private static final int[][] isoLinePts = new int[][]
                { { },                                                          // 0
                  { 0, 3, 0, 1 },                                               // 1
                  { 0, 1, 1, 2 },                                               // 2
                  { 0, 3, 1, 2 },                                               // 3
                  { 1, 2, 3, 2 },                                               // 4
                  { 1, 2, 0, 1, 0, 3, 3, 2 },                                   // 5
                  { 0, 1, 3, 2 },                                               // 6
                  { 0, 3, 3, 2 },                                               // 7

                  { 3, 2, 0, 3 },                                               // 8
                  { 3, 2, 0, 1 },                                               // 9
                  { 0, 1, 0, 3, 3, 2, 1, 2 },                                   // 10
                  { 3, 2, 1, 2 },                                               // 11
                  { 1, 2, 0, 3 },                                               // 12
                  { 1, 2, 0, 1 },                                               // 13
                  { 0, 1, 0, 3 },                                               // 14
                  { } };                                                        // 15

        private static final int[][] indexes = new int[][]
                { { 0, 1, 2, 2, 3, 0 },                                         // 0
                  { 0, 3, 4, 0, 1, 3, 1, 2, 3 },                                // 1
                  { 0, 3, 4, 0, 1, 3, 1, 2, 3 },                                // 2
                  { 0, 1, 2, 2, 3, 0 },                                         // 3
                  { 0, 3, 4, 0, 1, 3, 1, 2, 3 },                                // 4
                  { 0, 1, 2, 3, 4, 5 },                                         // 5
                  { 0, 1, 2, 2, 3, 0 },                                         // 6
                  { 0, 1, 2 },                                                  // 7

                  { 0, 3, 4, 0, 1, 3, 1, 2, 3 },                                // 8
                  { 0, 1, 2, 2, 3, 0 },                                         // 9
                  { 0, 1, 2, 3, 4, 5 },                                         // 10
                  { 0, 1, 2 },                                                  // 11
                  { 0, 1, 2, 2, 3, 0 },                                         // 12
                  { 0, 1, 2 },                                                  // 13
                  { 0, 1, 2 },                                                  // 14
                  { } };                                                        // 15

        private static final Vector3[] normals = new Vector3[]
                { new Vector3(0, 1, 0).normL(),                                 // 0
                  new Vector3(-1, 0, 1).normL(),                                // 1
                  new Vector3(1, 0, 1).normL(),                                 // 2
                  new Vector3(0, 0, 1).normL(),                                 // 3
                  new Vector3(1, 0, -1).normL(),                                // 4
                  new Vector3(-1, 0, -1).normL(),                               // 5
                  new Vector3(1, 0, 0).normL(),                                 // 6
                  new Vector3(1, 0, 1).normL(),                                 // 7

                  new Vector3(-1, 0, -1).normL(),                               // 8
                  new Vector3(-1, 0, 0).normL(),                                // 9
                  new Vector3(1, 0, -1).normL(),                                // 10
                  new Vector3(-1, 0, 1).normL(),                                // 11
                  new Vector3(0, 0, -1).normL(),                                // 12
                  new Vector3(-1, 0, -1).normL(),                               // 13
                  new Vector3(1, 0, -1).normL(),                                // 14
                  new Vector3(0, -1, 0).normL() };                              // 15

        private static final Vector3[] tangents = new Vector3[]
                { new Vector3(1, 0, 0).normL(),                                 // 0
                  new Vector3(1, 0, 1).normL(),                                 // 1
                  new Vector3(1, 0, -1).normL(),                                // 2
                  new Vector3(1, 0, 0).normL(),                                 // 3
                  new Vector3(-1, 0, -1).normL(),                               // 4
                  new Vector3(-1, 0, 1).normL(),                                // 5
                  new Vector3(0, 0, -1).normL(),                                // 6
                  new Vector3(1, 0, -1).normL(),                                // 7

                  new Vector3(-1, 0, 1).normL(),                                // 8
                  new Vector3(0, 0, 1).normL(),                                 // 9
                  new Vector3(-1, 0, -1).normL(),                               // 10
                  new Vector3(1, 0, 1).normL(),                                 // 11
                  new Vector3(-1, 0, 0).normL(),                                // 12
                  new Vector3(-1, 0, 1).normL(),                                // 13
                  new Vector3(-1, 0, -1).normL(),                               // 14
                  new Vector3(-1, 0, 0).normL() };                              // 15

        private static final boolean[][] neighbors = new boolean[][]
                // bottom, top, right, left
                { { true, true, true, true },                                   // 0
                  { true, true, true, true },                                   // 1
                  { true, true, true, true },                                   // 2
                  { false, true, true, true },                                  // 3
                  { true, true, true, true },                                   // 4
                  { true, true, true, true },                                   // 5
                  { true, true, false, true },                                  // 6
                  { false, true, false, true },                                 // 7

                  { true, true, true, true },                                   // 8
                  { true, true, true, false },                                  // 9
                  { true, true, true, true },                                   // 10
                  { false, true, true, false },                                 // 11
                  { true, false, true, true },                                  // 12
                  { true, false, true, false },                                 // 13
                  { true, false, false, true },                                 // 14
                  { false, false, false, false } };                             // 15

        private static final boolean[][] transitionNeighbors = new boolean[][]
                // bottom, top, right, left
                { { false, false, false, false },                               // 0
                  { true, false, false, true },                                 // 1
                  { true, false, true, false },                                 // 2
                  { false, false, true, true },                                 // 3
                  { false, true, true, false },                                 // 4
                  { true, true, true, true },                                   // 5
                  { true, true, false, false },                                 // 6
                  { false, true, false, true },                                 // 7

                  { false, true, false, true },                                 // 8
                  { true, true, false, false },                                 // 9
                  { true, true, true, true },                                   // 10
                  { false, true, true, false },                                 // 11
                  { false, false, true, true },                                 // 12
                  { true, false, true, false },                                 // 13
                  { true, false, false, true },                                 // 14
                  { false, false, false, false } };                             // 15

        private static int getQuadCase(float[] data) {
            int qCase = 0;
            if (data[0] < 0)
                qCase |= 1;
            if (data[1] < 0)
                qCase |= 2;
            if (data[2] < 0)
                qCase |= 4;
            if (data[3] < 0)
                qCase |= 8;
            return qCase;
        }

        public static int getQuadCase(int x, int y, int z, float[][][] data) {
            int qCase = 0;
            if (data[x][y][z + 1] < 0)
                qCase |= 1;
            if (data[x + 1][y][z + 1] < 0)
                qCase |= 2;
            if (data[x + 1][y][z] < 0)
                qCase |= 4;
            if (data[x][y][z] < 0)
                qCase |= 8;

            return qCase;
        }

        public static int getVertCount(int quadCase) {
            if (quadCase >= 0 && quadCase < caseCount)
                return vertCounts[quadCase];
            return 0;
        }

        public static int getNativeVertCount(int quadCase) {
            if (quadCase >= 0 && quadCase < caseCount)
                return nativePts[quadCase].length;
            return 0;
        }

        public static int getEdgeCount(int quadCase) {
            if (quadCase >= 0 && quadCase < caseCount)
                return edges[quadCase].length;
            return 0;
        }

        public static int[] getEdges(int quadCase) {
            if (quadCase >= 0 && quadCase < caseCount)
                return edges[quadCase];
            return null;
        }

        public static int getNativeID(int quadCase, int vertId) {
            if (quadCase >= 0 && quadCase < caseCount
                    && vertId >= 0 && vertId < nativePts[quadCase].length)
                return nativePts[quadCase][vertId];
            return -1;
        }

        public static int getIsoVertID(int quadCase, int vertId) {
            if (quadCase >= 0 && quadCase < caseCount
                    && vertId >= 0 && vertId < isoLinePts[quadCase].length)
                return isoLinePts[quadCase][vertId];
            return -1;
        }

        public static int getIsoLineVertCount(int quadCase) {
            if (quadCase >= 0 && quadCase < caseCount)
                return isoLinePts[quadCase].length / 2;
            return 0;
        }

        public static Vector3 getNormal(int quadCase) {
            if (quadCase >= 0 && quadCase < caseCount)
                return new Vector3(normals[quadCase]);
            return null;
        }

        public static int getNormalID(Vector3 normal) {
            for (int i = 0; i < 16; i++)
                if (normal.equals(normals[i]))
                    return i;
            return -1;
        }

        public static Vector4 getTangent(int quadCase) {
            if (quadCase >= 0 && quadCase < caseCount)
                return new Vector4(tangents[quadCase].x, tangents[quadCase].y,
                                   tangents[quadCase].z, 1);
            return null;
        }

        public static Vector3 getTangentDirection(int quadCase) {
            if (quadCase >= 0 && quadCase < caseCount)
                return new Vector3(tangents[quadCase]);
            return null;
        }

        public static int getIndex(int quadCase, int indexId) {
            if (quadCase >= 0 && quadCase < caseCount
                    && indexId >= 0 && indexId < indexes[quadCase].length)
                return indexes[quadCase][indexId];
            return -1;
        }

        public static int getIndexCount(int quadCase) {
            if (quadCase >= 0 && quadCase < caseCount)
                return indexes[quadCase].length;
            return 0;
        }

        public static boolean hasNeighbor(int quadCase, int neighborID) {
            if (quadCase >= 0 && quadCase < caseCount)
                return neighbors[quadCase][neighborID];
            return false;
        }

        public static boolean hasTransitionNeighbor(int quadCase, int neighborID) {
            if (quadCase >= 0 && quadCase < caseCount)
                return transitionNeighbors[quadCase][neighborID];
            return false;
        }

        public static int caseCount() {
            return caseCount;
        }
    }

    VoxelSurface surface;
    final int currentDepth, positionIndex;
    long id, key;
    private Vector3[] vertices = new Vector3[4];
    VoxelQuadTree[] trees;
    VoxelQuadTree[] neighbors = new VoxelQuadTree[6];
    boolean[] patchSides = new boolean[6];

    public float x = 0, y = 0, z = 0, size = 1;
    public int quadCase = 0, lod;
    boolean bound = false, transitionQuad = false;

    private float[] leafData = new float[4];

    private Vector3[] tVerts;
    private Vector3[] tNorms;
    private Vector4[] tTangs;
    private int[] tIndexes;

    public VoxelQuadTree(int x, int y, int z, int quadCase,
                         boolean bound, float[][][] data) {
        currentDepth = 0;
        positionIndex = -1;

        vertices[0] = new Vector3();
        vertices[1] = new Vector3();
        vertices[2] = new Vector3();
        vertices[3] = new Vector3();

        this.x = x;
        this.y = y;
        this.z = z;
        this.bound = bound;
        this.quadCase = quadCase;

        leafData[0] = data[x][y][z + 1];
        leafData[1] = data[x + 1][y][z + 1];
        leafData[2] = data[x + 1][y][z];
        leafData[3] = data[x][y][z];
    }

    private VoxelQuadTree(VoxelQuadTree parent, int childIndex) {
        key = parent.key;
        positionIndex = childIndex;
        lod = parent.lod - 1;
        currentDepth = parent.currentDepth + 1;
        size = parent.size / 2;
        surface = parent.surface;

        vertices[0] = new Vector3();
        vertices[1] = new Vector3();
        vertices[2] = new Vector3();
        vertices[3] = new Vector3();
    }

    private int patchCrackNeeded() {
        if (bound && quadCase > 0)
            return 1;

        return 0;
    }

    private Vector3 exactIntersection(int pt1, int pt2) {
        float step =
                Math.abs(leafData[pt1])
                / (Math.abs(leafData[pt1]) + Math.abs(leafData[pt2]));
        //        float step = (Math.abs(leafData[pt1]) + Math.abs(leafData[pt2]));
        Vector3 lineDir = vertices[pt2].sub(vertices[pt1]);
        return lineDir.multL(step).addL(vertices[pt1]);
    }

    public long generate(float lvlHeight, boolean fill,
                         boolean smooth, int lod, boolean[] sides) {
        long t = System.nanoTime();
        subdivide(lod);

        if (isLeaf())
            generateComponent(lvlHeight, fill, smooth);
        else for (VoxelQuadTree q : trees)
            if (q != null)
                q.generate(lvlHeight, fill, smooth, lod - 1, sides);
        return System.nanoTime() - t;
    }

    private void generateComponent(float lvlHeight, boolean fill, boolean smooth) {
        boolean extrude = lvlHeight > 0 && y > 0, outline = extrude || !fill;

        if (extrude)
            y = y * lvlHeight;

        if (lvlHeight < 0)
            y = 0;

        vertices[0] = new Vector3(x, y, z + size);
        vertices[1] = new Vector3(x + size, y, z + size);
        vertices[2] = new Vector3(x + size, y, z);
        vertices[3] = new Vector3(x, y, z);

        if (quadCase == 0) {
            extrude = false;
            outline = false;
        }

        int indexCount = outline ? 2 + (extrude ? 4 : 0) : 0;
        if (quadCase == 5 || quadCase == 10)
            indexCount += outline ? 2 + (extrude ? 4 : 0) : 0;
            indexCount += fill ? QuadCases.getIndexCount(quadCase) : 0;

            int vertCount = outline ? 2 + (extrude ? 2 : 0) : 0;
            if (quadCase == 5 || quadCase == 10)
                vertCount += outline ? 2 + (extrude ? 2 : 0) : 0;
                vertCount += fill ? QuadCases.vertCounts[quadCase] : 0;

                if (extrude && smooth)
                    vertCount -= 2;

                tVerts = new Vector3[vertCount];
                tNorms = new Vector3[vertCount];
                tTangs = new Vector4[vertCount];
                tIndexes = new int[indexCount];

                if (outline)
                    outline(extrude, smooth, lvlHeight);

                if (fill)
                    if (neighbors[4] == null || quadCase != neighbors[4].quadCase)
                        fill(extrude, smooth);
    }

    private void outline(boolean extrude, boolean smooth, float width) {
        int ptID1, ptID2, edgeCount = QuadCases.getEdgeCount(quadCase);

        if (extrude) {
            tIndexes[0] = 3;
            tIndexes[1] = 1;
            tIndexes[2] = 2;
            tIndexes[3] = 2;
            tIndexes[4] = 1;
            tIndexes[5] = 0;

            if (quadCase == 5 || quadCase == 10) {
                tIndexes[6] = 7;
                tIndexes[7] = 5;
                tIndexes[8] = 6;
                tIndexes[9] = 6;
                tIndexes[10] = 5;
                tIndexes[11] = 4;
            }

            for (int i = 0; i < edgeCount; i++) {
                ptID1 = QuadCases.getIsoVertID(quadCase, i * 2);
                ptID2 = QuadCases.getIsoVertID(quadCase, i * 2 + 1);

                tVerts[i] = intersection(ptID1, ptID2, true);
                //                tVerts[i] = exactIntersection(ptID1, ptID2);
                tNorms[i] = QuadCases.getNormal(quadCase);
                //                tNorms[i] = surface.getData().getNormal(tVerts[i].x,
                //                                                        tVerts[i].y,
                //                                                        tVerts[i].z);
                //                System.out.println(tVerts[i] + ", "
                //                        + surface.getData().getInterpolatedDensity(tVerts[i]));
                //                System.out.println("-----------------------------------------");
                tNorms[i].setY(0).normL();
                Vector3 b = tNorms[i].cross(new Vector3(0, -1, 0));
                tTangs[i] = new Vector4(b.x, b.y, b.z, 1);
                //                tTangs[i] = QuadCases.getTangent(quadCase);

                tVerts[i + edgeCount] = tVerts[i].add(0, -width, 0);
                //                tNorms[i + 2] = QuadCases.getNormal(quadCase);
                tNorms[i + edgeCount] = new Vector3(tNorms[i]);
                //                tTangs[i + 2] = QuadCases.getTangent(quadCase);
                tTangs[i + edgeCount] = new Vector4(tTangs[i]);

                if (bound) {
                    //                    int edgeID = QuadCases.edges[quadCase][i];
                    //                    if (QuadCases.hasNeighbor(quadCase, edgeID)
                    //                            && neighbors[edgeID] == null)
                    //                        System.out.println(quadCase + ", " + bound);
                    //                        if (lod != neighbors[edgeID].lod) {
                    //
                    //                        }
                }
            }
        } else {
            tIndexes[0] = 0;
            tIndexes[1] = 1;

            if (quadCase == 5 || quadCase == 10) {
                tIndexes[2] = 2;
                tIndexes[3] = 3;
            }

            for (int i = 0; i < edgeCount; i++) {
                ptID1 = QuadCases.getIsoVertID(quadCase, i * 2);
                ptID2 = QuadCases.getIsoVertID(quadCase, i * 2 + 1);

                tVerts[i] = intersection(ptID1, ptID2, true);
                tNorms[i] = QuadCases.getNormal(quadCase);
                tTangs[i] = QuadCases.getTangent(quadCase);
            }
        }
    }

    private void fill(boolean extrude, boolean smooth) {
        int ptID1, ptID2, vertOffset = extrude ? 4 : 0, vertCount = tVerts.length;
        int edgeCount = QuadCases.getEdgeCount(quadCase);
        int indexOffset = extrude ? 6 : 0, indexCount = tIndexes.length;

        if (quadCase == 5 || quadCase == 10) {
            vertOffset += extrude ? 4 : 0;
            indexOffset += extrude ? 6 : 0;
        }

        if (extrude && smooth) {
            int caseOff = vertCount > 6 ? 1 : 0;
            tIndexes[indexOffset++] = 0;
            tIndexes[indexOffset++] = 1;
            tIndexes[indexOffset++] = 4 + caseOff;

            tVerts[vertOffset] = vertices[QuadCases.getNativeID(quadCase, 0)];
            tNorms[vertOffset] = QuadCases.getNormal(0);
            tTangs[vertOffset++] = QuadCases.getTangent(quadCase);

            if (vertOffset < vertCount) {
                tIndexes[indexOffset++] = 0;
                tIndexes[indexOffset++] = 4 + caseOff;
                tIndexes[indexOffset++] = 5 + caseOff;

                tVerts[vertOffset] = vertices[QuadCases.getNativeID(quadCase, 1)];
                tNorms[vertOffset] = QuadCases.getNormal(0);
                tTangs[vertOffset++] = QuadCases.getTangent(quadCase);
            }

            if (vertOffset < vertCount) {
                tIndexes[indexOffset++] = 1;
                tIndexes[indexOffset++] = 4;
                tIndexes[indexOffset++] = 5;

                tVerts[vertOffset] = vertices[QuadCases.getNativeID(quadCase, 2)];
                tNorms[vertOffset] = QuadCases.getNormal(0);
                tTangs[vertOffset++] = QuadCases.getTangent(quadCase);
            }

            tNorms[0].addL(QuadCases.getNormal(0)).normL();
            tNorms[1].addL(QuadCases.getNormal(0)).normL();
        } else {
            for (int i = indexOffset; i < indexCount; i++)
                tIndexes[i] = QuadCases.getIndex(quadCase, i - indexOffset) + vertOffset;

            if (quadCase != 0)
                for (int i = 0; i < edgeCount; i++) {
                    ptID1 = QuadCases.getIsoVertID(quadCase, i * 2);
                    ptID2 = QuadCases.getIsoVertID(quadCase, i * 2 + 1);

                    tVerts[vertOffset] = intersection(ptID1, ptID2, true);
                    //                tVerts[vertOffset] = exactIntersection(ptID1, ptID2);
                    tNorms[vertOffset] = QuadCases.getNormal(0);
                    //                tNorms[vertOffset] = surface.getData().getNormal(tVerts[vertOffset].x,
                    //                                                                 tVerts[vertOffset].y,
                    //                                                                 tVerts[vertOffset].z);
                    tTangs[vertOffset] = QuadCases.getTangent(quadCase);

                    vertOffset++;
                }

            for (int i = vertOffset; i < vertCount; i++) {
                tVerts[i] = vertices[QuadCases.getNativeID(quadCase, i - vertOffset)];
                tNorms[i] = QuadCases.getNormal(0);
                //            tNorms[i] = surface.getData().getNormal(tVerts[i].x,
                //                                                    tVerts[i].y,
                //                                                    tVerts[i].z);
                tTangs[i] = QuadCases.getTangent(quadCase);
            }
        }
    }

    private void subdivide(int lod) {
        this.lod = lod;
        if (lod > 1)
            if (needsSubdivide())
                createQuadTree();

    }

    private void createQuadTree() {
        trees = new VoxelQuadTree[4];

        for (int i = 0; i < 4; i++)
            trees[i] = createQuad(i);
    }

    private VoxelQuadTree createQuad(int pos) {
        VoxelQuadTree quad = new VoxelQuadTree(this, pos);
        float step = size / 2;
        switch (pos) {
            case 0:
                quad.x = x;
                quad.y = y;
                quad.z = z + step;
                quad.leafData[0] = leafData[0];
                quad.leafData[1] = (leafData[0] + leafData[1]) / 2;
                quad.leafData[2] =
                        (leafData[0] + leafData[1] + leafData[2] + leafData[3]) / 4;
                quad.leafData[3] = (leafData[0] + leafData[3]) / 2;
                break;
            case 1:
                quad.x = x + step;
                quad.y = y;
                quad.z = z + step;
                quad.leafData[0] = (leafData[1] + leafData[0]) / 2;
                quad.leafData[1] = leafData[1];
                quad.leafData[2] = (leafData[1] + leafData[2]) / 2;
                quad.leafData[3] =
                        (leafData[0] + leafData[1] + leafData[2] + leafData[3]) / 4;
                break;
            case 2:
                quad.x = x + step;
                quad.y = y;
                quad.z = z;
                quad.leafData[0] =
                        (leafData[0] + leafData[1] + leafData[2] + leafData[3]) / 4;
                quad.leafData[1] = (leafData[2] + leafData[1]) / 2;
                quad.leafData[2] = leafData[2];
                quad.leafData[3] = (leafData[2] + leafData[3]) / 2;
                break;
            case 3:
                quad.x = x;
                quad.y = y;
                quad.z = z;
                quad.leafData[0] = (leafData[3] + leafData[0]) / 2;
                quad.leafData[1] =
                        (leafData[0] + leafData[1] + leafData[2] + leafData[3]) / 4;
                quad.leafData[2] = (leafData[3] + leafData[2]) / 2;
                quad.leafData[3] = leafData[3];
                break;
        }

        quad.size = step;
        // quad.leafData[0] = VoxelWorld.matchToResolution(quad.leafData[0]);
        // quad.leafData[1] = VoxelWorld.matchToResolution(quad.leafData[1]);
        // quad.leafData[2] = VoxelWorld.matchToResolution(quad.leafData[2]);
        // quad.leafData[3] = VoxelWorld.matchToResolution(quad.leafData[3]);
        quad.quadCase = QuadCases.getQuadCase(quad.leafData);

        quad.bound = quad.x == surface.getXMin() || quad.x == surface.getXMax() - step
                || quad.y == surface.getYMin() || quad.y == surface.getYMax() - step
                || quad.z == surface.getZMin() || quad.z == surface.getZMax() - step;
        //        quad.bound &= quad.quadCase != 0;
        if (quad.quadCase < 15)
            return quad;
        return null;
    }

    private boolean needsSubdivide() {
        boolean subdivide = false;
        if (leafData[0] >= 0 && leafData[1] >= 0 && leafData[2] >= 0
                && leafData[3] >= 0)
            return false;
        subdivide |= leafData[0] >= 0 && leafData[0] <= size;
        subdivide |= leafData[1] >= 0 && leafData[1] <= size;
        subdivide |= leafData[2] >= 0 && leafData[2] <= size;
        subdivide |= leafData[3] >= 0 && leafData[3] <= size;
        return subdivide;
    }

    private Vector3 intersection(int pt1, int pt2, boolean interpolate) {
        Vector3 lineDir = vertices[pt2].sub(vertices[pt1]);
        float step = lineDir.length() / 2;
        if (interpolate)
            return lineDir.normL().multL(step).addL(vertices[pt1]);
        else return lineDir.normL().multL(step);
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    public Vector3 getPosition() {
        return new Vector3(x, y, z);
    }

    public int getIndex() {
        return positionIndex;
    }

    public VoxelQuadTree getBottomLeftTree() {
        return trees[0];
    }

    public VoxelQuadTree getBottomRightTree() {
        return trees[1];
    }

    public VoxelQuadTree getTopRightTree() {
        return trees[2];
    }

    public VoxelQuadTree getTopLeftTree() {
        return trees[3];
    }

    public boolean isLeaf() {
        return trees == null;
    }

    public int getCurrentDepth() {
        return currentDepth;
    }

    public boolean isBoundQuad() {
        return bound;
    }

    public boolean isTransitionQuad() {
        if (bound && quadCase != 0)
            for (int i = 0; i < 4; i++)
                if (QuadCases.hasTransitionNeighbor(quadCase, i) && neighbors[i] == null)
                    return true;
        return false;
    }

    public int getQuadCase() {
        return quadCase;
    }

    public float[] getLeafData() {
        return leafData;
    }

    public Vector3[] getVertices() {
        return tVerts;
    }

    public Vector3[] getNormals() {
        return tNorms;
    }

    public Vector4[] getTangents() {
        return tTangs;
    }

    public int[] getIndexes() {
        return tIndexes;
    }

    private String quadToString(int depth) {
        String str = "";
        String off = "";

        for (int i = 0; i < depth; i++)
            off += "    ";

        if (isLeaf()) {
            str += off + "Leaf" + getPosition() + " - case " + quadCase + ", size " + size + "- " + id + "\n";
            str += off + "    VoxelValue(" + x + ", " + y + ") = " + leafData[0] + "\n";
            str += off + "    VoxelValue(" + (x + size) + ", " + y + ") = " + leafData[1] + "\n";
            str += off + "    VoxelValue(" + (x + size) + ", " + (y + size) + ") = " + leafData[2] + "\n";
            str += off + "    VoxelValue(" + x + ", " + (y + size) + ") = " + leafData[3] + "\n";
            str += neighbors(off);
            return str;
        } else {
            str += off + "QuadTree" + getPosition() + " - case " + quadCase + ", size " + size + "- " + id + "\n";
            str += off + "    VoxelValue(" + x + ", " + y + ") = " + leafData[0] + "\n";
            str += off + "    VoxelValue(" + (x + size) + ", " + y + ") = " + leafData[1] + "\n";
            str += off + "    VoxelValue(" + (x + size) + ", " + (y + size) + ") = " + leafData[2] + "\n";
            str += off + "    VoxelValue(" + x + ", " + (y + size) + ") = " + leafData[3] + "\n";
            str += neighbors(off);
            for (int i = 0; i < 4; i++)
                if (trees[i] != null)
                    str += off + trees[i].quadToString(depth + 1);
            return str;
        }
    }

    private String neighbors(String off) {
        String str = "";
        str += "    Neighbors\n";
        for (int i = 0; i < 6; i++)
            if (neighbors[i] != null)
                str += off + "    Quad" + neighbors[i].getPosition() + " - case "
                        + neighbors[i].quadCase + ", size " + neighbors[i].size
                        + " - " + lod + ", " + neighbors[i].id + "\n";
            else str += "    none\n";
        return str;
    }

    @Override
    public String toString() {
        return quadToString(0);
    }

    public static long zip(int x, int y, int z, int faceCase, boolean bound, long zipKey) {
        long zippedFace = x * (zipKey * zipKey * QuadCases.caseCount)
                + y * (zipKey * QuadCases.caseCount)
                + z * QuadCases.caseCount + faceCase;

        zippedFace++;
        if (bound)
            zippedFace = -zippedFace;

        return zippedFace;
    }

    public static VoxelQuadTree unzip(long zippedFace, long zipKey,
                                      VoxelQuadTree quad) {
        long keySqr = zipKey * zipKey;
        if (quad == null)
            quad = new VoxelQuadTree(0, 0, 0, 0, false, null);

        if (Math.signum(zippedFace) == Math.signum(-1)) {
            quad.bound = true;
            zippedFace = -zippedFace;
        }
        zippedFace--;

        quad.x = zippedFace / (keySqr * QuadCases.caseCount);
        zippedFace = zippedFace % (keySqr * QuadCases.caseCount);

        quad.y = zippedFace / (zipKey * QuadCases.caseCount);
        zippedFace = zippedFace % (zipKey * QuadCases.caseCount);

        quad.z = zippedFace / QuadCases.caseCount;

        quad.quadCase = (int) zippedFace % QuadCases.caseCount;
        return quad;
    }
}
