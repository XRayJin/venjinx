package de.venjinx.core;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import de.venjinx.core.VoxelWorld.VoxelMode;

public final class VoxelObject {

    VoxelObjectProperties properties = new VoxelObjectProperties();

    VoxelMode mode = VoxelMode.PSEUDO_3D_SURFACE;
    //    VoxelMode mode = VoxelMode.SURFACE_2D;
    //    VoxelMode mode = VoxelMode.LINE_2D;
    int maxLOD = 2, chunkSize = 4, extractionLvl = 1;
    float lvlHeight = 1f;

    int sizeX, sizeY, sizeZ, chunksX, chunksY, chunksZ;
    VoxelChunk[][][] chunks;
    HermiteData data;

    private Vector3 currentRefPoint = new Vector3();
    private Vector3 newRefPoint = new Vector3();
    private Vector3 center = new Vector3();

    private boolean needsUpdate, hasChanged = false;

    static ScheduledThreadPoolExecutor threadPool = new ScheduledThreadPoolExecutor(1);
    private HashMap<VoxelChunk, Future<VoxelChunk>> futureChunks = new HashMap<>();
    private LinkedList<VoxelChunk> chunksToGenerate = new LinkedList<>();

    private long generateTime = 0L;

    public VoxelObject(int size) {
        this(new HermiteData(size, size, size));
    }

    public VoxelObject(HermiteData hData) {
        data = hData;

        sizeX = data.getWidth();
        sizeY = data.getHeight();
        sizeZ = data.getDepth();

        createChunks();

        center = new Vector3(sizeX / 2f, sizeY / 2f, sizeZ / 2f);
        needsUpdate = true;
    }

    public void update(float refX, float refY, float refZ) {
        update(newRefPoint.set(refX, refY, refZ));
    }

    public void update(Vector3 refPoint) {
        VoxelChunk chunk;
        Future<VoxelChunk> futureChunk;

        if (!refPoint.equals(currentRefPoint)) {
            needsUpdate = true;
            currentRefPoint.set(refPoint);
        }

        if (needsUpdate) {
            updateChunks();
            needsUpdate = false;
        }

        while (!chunksToGenerate.isEmpty()) {
            chunk = chunksToGenerate.removeFirst();
            if (!futureChunks.containsKey(chunk))
                futureChunks.put(chunk, threadPool.submit(chunk));
        }

        if (!futureChunks.isEmpty())
            for (int x = 0; x < chunksX; x++)
                for (int y = 0; y < chunksY; y++)
                    for (int z = 0; z < chunksZ; z++) {
                        chunk = chunks[x][y][z];
                        futureChunk = futureChunks.get(chunk);
                        if (futureChunk != null && futureChunk.isDone()) {
                            futureChunks.remove(chunk);
                            if (!futureChunk.isCancelled())
                                hasChanged |= chunk.hasNewSurfaces();

                            //                            if (futureChunks.isEmpty())
                            //                                System.out.println("object vertex count: "
                            //                                        + getVertexCount()
                            //                                        + "\ngenerate time: "
                            //                                        + getGenerateTime() / 1000000
                            //                                        + "\n-----------------------------------");
                        }
                    }
    }

    public void updateChunks() {
        chunksToGenerate.clear();
        VoxelChunk chunk;
        Vector3 currentChunkPos = currentRefPoint.div(chunkSize);
        int currentX = (int) currentChunkPos.x;
        int currentY = (int) currentChunkPos.y;
        int currentZ = (int) currentChunkPos.z;
        int tmpX, tmpY, tmpZ, distance;
        int oldLOD;
        for (int x = 0; x < chunksX; x++)
            for (int y = 0; y < chunksY; y++)
                for (int z = 0; z < chunksZ; z++) {
                    chunk = chunks[x][y][z];
                    oldLOD = chunk.getLOD();
                    tmpX = Math.abs(x - currentX);
                    tmpY = Math.abs(y - currentY);
                    tmpZ = Math.abs(z - currentZ);
                    distance = Math.max(tmpX, Math.max(tmpY, tmpZ));
                    if (chunk.update(distance))
                        if (oldLOD > chunk.getLOD())
                            chunksToGenerate.add(chunk);
                        else chunksToGenerate.addFirst(chunk);
                }

        boolean patchCracks;
        for (int x = 0; x < chunksX; x++)
            for (int y = 0; y < chunksY; y++)
                for (int z = 0; z < chunksZ; z++) {
                    patchCracks = false;
                    chunk = chunks[x][y][z];
                    //                    System.out.println("chunk " + x + ", " + y + ", " + z);
                    if (z < chunksZ - 1 && chunk.getLOD() < chunks[x][y][z + 1].getLOD()) {
                        chunk.setSideToPatch(0, true);
                        patchCracks = true;
                    } else chunk.setSideToPatch(0, false);

                    if (z > 0 && chunk.getLOD() < chunks[x][y][z - 1].getLOD()) {
                        chunk.setSideToPatch(1, true);
                        patchCracks = true;
                    } else chunk.setSideToPatch(1, false);

                    if (x < chunksX - 1 && chunk.getLOD() < chunks[x + 1][y][z].getLOD()) {
                        chunk.setSideToPatch(2, true);
                        patchCracks = true;
                    } else chunk.setSideToPatch(2, false);

                    if (x > 0 && chunk.getLOD() < chunks[x - 1][y][z].getLOD()) {
                        chunk.setSideToPatch(3, true);
                        patchCracks = true;
                    } else chunk.setSideToPatch(3, false);

                    if (y < chunksY - 1 && chunk.getLOD() < chunks[x][y + 1][z].getLOD()) {
                        chunk.setSideToPatch(4, true);
                        patchCracks = true;
                    } else chunk.setSideToPatch(4, false);

                    if (y > 0 && chunk.getLOD() < chunks[x][y - 1][z].getLOD()) {
                        chunk.setSideToPatch(5, true);
                        patchCracks = true;
                    } else chunk.setSideToPatch(5, false);

                    if (patchCracks && !chunksToGenerate.contains(chunk))
                        chunksToGenerate.add(chunk);

                    //                    for (boolean b : chunk.getSidesToPatch())
                    //                        System.out.println(b);
                    //                    System.out.println("-----------------------------------------");
                }
    }

    public boolean hasChanged() {
        return hasChanged;
    }

    public void setMode(VoxelMode mode) {
        needsUpdate |= this.mode != mode;
        this.mode = mode;
    }

    public VoxelMode getMode() {
        return mode;
    }

    public void setMaxLOD(int lod) {
        needsUpdate |= maxLOD != lod;
        maxLOD = lod;
    }

    public int getMaxLOD() {
        return maxLOD;
    }

    public void setChunkSize(int size) {
        needsUpdate |= chunkSize != size;
        chunkSize = size;
        createChunks();
    }

    public int getChunkSize() {
        return chunkSize;
    }

    public void setExtractionLvl(int level) {
        needsUpdate |= extractionLvl != level;
        extractionLvl = level;
    }

    public int getExtractionLvl() {
        return extractionLvl;
    }

    public void setLvlHeight(float height) {
        needsUpdate |= lvlHeight != height;
        lvlHeight = height >= 0 ? height : 0;
    }

    public float getLvlHeight() {
        return lvlHeight;
    }

    public HermiteData getData() {
        return data;
    }

    public VoxelChunk[][][] getChunks() {
        return chunks;
    }

    public Vector3 getCenter() {
        return center;
    }

    public int getVertexCount() {
        int count = 0;

        for (int x = 0; x < chunksX; x++)
            for (int y = 0; y < chunksY; y++)
                for (int z = 0; z < chunksZ; z++)
                    count += chunks[x][y][z].getVertextCount();
        return count;
    }

    public long getGenerateTime() {
        long time = 0;

        for (int x = 0; x < chunksX; x++)
            for (int y = 0; y < chunksY; y++)
                for (int z = 0; z < chunksZ; z++)
                    time += chunks[x][y][z].getGenerateTime();
        return time;
    }

    private void createChunks() {
        chunksX = (sizeX - 2) / chunkSize + 1;
        chunksY = (sizeY - 2) / chunkSize + 1;
        chunksZ = (sizeZ - 2) / chunkSize + 1;

        chunks = new VoxelChunk[chunksX][chunksY][chunksZ];
        for (int x = 0; x < chunksX; x++)
            for (int y = 0; y < chunksY; y++)
                for (int z = 0; z < chunksZ; z++)
                    chunks[x][y][z] = new VoxelChunk(x * chunkSize,
                                                     y * chunkSize,
                                                     z * chunkSize,
                                                     this);
    }

    private String treeToString(int depth) {
        String str = "";

        for (int i = 0; i < depth; i++)
            str += "    ";

        // str += "VoxelObject(" + xMin + ", " + yMin + ", " + zMin + ")";
        // str += " - (" + xMax + ", " + yMax + ", " + zMax + ")";
        // str += ", " + size + ", " + lod + ", " + mode + ", " + state;
        // str += " - " + newData + "\n";
        // if (ocTree != null)
        // for (int x = 0; x < 2; x++)
        // for (int y = 0; y < 2; y++)
        // for (int z = 0; z < 2; z++)
        // str += ocTree[x][y][z].treeToString(depth + 1);

        return str;
    }

    @Override
    public String toString() {
        return treeToString(0);
    }
}
