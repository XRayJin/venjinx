package de.venjinx.core;

import de.venjinx.core.CopyOfVoxelQuadTree.QuadCases;

public class VoxelOperations {
    static NormalVertexCreator NormalVertexCreator = new NormalVertexCreator();
    static PatchVertexCreator PatchVertexCreator = new PatchVertexCreator();

    static NormalSegmentBuilder NormalSegmentBuilder = new NormalSegmentBuilder();
    static SmoothSegmentBuilder SmoothSegmentBuilder = new SmoothSegmentBuilder();

    interface VertexCreator {
        public Vector3 create(CopyOfVoxelQuadTree quad, int index, float height);
    }

    interface SegmentBuilder {
        public void build(CopyOfVoxelQuadTree quad, int[] idMap, int segID,
                          float height, int iOff, int vOff);
    }

    static class NormalSegmentBuilder implements SegmentBuilder {
        @Override
        public void build(CopyOfVoxelQuadTree quad, int[] idMap, int segID,
                          float height, int iOff, int vOff) {
            Vector3 n;
            int[] tmpIDs =
                    QuadCases.fillIndexes[quad.ambiguous ? 15 - quad.quadCase
                                                         : quad.quadCase];

            int ambFix = quad.ambiguous ? 1 : 0;
            int segOff = segID > 0 ? 3 : 0;
            int vID0 = segOff + ambFix, vID1 = segOff + 1 - ambFix;

            quad.tIndexes[iOff++] = 0 + vOff;
            quad.tIndexes[iOff++] = 1 + vOff;
            quad.tIndexes[iOff++] = 3 + vOff;
            quad.tIndexes[iOff++] = 3 + vOff;
            quad.tIndexes[iOff++] = 1 + vOff;
            quad.tIndexes[iOff++] = 2 + vOff;

            ambFix = segID > 0 ? -ambFix : ambFix;
            segID += ambFix;

            n = quad.getNormal(segID, idMap);
            Vector4 tang = new Vector4();

            quad.tVerts[vOff] = quad.tVerts[idMap[tmpIDs[vID0]]].copy().subL(0, height, 0);
            quad.tNorms[vOff] = n.copy();
            quad.tTangs[vOff++] = tang.copy();

            quad.tVerts[vOff] = quad.tVerts[idMap[tmpIDs[vID1]]].copy().subL(0, height, 0);
            quad.tNorms[vOff] = n.copy();
            quad.tTangs[vOff++] = tang.copy();

            quad.tVerts[vOff] = quad.tVerts[idMap[tmpIDs[vID1]]].copy();
            quad.tNorms[vOff] = n.copy();
            quad.tTangs[vOff++] = tang.copy();

            quad.tVerts[vOff] = quad.tVerts[idMap[tmpIDs[vID0]]].copy();
            quad.tNorms[vOff] = n.copy();
            quad.tTangs[vOff++] = tang.copy();
        }
    }

    static class SmoothSegmentBuilder implements SegmentBuilder {
        @Override
        public void build(CopyOfVoxelQuadTree quad, int[] idMap, int segID,
                          float height, int iOff, int vOff) {
            int[] tmpIDs = QuadCases.outlineIndexes[quad.ambiguous ? 15 - quad.quadCase
                                                                   : quad.quadCase];
            int segOff = 0;
            Vector3 n;
            Vector4 tang = new Vector4();
            int ambFix = quad.ambiguous ? 1 : 0;

            if (segID > 0)
                segOff = 6;

            quad.tIndexes[iOff++] = idMap[tmpIDs[segOff + ambFix]];
            quad.tIndexes[iOff++] = idMap[tmpIDs[segOff - ambFix + 1]];
            quad.tIndexes[iOff++] = idMap[tmpIDs[segOff + 2]];
            quad.tIndexes[iOff++] = idMap[tmpIDs[segOff + ambFix + 3]];
            quad.tIndexes[iOff++] = idMap[tmpIDs[segOff - ambFix + 4]];
            quad.tIndexes[iOff++] = idMap[tmpIDs[segOff + 5]];

            ambFix = segID > 0 ? -ambFix : ambFix;
            segID += ambFix;
            n = quad.getNormal(segID, idMap);

            quad.tNorms[idMap[tmpIDs[segOff]]].addL(n).normL();
            quad.tTangs[idMap[tmpIDs[segOff]]].addL(tang).normL();

            quad.tNorms[idMap[tmpIDs[segOff + 1]]].addL(n).normL();
            quad.tTangs[idMap[tmpIDs[segOff + 1]]].addL(tang).normL();

            quad.tNorms[idMap[tmpIDs[segOff + 2]]].addL(n).normL();
            quad.tTangs[idMap[tmpIDs[segOff + 2]]].addL(tang).normL();

            quad.tNorms[idMap[tmpIDs[segOff + 3]]].addL(n).normL();
            quad.tTangs[idMap[tmpIDs[segOff + 3]]].addL(tang).normL();
        }
    }

    static class NormalVertexCreator implements VertexCreator {

        @Override
        public Vector3 create(CopyOfVoxelQuadTree quad, int index, float height) {
            Vector3 v;
            float x = quad.x, y = quad.y, z = quad.z, size = quad.size;

            switch (index) {
                case 0:
                case 9:
                    v = new Vector3(x, y, z + size);
                    break;
                case 1:
                case 10:
                    v = new Vector3(x + size, y, z + size);
                    break;
                case 2:
                case 11:
                    v = new Vector3(x + size, y, z);
                    break;
                case 3:
                case 12:
                    v = new Vector3(x, y, z);
                    break;
                case 4:
                case 13:
                    v = new Vector3(x + size / 2, y, z + size);
                    break;
                case 5:
                case 14:
                    v = new Vector3(x + size, y, z + size / 2);
                    break;
                case 6:
                case 15:
                    v = new Vector3(x + size / 2, y, z);
                    break;
                case 7:
                case 16:
                    v = new Vector3(x, y, z + size / 2);
                    break;
                case 8:
                case 17:
                    v = new Vector3(x + size / 2, y, z + size / 2);
                    break;
                default:
                    v = null;
            }

            if (index > 8)
                v.setY(v.y - height);

            return v;
        }
    }

    static class PatchVertexCreator implements VertexCreator {

        @Override
        public Vector3 create(CopyOfVoxelQuadTree quad, int index, float height) {
            Vector3 v;
            float x = quad.x, y = quad.y, z = quad.z;
            float size = quad.size;

            CopyOfVoxelQuadTree q;
            switch (index) {
                case 0:
                case 9:
                    v = new Vector3(x, y, z + size);
                    break;
                case 1:
                case 10:
                    v = new Vector3(x + size, y, z + size);
                    break;
                case 2:
                case 11:
                    v = new Vector3(x + size, y, z);
                    break;
                case 3:
                case 12:
                    v = new Vector3(x, y, z);
                    break;
                case 4:
                case 13:
                    q = quad.patchSides[0] ? quad.getQuad(0) : quad;
                    v = new Vector3(q.x + q.size / 2, q.y, q.z + q.size);
                    break;
                case 5:
                case 14:
                    q = quad.patchSides[1] ? quad.getQuad(1) : quad;
                    v = new Vector3(q.x + q.size, q.y, q.z + q.size / 2);
                    break;
                case 6:
                case 15:
                    q = quad.patchSides[2] ? quad.getQuad(2) : quad;
                    v = new Vector3(q.x + q.size / 2, q.y, q.z);
                    break;
                case 7:
                case 16:
                    q = quad.patchSides[3] ? quad.getQuad(3) : quad;
                    v = new Vector3(q.x, q.y, q.z + q.size / 2);
                    break;
                case 8:
                case 17:
                    v = new Vector3(x + size / 2, y, z + size / 2);
                    break;
                default:
                    v = null;
            }

            if (index > 8)
                v.setY(v.y - height);

            return v;
        }
    }
}