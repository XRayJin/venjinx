package de.venjinx.core;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class HermiteData {

    private Vector3 size = new Vector3();
    private float threshold = 0f, lvlHeight = 1f;
    private int groundLvl = 0;

    private float[][] heightMap;
    private float[][][] densityField;
    private int[][][] normalField;
    private Vector3[][][] normals;

    public HermiteData(int xSize, int ySize, int zSize) {
        size.set(++xSize, ++ySize, ++zSize);
        heightMap = new float[xSize][zSize];
        densityField = new float[xSize][ySize][zSize];
        normalField = new int[xSize][ySize][zSize];
        normals = new Vector3[xSize][ySize][zSize];
    }

    public HermiteData(String densityFileName, int height) {
        loadFromFile(densityFileName, height);
    }

    public HermiteData(BufferedImage densityImage, int height) {
        loadFromImage(densityImage, height);
    }

    public void loadFromFile(String fileName, int height) {
        BufferedImage image;
        try {
            image = ImageIO.read(new File(fileName));
            loadFromImage(image, height);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadFromImage(BufferedImage image, int height) {
        int xMax = image.getWidth() + 1;
        int yMax = height + 1;
        int zMax = image.getHeight() + 1;
        size.set(xMax, yMax, zMax);
        densityField = new float[xMax][yMax][zMax];
        normalField = new int[xMax][yMax][zMax];
        normals = new Vector3[xMax][yMax][zMax];

        heightMap = convertHeightImage(image);

        convertToHermite();
    }

    private void convertToHermite() {
        int xMax = (int) size.x;
        int yMax = (int) size.y;
        int zMax = (int) size.z;

        float d, h;
        for (int x = 0; x < xMax; x++)
            for (int y = 0; y < yMax; y++)
                for (int z = 0; z < zMax; z++) {
                    h = getHeight(x, z);
                    d = heightToDensity(h, y);
                    densityField[x][y][z] = d;
                }

        calcNormals();
    }

    public float getThreshold() {
        return threshold;
    }

    public void setThreshold(float value) {
        if (threshold != value) {
            threshold = value;
            convertToHermite();
        }
    }

    public int getGroundLvl() {
        return groundLvl;
    }

    public void setGroundLvl(int level) {
        if (groundLvl != level) {
            groundLvl = level;
            convertToHermite();
        }
    }

    private float heightToDensity(float value, float yPos) {
        return heightToDensity(yPos, value, groundLvl, threshold);
    }

    private float
    heightToDensity(float yPos, float value, int groundLvl, float threshold) {
        float density = (value - threshold) * size.y - groundLvl - yPos;
        density = density > 1 ? 1 : density < -1 ? -1 : density;
        return density;
    }

    private void calcNormals() {
        float nx, ny, nz;
        int gx0, gx1, gy0, gy1, gz0, gz1;

        for (int x = 0; x < size.x; x++)
            for (int y = 0; y < size.y; y++)
                for (int z = 0; z < size.z; z++) {
                    gx0 = x + (x > 0 ? -1 : 0);
                    gx1 = x + (x < size.x - 1 ? 1 : 0);
                    nx = densityField[gx0][y][z] - densityField[gx1][y][z];

                    gy0 = y + (y > 0 ? -1 : 0);
                    gy1 = y + (y < size.y - 1 ? 1 : 0);
                    ny = densityField[x][gy0][z] - densityField[x][gy1][z];

                    gz0 = z + (z > 0 ? -1 : 0);
                    gz1 = z + (z < size.z - 1 ? 1 : 0);
                    nz = densityField[x][y][gz0] - densityField[x][y][gz1];

                    normals[x][y][z] = new Vector3(nx, ny, nz).normL();
                }
    }

    //    public Vector3 getNormal(int x, int y, int z) {
    //        //        Vector3 normal = new Vector3();
    //        //        int pixel = normalField[x][y][z];
    //        //
    //        //        normal.x = ((pixel >> 16 & 0xff) + 1) / 256f;
    //        //        normal.y = ((pixel >> 8 & 0xff) + 1) / 256f;
    //        //        normal.z = ((pixel & 0xff) + 1) / 256f;
    //        //        normal.multL(2).subL(1, 1, 1).normL();
    //        //        return normal;
    //
    //        return normals[x][y][z];
    //    }

    public Vector3 getNormal(int x, int y, int z) {
        return normals[x][y][z];
    }

    public Vector3 getNormal(float x, float y, float z) {
        if (x < 0 || x >= size.x || y < 0 || y >= size.y || z < 0 || z >= size.z)
            return new Vector3();

        float nx, ny, nz;
        int gx0 = (int) x, gy0 = (int) y, gz0 = (int) z;
        //        if (x - gx0 == 0 && y - gy0 == 0 && z - gz0 == 0)
        //            return getNormal(gx0, gy0, gz0);

        int gx1 = gx0 + (x + 1 < size.x ? 1 : 0);
        int gy1 = gy0 + (y + 1 < size.y ? 1 : 0);
        int gz1 = gz0 + (z + 1 < size.z ? 1 : 0);

        nx = getDensity(gx0, gy0, gz0) - getDensity(gx1, gy0, gz0);
        nx += getDensity(gx0, gy0, gz1) - getDensity(gx1, gy0, gz1);
        nx += getDensity(gx0, gy1, gz0) - getDensity(gx1, gy1, gz0);
        nx += getDensity(gx0, gy1, gz1) - getDensity(gx1, gy1, gz1);
        nx /= 4;
        nx *= x == 0 || x == size.x - 1 ? 2 : 1;

        ny = getDensity(gx0, gy0, gz0) - getDensity(gx0, gy1, gz0);
        ny += getDensity(gx0, gy0, gz1) - getDensity(gx0, gy1, gz1);
        ny += getDensity(gx1, gy0, gz0) - getDensity(gx1, gy1, gz0);
        ny += getDensity(gx1, gy0, gz1) - getDensity(gx1, gy1, gz1);
        ny /= 4;
        ny *= y == 0 || y == size.y - 1 ? 2 : 1;

        nz = getDensity(gx0, gy0, gz0) - getDensity(gx0, gy0, gz1);
        nz += getDensity(gx0, gy1, gz0) - getDensity(gx0, gy1, gz1);
        nz += getDensity(gx1, gy0, gz0) - getDensity(gx1, gy0, gz1);
        nz += getDensity(gx1, gy1, gz0) - getDensity(gx1, gy1, gz1);
        nz /= 4;
        nz *= z == 0 || z == size.z - 1 ? 2 : 1;

        //        gx0 = x + (x > 0 ? -1 : 0);
        //        gx1 = x + (x < size.x - 1 ? 1 : 0);
        //        nx = getInterpolatedDensity(gx0, y, z) - getInterpolatedDensity(gx1, y, z);
        //        nx *= x == 0 || x == size.x - 1 ? 2 : 1;
        //
        //        gy0 = y + (y > 0 ? -1 : 0);
        //        gy1 = y + (y < size.y - 1 ? 1 : 0);
        //        ny = getInterpolatedDensity(x, gy0, z) - getInterpolatedDensity(x, gy1, z);
        //        ny *= y == 0 || y == size.y - 1 ? 2 : 1;
        //
        //        gz0 = z + (z > 0 ? -1 : 0);
        //        gz1 = z + (z < size.z - 1 ? 1 : 0);
        //        nz = getInterpolatedDensity(x, y, gz0) - getInterpolatedDensity(x, y, gz1);
        //        nz *= z == 0 || z == size.z - 1 ? 2 : 1;

        //        if (y == 3 || y == 4) {
        //            System.out.print("(" + x + ", " + y + ", " + z + ") d = "
        //                    + getInterpolatedDensity(x, y, z));
        //            System.out.println(", " + new Vector3(nx, ny, nz).normL());
        //        }

        return new Vector3(nx, ny, nz).normL();
    }

    public Vector3 getNormal(Vector3 pos) {
        return getNormal(pos.x, pos.y, pos.z);
    }

    public int getWidth() {
        return (int) size.x;
    }

    public int getHeight() {
        return (int) size.y;
    }

    public int getDepth() {
        return (int) size.z;
    }

    public Vector3 getDimensions() {
        return size;
    }

    public float[][][] getDensityField() {
        return densityField;
    }

    public float getHeight(int x, int z) {
        if (heightMap == null)
            return -1;

        if (x < 0 || x >= size.x || z < 0 || z >= size.z)
            return 0;
        return heightMap[x][z];
    }

    public float getInterpolatedHeight(Vector2 pos) {
        return getInterpolatedHeight(pos.x, pos.y);
    }

    public float getInterpolatedHeight(float x, float z) {
        if (heightMap == null)
            return -1;

        if (x < 0 || x >= size.x || z < 0 || z >= size.z)
            return -1;

        int gx = (int) x, gz = (int) z;
        float interX = x - gx, interZ = z - gz;

        if (interX == 0 && interZ == 0)
            return heightMap[gx][gz];

        float[][] cell = new float[2][2];
        float dInt0, dInt1;

        cell[0][0] = heightMap[gx][gz];
        cell[0][1] = heightMap[gx][gz + 1];
        cell[1][0] = heightMap[gx + 1][gz];
        cell[1][1] = heightMap[gx + 1][gz + 1];

        gx = interX > 0 ? 1 : 0;
        gz = interZ > 0 ? 1 : 0;

        dInt0 = cell[0][0];
        dInt1 = cell[0][gz];
        if (interX > 0) {
            dInt0 += (cell[1][0] - cell[0][0]) * interX;
            dInt1 += (cell[1][gz] - cell[0][gz]) * interX;
        }
        interX = (dInt0 + dInt1) / 2;

        dInt0 = cell[0][0];
        dInt1 = cell[gx][0];
        if (interZ > 0) {
            dInt0 += (cell[0][1] - cell[0][0]) * interZ;
            dInt1 += (cell[gx][1] - cell[gx][0]) * interZ;
        }
        interZ = (dInt0 + dInt1) / 2;

        return (interX + interZ) / 2;
    }

    public float getSampleDensity(int x, int y, int z, int groundLvl, float threshold) {
        if (x < 0 || x >= size.x || y < 0 || y >= size.y || z < 0 || z >= size.z)
            return -1;

        if (heightMap != null)
            return heightToDensity(y, heightMap[x][z], groundLvl, threshold);

        return getDensity(x, y, z);
    }

    public float getSampleDensity(Vector3 pos, int groundLvl, float threshold) {
        return getSampleDensity(pos.x, pos.y, pos.z, groundLvl, threshold);
    }

    public float getSampleDensity(float x, float y, float z, int groundLvl,
                                  float threshold) {
        if (x < 0 || x >= size.x || y < 0 || y >= size.y || z < 0 || z >= size.z)
            return -1;

        if (heightMap != null)
            return heightToDensity(y, getInterpolatedHeight(x, z), groundLvl, threshold);

        return getInterpolatedDensity(x, y, z);
    }

    public float getDensity(int x, int y, int z) {
        if (x < 0 || x >= size.x || y < 0 || y >= size.y || z < 0 || z >= size.z)
            return -1;
        return densityField[x][y][z];
    }

    public float getInterpolatedDensity(Vector3 pos) {
        return getInterpolatedDensity(pos.x, pos.y, pos.z);
    }

    public float getInterpolatedDensity(float x, float y, float z) {
        if (x < 0 || x > size.x - 1 || y < 0 || y > size.y - 1 || z < 0 || z > size.z - 1)
            return -1;

        int gx = (int) x, gy = (int) y, gz = (int) z;
        float interX = x - gx, interY = y - gy, interZ = z - gz;

        if (interX == 0 && interY == 0 && interZ == 0)
            return densityField[gx][gy][gz];

        float[][][] cell = new float[2][2][2];
        float dInt0, dInt1, dInt2, dInt3;

        cell[0][0][0] = getDensity(gx, gy, gz);
        cell[0][0][1] = getDensity(gx, gy, gz + 1);
        cell[0][1][0] = getDensity(gx, gy + 1, gz);
        cell[0][1][1] = getDensity(gx, gy + 1, gz + 1);
        cell[1][0][0] = getDensity(gx + 1, gy, gz);
        cell[1][0][1] = getDensity(gx + 1, gy, gz + 1);
        cell[1][1][0] = getDensity(gx + 1, gy + 1, gz);
        cell[1][1][1] = getDensity(gx + 1, gy + 1, gz + 1);

        //        System.out.println(cell[0][0][0]);
        //        System.out.println(cell[0][0][1]);
        //        System.out.println(cell[0][1][0]);
        //        System.out.println(cell[0][1][1]);
        //        System.out.println(cell[1][0][0]);
        //        System.out.println(cell[1][0][1]);
        //        System.out.println(cell[1][1][0]);
        //        System.out.println(cell[1][1][1]);

        gx = interX > 0 ? 1 : 0;
        gy = interY > 0 ? 1 : 0;
        gz = interZ > 0 ? 1 : 0;

        dInt0 = cell[0][0][0];
        dInt1 = cell[0][0][gz];
        dInt2 = cell[0][gy][0];
        dInt3 = cell[0][gy][gz];
        float step;
        if (interX > 0) {
            //            step =
            //                   Math.abs(cell[0][0][0])
            //                           / (Math.abs(cell[0][0][0]) + Math.abs(cell[gx][0][0]));
            dInt0 += (cell[gx][0][0] - cell[0][0][0]) * interX;

            //            step =
            //                   Math.abs(cell[0][0][gz])
            //                           / (Math.abs(cell[0][0][gz]) + Math.abs(cell[gx][0][gz]));
            dInt1 += (cell[gx][0][gz] - cell[0][0][gz]) * interX;

            //            step =
            //                   Math.abs(cell[0][gy][0])
            //                           / (Math.abs(cell[0][gy][0]) + Math.abs(cell[gx][gy][0]));
            dInt2 += (cell[gx][gy][0] - cell[0][gy][0]) * interX;

            //            step =
            //                   Math.abs(cell[0][gy][gz])
            //                           / (Math.abs(cell[0][gy][gz]) + Math.abs(cell[gx][gy][gz]));
            dInt3 += (cell[gx][gy][gz] - cell[0][gy][gz]) * interX;
        }
        interX = (dInt0 + dInt1 + dInt2 + dInt3) / 4;

        dInt0 = cell[0][0][0];
        dInt1 = cell[0][0][gz];
        dInt2 = cell[gx][0][0];
        dInt3 = cell[gx][0][gz];
        if (interY > 0) {
            //            step =
            //                   Math.abs(cell[0][0][0])
            //                           / (Math.abs(cell[0][0][0]) + Math.abs(cell[0][gy][0]));
            dInt0 += (cell[0][gy][0] - cell[0][0][0]) * interY;

            //            step =
            //                   Math.abs(cell[0][0][gz])
            //                           / (Math.abs(cell[0][0][gz]) + Math.abs(cell[0][gy][gz]));
            dInt1 += (cell[0][gy][gz] - cell[0][0][gz]) * interY;

            //            step =
            //                   Math.abs(cell[gx][0][0])
            //                           / (Math.abs(cell[gx][0][0]) + Math.abs(cell[gx][gy][0]));
            dInt2 += (cell[gx][gy][0] - cell[gx][0][0]) * interY;

            //            step =
            //                   Math.abs(cell[gx][0][gz])
            //                           / (Math.abs(cell[gx][0][gz]) + Math.abs(cell[gx][gy][gz]));
            dInt3 += (cell[gx][gy][gz] - cell[gx][0][gz]) * interY;
        }
        interY = (dInt0 + dInt1 + dInt2 + dInt3) / 4;

        dInt0 = cell[0][0][0];
        dInt1 = cell[0][gy][0];
        dInt2 = cell[gx][0][0];
        dInt3 = cell[gx][gy][0];
        if (interZ > 0) {
            //            step =
            //                   Math.abs(cell[0][0][0])
            //                           / (Math.abs(cell[0][0][0]) + Math.abs(cell[0][0][gz]));
            dInt0 += (cell[0][0][gz] - cell[0][0][0]) * interZ;

            //            step =
            //                   Math.abs(cell[0][gy][0])
            //                           / (Math.abs(cell[0][gy][0]) + Math.abs(cell[0][gy][gz]));
            dInt1 += (cell[0][gy][gz] - cell[0][gy][0]) * interZ;

            //            step =
            //                   Math.abs(cell[gx][0][0])
            //                           / (Math.abs(cell[gx][0][0]) + Math.abs(cell[gx][0][gz]));
            dInt2 += (cell[gx][0][gz] - cell[gx][0][0]) * interZ;

            //            step =
            //                   Math.abs(cell[gx][gy][0])
            //                           / (Math.abs(cell[gx][gy][0]) + Math.abs(cell[gx][gy][gz]));
            dInt3 += (cell[gx][gy][gz] - cell[gx][gy][0]) * interZ;
        }
        interZ = (dInt0 + dInt1 + dInt2 + dInt3) / 4;

        return (interX + interY + interZ) / 3;
    }

    private float[][] convertHeightImage(BufferedImage heightMap) {
        int xMax = heightMap.getWidth();
        int yMax = heightMap.getHeight();
        int halfX = heightMap.getWidth() / 2;
        int halfY = heightMap.getHeight() / 2;

        float[][] h = new float[xMax + 1][yMax + 1];

        for (int x = 0; x < halfX; x++)
            for (int y = 0; y < yMax; y++) {
                h[x][y] = argbToGrayScale(heightMap.getRGB(x, y));
                h[xMax - x][y] = argbToGrayScale(heightMap.getRGB(xMax - x - 1, y));
            }

        for (int y = 0; y < yMax; y++)
            h[halfX][y] = (argbToGrayScale(heightMap.getRGB(halfX - 1, y))
                    + argbToGrayScale(heightMap.getRGB(halfX, y))) / 2;

        for (int x = 0; x < xMax; x++)
            for (int y = 0; y < halfY; y++)
                h[x][yMax - y] = h[x][yMax - y - 1];

        for (int x = 0; x < xMax; x++)
            h[x][halfY] = (h[x][halfY - 1] + h[x][halfY]) / 2;

        h[xMax][yMax] = (h[xMax - 1][yMax] + h[xMax][yMax - 1]) / 2;

        return h;
    }

    private int rgbToPixel(int r, int g, int b) {
        return 0xff000000 | r << 16 | g << 8 | b;
    }

    private float argbToGrayScale(int argb) {
        return (.299f * (argb >> 16 & 0xff)
                + .587f * (argb >> 8 & 0xff)
                + .114f * (argb & 0xff)) / 255f;
    }

    //    private float matchToResolution(float value, int maxLOD) {
    //        float resolution = FastMath.pow(2, -maxLOD);
    //        value /= resolution;
    //        value = Math.round(value);
    //        value *= resolution;
    //        return value;
    //    }

    //  private Vector3[][] fillNormalField(float[][] heightField) {
    //      int xMax = (int) size.x;
    //      int yMax = (int) size.z;
    //      Vector3 v0 = new Vector3(), v1 = new Vector3(), normal;
    //
    //      Vector3[][] normals = new Vector3[xMax][yMax];
    //
    //      for (int x = 1; x < xMax - 1; x++)
    //          for (int y = 1; y < yMax - 1; y++) {
    //
    //              // Construct both vectors with the surface difference at this point
    //              // (vec3 is a vector in 3d space)
    //              //
    //              // Observe that the z coordinate holds the x and y perturbation of the
    //              // image, you can do this because you are assuming that the image actually
    //              // represents a "surface", so, the x and y vector are altered in z due to
    //              // to the bumpness of the surface at that point
    //
    //              v0.set(0, heightField[x - 1][y] - heightField[x + 1][y], 1);
    //              v1.set(1, heightField[x][y - 1] - heightField[x][y + 1], 0);
    //
    //              // Given vx and vy, you have both the x and y basis of the coordenate system
    //              // tangent to the "surface" at the point, to get the normal, calculate
    //              // the cross product of vx and vy
    //
    //              normal = v0.cross(v1).normL();
    //              normals[x][y] = normal;
    //
    //              //                if (normal.z != -1)
    //              //                    System.out.println("x = " + x + ", y = " + y + ", " + normal);
    //
    //              // Now, the tricky part, store the normal as a RGB pixel in the output image
    //              // to do that, remember that the x, y and z coordenates of the vector lies
    //              // in range [-1, 1] (because is a normalized vector), then you have to
    //              // take then to the range [0,255], so we first take then from [-1, 1]
    //              // to [0, 1]
    //              //
    //              //   -1 is equivalent to 0
    //              //    0 would be equivalent to 0.5
    //              //    1 would be equivalent to 1
    //              //
    //              // with this info, we deduce the following formula:
    //              //   pixel_component <- (coordinate + 1) / 2
    //              //
    //              // Multiply this by 255 and we are done for
    //
    //              int r = (int) ((normal.x + 1) / 2 * 255);
    //              int g = (int) ((normal.y + 1) / 2 * 255);
    //              int b = (int) ((normal.z + 1) / 2 * 255);
    //
    //              //                if (g != 255) {
    //              //                System.out.println(x + ", " + y);
    //              //                System.out.println("r " + r + ", g " + g + ", b " + b + " - " + normal);
    //              //                }
    //
    //              //  Clamp the pixel components if neccesary
    //              //
    //              //  Store the pixel in the output image
    //              normalField[x][0][y] = rgbToPixel(r, g, b);
    //          }
    //      return normals;
    //  }
}
