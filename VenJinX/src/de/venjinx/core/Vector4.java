package de.venjinx.core;

/**
 * <code>Vector4</code> defines a four dimensional vector using float values.
 * It provides the basic methods for adding, subtracting, multiplying and
 * dividing vectors. Furthermore methods for negating and normalizing a vector,
 * getting the length and the determinant of a vector and calculating
 * dot product, cross product and distance between two vectors are also
 * provided.
 * 
 * @author Torge Rothe (X-Ray-Jin)
 */
public final class Vector4 {

    /**
     * This vectors x value.
     */
    public float x;

    /**
     * This vectors y value.
     */
    public float y;

    /**
     * This vectors z value.
     */
    public float z;

    /**
     * This vectors w value.
     */
    public float w;

    /**
     * Creates a new Vector4 and initializes the x, y, z, w values with 0.
     */
    public Vector4() {
        x = y = z = w = 0;
    }

    /**
     * Creates a new Vector4 with the provided initial x, y, z, w values.
     * 
     * @param x
     *            The initial x value.
     * @param y
     *            The initial y value.
     * @param z
     *            The initial z value.
     * @param w
     *            The initial w value.
     */
    public Vector4(float x, float y, float z, float w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    /**
     * Creates a new Vector4 using a Vector3 for the x, y, z values and the
     * initial w value.
     * 
     * @param v
     *            The three dimensional vector used as the base for x, y, z
     *            values.
     * @param w
     *            The initial w value.
     */
    public Vector4(Vector3 v, float w) {
        x = v.x;
        y = v.y;
        z = v.z;
        this.w = w;
    }

    /**
     * Creates a new Vector4 and initializes the x, y, z, w values with the x,
     * y, z, w values of the provided vector.
     * 
     * @param copy
     *            The vector to copy the values from.
     */
    public Vector4(Vector4 copy) {
        x = copy.x;
        y = copy.y;
        z = copy.z;
        w = copy.w;
    }

    /**
     * Sets this vectors x value to provided x value.
     * 
     * @param newX
     *            The new x value.
     * @return This vector with the new x value.
     */
    public Vector4 setX(float newX) {
        x = newX;
        return this;
    }

    /**
     * Sets this vectors y value to the provided y value.
     * 
     * @param newY
     *            The new y value.
     * @return This vector with the new y value.
     */
    public Vector4 setY(float newY) {
        y = newY;
        return this;
    }

    /**
     * Sets this vectors z value to the provided z value.
     * 
     * @param newZ
     *            The new z value.
     * @return This vector with the new z value.
     */
    public Vector4 setZ(float newZ) {
        z = newZ;
        return this;
    }

    /**
     * Sets this vectors w value to the provided w value.
     * 
     * @param newZ
     *            The new z value.
     * @return This vector with the new z value.
     */
    public Vector4 setW(float newW) {
        w = newW;
        return this;
    }

    /**
     * Sets this vectors x, y, z, w values to given x, y, z, w values.
     * 
     * @param newX
     *            The new x value.
     * @param newY
     *            The new y value.
     * @param newZ
     *            The new z value.
     * @param newW
     *            The new W value.
     * @return This vector with the new x, y, z, w values.
     */
    public Vector4 set(float newX, float newY, float newZ, float newW) {
        x = newX;
        y = newY;
        z = newZ;
        w = newW;
        return this;
    }

    /**
     * Sets this vectors x, y, z, w values to the other vectors x, y, z, w
     * values.
     * 
     * @param copy
     *            The vector to copy the x, y, z, w values from.
     * @return This vector with the new x, y, z, w values.
     */
    public Vector4 set(Vector4 copy) {
        x = copy.x;
        y = copy.y;
        z = copy.z;
        w = copy.w;
        return this;
    }

    /**
     * Adds the provided x, y, z, w values to this vectors x, y, z, w values and
     * saves the result in a newly created vector and returns the new vector.
     * 
     * @param addX
     *            Value to add to x.
     * @param addY
     *            Value to add to y.
     * @param addZ
     *            Value to add to z.
     * @param add
     *            Value to add to 2.
     * @return A new vector with the added x, y, z, w values.
     */
    public Vector4 add(float addX, float addY, float addZ, float addW) {
        return new Vector4(x + addX, y + addY, z + addZ, w + addW);
    }

    /**
     * Adds the provided vectors x, y, z, w values to this vectors x, y, z, w
     * values and saves the result in a newly created vector and returns the new
     * vector.
     * 
     * @param other
     *            The vector to add to this vector.
     * @return A new vector with the added x, y, z, w values.
     */
    public Vector4 add(Vector4 other) {
        return new Vector4(x + other.x, y + other.y, z + other.z, w + other.w);
    }

    /**
     * Adds the provided x, y, z, w values to this vectors x, y, z, w values
     * locally and returns this vector.
     * 
     * @param addX
     *            Value to add to x.
     * @param addY
     *            Value to add to y.
     * @param addZ
     *            Value to add to z.
     * @param addW
     *            Value to add to w.
     * @return This vector with the added x, y, z, w values.
     */
    public Vector4 addL(float addX, float addY, float addZ, float addW) {
        x += addX;
        y += addY;
        z += addZ;
        w += addW;
        return this;
    }

    /**
     * Adds the provided vectors x, y, z, w values to this vectors x, y, z, w
     * values locally and returns this vector.
     * 
     * @param other
     *            The vector to add to this vector.
     * @return This vector with the added x, y, z, w values.
     */
    public Vector4 addL(Vector4 other) {
        x += other.x;
        y += other.y;
        z += other.z;
        w += other.w;
        return this;
    }

    /**
     * Subtracts the provided x, y, z, w values from this vectors x, y, z, w
     * values and saves the result in a newly created vector and returns the new
     * vector.
     * 
     * @param subX
     *            Value to subtract from x.
     * @param subY
     *            Value to subtract from y.
     * @param subZ
     *            Value to subtract from z.
     * @param subW
     *            Value to subtract from w.
     * @return A new vector with the subtracted x, y, z, w values.
     */
    public Vector4 sub(float subX, float subY, float subZ, float subW) {
        return new Vector4(x - subX, y - subY, z - subZ, w - subW);
    }

    /**
     * Subtracts the provided vectors x, y, z, w values from this vectors
     * x, y, z, w values and saves the result in a newly created vector and
     * returns the new vector.
     * 
     * @param other
     *            The vector to subtract from this vector.
     * @return A new vector with the subtracted x, y, z, w values.
     */
    public Vector4 sub(Vector4 other) {
        return new Vector4(x - other.x, y - other.y, z - other.z, w - other.w);
    }

    /**
     * Subtracts the provided x, y, z, w values from this vectors x, y, z, w
     * values locally and returns this vector.
     * 
     * @param subX
     *            Value to subtract from x.
     * @param subY
     *            Value to subtract from y.
     * @param subZ
     *            Value to subtract from z.
     * @param subW
     *            Value to subtract from w.
     * @return This vector with the subtracted x, y, z, w values.
     */
    public Vector4 subL(float subX, float subY, float subZ, float subW) {
        x -= subX;
        y -= subY;
        z -= subZ;
        w -= subW;
        return this;
    }

    /**
     * Subtracts the provided vectors x, y, z, w values from this vectors
     * x, y, z, w values locally and returns this vector.
     * 
     * @param other
     *            The vector to subtract from this vector.
     * @return This vector with the subtracted x, y, z, w values.
     */
    public Vector4 subL(Vector4 other) {
        x -= other.x;
        y -= other.y;
        z -= other.z;
        w -= other.w;
        return this;
    }

    /**
     * Multiplies this vectors x, y, z, w values by a scalar and saves the
     * result in a newly created vector which is returned.
     * 
     * @param scalar
     *            The value to multiply this vector by.
     * @return A new vector with the multiplied x, y, z, w values.
     */
    public Vector4 mult(float scalar) {
        return new Vector4(x * scalar, y * scalar, z * scalar, w * scalar);
    }

    /**
     * Multiplies this vectors x, y, z, w values by a provided vectors
     * x, y, z, w values and saves the result in a newly created vector and
     * returns the new vector.
     * 
     * @param other
     *            The vector to multiply this vector by.
     * @return A new vector with the multiplied x, y, z, w values.
     */
    public Vector4 mult(Vector4 other) {
        x *= other.x;
        y *= other.y;
        z *= other.z;
        w *= other.w;
        return this;
    }

    /**
     * Multiplies this vectors x, y, z, w values locally by a scalar and returns
     * this vector.
     * 
     * @param scalar
     *            The value to multiply this vector by.
     * @return This vector with the multiplied x, y, z, w values.
     */
    public Vector4 multL(float scalar) {
        x *= scalar;
        y *= scalar;
        z *= scalar;
        w *= scalar;
        return this;
    }

    /**
     * Multiplies this vectors x, y, z, w values by a provided vectors
     * x, y, z, w values locally and returns this vector.
     * 
     * @param other
     *            The vector to multiply this vector by.
     * @return This vector with the multiplied x, y, z, w values.
     */
    public Vector4 multL(Vector4 other) {
        x *= other.x;
        y *= other.y;
        z *= other.z;
        w *= other.w;
        return this;
    }

    /**
     * Divides this vectors x, y, z, w values by a scalar and saves the result
     * in a newly created vector which is returned.
     * 
     * @param scalar
     *            The value to divide this vector by.
     * @return A new vector with the divided x, y, z, w values.
     */
    public Vector4 div(float scalar) {
        return new Vector4(x / scalar, y / scalar, z / scalar, w / scalar);
    }

    /**
     * Divides this vectors x, y, z, w values by a provided vectors x, y, z, w
     * values and saves the result in a newly created vector and returns the new
     * vector.
     * 
     * @param other
     *            The vector to divide this vector by.
     * @return A new vector with the divided x, y, z, w values.
     */
    public Vector4 div(Vector4 other) {
        return new Vector4(x / other.x, y / other.y, z / other.z, w / other.w);
    }

    /**
     * Divides this vectors x, y, z, w values locally by a scalar and returns
     * this vector.
     * 
     * @param scalar
     *            The value to divide this vector by.
     * @return This vector with the divided x, y, z, w values.
     */
    public Vector4 divL(float scalar) {
        x /= scalar;
        y /= scalar;
        z /= scalar;
        w /= scalar;
        return this;
    }

    /**
     * Divides this vectors x, y, z, w values by a provided vectors x, y, z, w
     * values locally and returns this vector.
     * 
     * @param other
     *            The vector to divide this vector by.
     * @return This vector with the divided x, y, z, w values.
     */
    public Vector4 divL(Vector4 other) {
        x /= other.x;
        y /= other.y;
        z /= other.z;
        w /= other.w;
        return this;
    }

    /**
     * Negates this vectors x, y, z, w values and saves the result in a newly
     * created vector and returns the new vector.
     * 
     * @return A new vector with the negated x, y, z, w values.
     */
    public Vector4 negate() {
        return new Vector4(-x, -y, -z, -w);
    }

    /**
     * Negates this vectors x, y, z, w values locally and returns this vector.
     * 
     * @return This vector with the negated x, y, z, w values.
     */
    public Vector4 negateL() {
        x = -x;
        y = -y;
        z = -z;
        w = -w;
        return this;
    }

    /**
     * Normalizes this vectors values and saves the normalized values in a
     * newly created vector and returns the new unit vector.
     * 
     * @return A new unit vector with the normalized x, y, z, w values.
     */
    public Vector4 norm() {
        float length = x * x + y * y + z * z + w * w;
        if (length != 1f && length != 0f) {
            length = 1f / (float) Math.sqrt(length);
            return new Vector4(x * length, y * length, z * length, w * length);
        }
        return new Vector4(this);
    }

    /**
     * Normalizes this vectors values locally and returns this vector as
     * unit vector.
     * 
     * @return This unit vector with the normalized x, y, z, w values.
     */
    public Vector4 normL() {
        float length = x * x + y * y + z * z + w * w;
        if (length != 1f && length != 0f) {
            length = 1f / (float) Math.sqrt(length);
            x *= length;
            y *= length;
            z *= length;
            w *= length;
        }
        return this;
    }

    /**
     * Calculates the dot product of this vector and a provided vector.
     * 
     * @param other
     *            The vector to dot with this vector.
     * @return The result of the dot product.
     */
    public float dot(Vector4 other) {
        return x * other.x + y * other.y + z * other.z;
    }

    /**
     * Calculates the length/magnitude of this vector.
     * 
     * @return The length/magnitude of this vector.
     */
    public float length() {
        float length = x * x + y * y + z * z + w * w;
        return length != 1 && length != 0 ? (float) Math.sqrt(length) : length;
    }

    /**
     * Calculates the distance between this vector and a provided vector.
     * 
     * @param other
     *            The vector to get the distance to.
     * @return The distance between this and the provided vector.
     */
    public float distance(Vector4 other) {
        float dx = x - other.x;
        float dy = y - other.y;
        float dz = z - other.z;
        float dw = w - other.w;
        return (float) Math.sqrt(dx * dx + dy * dy + dz * dz + dw * dw);
    }

    /**
     * Creates a new Vector and copies over the x, y, z values of this vector.
     * 
     * @return A new Vector with the x, y, z values of this vector.
     */
    public Vector4 copy() {
        return new Vector4(this);
    }

    /**
     * Checks whether this and the provided object are equivalent, i.e if the
     * provided object is an instance of Vector4 and the x, y, z, w values of
     * the vectors are the same or they are the same object.
     * 
     * @param o
     *            The object to compare with this vector.
     * @return True if they are equivalent or the same.
     */
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Vector4))
            return false;

        if (this == o)
            return true;

        Vector4 other = (Vector4) o;
        return Float.compare(x, other.x) == 0
                && Float.compare(y, other.y) == 0
                && Float.compare(z, other.z) == 0
                && Float.compare(w, other.w) == 0;
    }

    /**
     * Returns a string containing the textual representation of this vector.
     * Format: (valueX, valueY, valueZ, valueW)
     * 
     * @return The string representation of this vector.
     */
    @Override
    public String toString() {
        return "(" + x + ", " + y + ", " + z + ", " + w + ")";
    }
}