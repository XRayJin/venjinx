package de.venjinx.jme3.scenegraph;

import java.util.EventObject;

import com.jme3.scene.Spatial;

import de.venjinx.jme3.scenegraph.SGNodeListener.NodeChangedEvent;
import de.venjinx.jme3.scenegraph.SGNodeListener.NodeChangedEvent.EventType;

public interface SGListener {

    public class GraphChangedEvent extends EventObject {
        private static final long serialVersionUID = 4493645377758962009L;

        private NodeChangedEvent nodeChangedEvent;

        public GraphChangedEvent(Scenegraph graph, NodeChangedEvent e) {
            super(graph);
            nodeChangedEvent = e;
        }

        public Scenegraph getGraph() {
            return (Scenegraph) source;
        }

        public SGNode getParent() {
            return nodeChangedEvent.getParent();
        }

        public Spatial getChild() {
            return nodeChangedEvent.getChild();
        }

        public EventType getType() {
            return nodeChangedEvent.getType();
        }
    }

    public void onGraphChanged(GraphChangedEvent e);

}