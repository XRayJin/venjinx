package de.venjinx.jme3.scenegraph;

import java.util.EventObject;

import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public interface SGNodeListener {

    public class NodeChangedEvent extends EventObject {
        public enum EventType { ATTACHED, DETACHED };

        private static final long serialVersionUID = -7106735648769020235L;
        private Spatial child;
        private EventType type;

        public NodeChangedEvent(Node parent, Spatial child, EventType changeType) {
            super(parent);
            type = changeType;
            this.child = child;
        }

        public SGNode getParent() {
            return (SGNode) source;
        }

        public Spatial getChild() {
            return child;
        }

        public EventType getType() {
            return type;
        }
    }

    public void onNodeChanged(NodeChangedEvent e);

}