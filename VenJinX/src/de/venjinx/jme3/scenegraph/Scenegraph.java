package de.venjinx.jme3.scenegraph;

import java.util.HashSet;

import de.venjinx.jme3.scenegraph.SGListener.GraphChangedEvent;

public class Scenegraph implements SGNodeListener {

    private SGNode rootNode = new SGNode("Scene root node", this);
    private HashSet<SGListener> listeners = new HashSet<>();

    private boolean logChanges = false;

    @Override
    public void onNodeChanged(NodeChangedEvent e) {
        StackTraceElement[] stackTrace;
        String trace;

        switch (e.getType()) {
            case ATTACHED:
                if (logChanges) {
                    stackTrace = Thread.currentThread().getStackTrace();
                    trace = "(" + stackTrace[4].getFileName() + ":"
                            + stackTrace[4].getLineNumber() + ")";
                    System.out.println(e.getChild() + " attached to "
                            + e.getParent() + " - " + trace);
                }
                break;
            case DETACHED:
                if (logChanges) {
                    stackTrace = Thread.currentThread().getStackTrace();
                    trace = "(" + stackTrace[4].getFileName() + ":"
                            + stackTrace[4].getLineNumber() + ")";
                    System.out.println(e.getChild() + " detached from "
                            + e.getParent() + " - " + trace);
                }
                break;
        }

        for (SGListener sl : listeners)
            sl.onGraphChanged(new GraphChangedEvent(this, e));
    }

    public SGNode getRootNode() {
        return rootNode;
    }

    public void addScenegraphListener(SGListener listener) {
        listeners.add(listener);
    }

    public void removeListener(SGListener listener) {
        listeners.remove(listener);
    }

    public HashSet<SGListener> getListeners() {
        return listeners;
    }
}
