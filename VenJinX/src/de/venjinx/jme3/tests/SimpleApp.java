package de.venjinx.jme3.tests;

import com.jme3.app.SimpleApplication;
import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;

//import de.venjinx.editor.gui.debug.DebugState;

public class SimpleApp extends SimpleApplication {

    public SimpleApp() {
        showSettings = false;
    }

    @Override
    public void simpleInitApp() {
//        DebugState debug = new DebugState();
//        stateManager.attach(debug);

        Box b = new Box(new Vector3f(0, 0, 0), new Vector3f(1, 1, 1));
        Geometry g = new Geometry("", b);
        g.setMaterial(new Material(assetManager, "Common/MatDefs/Misc/ShowNormals.j3md"));
        rootNode.attachChild(g);
    }

    public static void main(String[] args) {
        SimpleApp app = new SimpleApp();
        app.start();
    }

}
