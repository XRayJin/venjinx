package de.venjinx.jme3;

import java.util.HashMap;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

import de.venjinx.core.HermiteData;
import de.venjinx.core.VoxelChunk;
import de.venjinx.core.VoxelObject;
import de.venjinx.core.VoxelWorld.VoxelMode;
import de.venjinx.jme3.scenegraph.SGNode;

public class VoxelObjectNode extends SGNode {

    private VoxelObject voxelObject;
    int chunksX, chunksY, chunksZ;
    private VoxelChunkNode[][][] chunks;

    private Material material;
    private SGNode geometryNode = new SGNode("VoxelGeometryNode");

    private Vector3f currentRefPoint = new Vector3f();

    static ScheduledThreadPoolExecutor threadPool = new ScheduledThreadPoolExecutor(1);
    private HashMap<VoxelChunkNode, Future<VoxelChunkNode>> futureChunks = new HashMap<>();

    public VoxelObjectNode(HermiteData hData) {
        this(new VoxelObject(hData));
    }

    private VoxelObjectNode(VoxelObject vObject) {
        super("VoxelObjectNode");
        attachChild(geometryNode);

        VoxelChunk[][][] voChunks = vObject.getChunks();
        chunksX = voChunks.length;
        chunksY = voChunks[0].length;
        chunksZ = voChunks[0][0].length;
        chunks = new VoxelChunkNode[chunksX][chunksY][chunksZ];
        for (int x = 0; x < chunksX; x++)
            for (int y = 0; y < chunksY; y++)
                for (int z = 0; z < chunksZ; z++) {
                    chunks[x][y][z] = new VoxelChunkNode(voChunks[x][y][z]);
                    chunks[x][y][z].setMaterial(material);
                    geometryNode.attachChild(chunks[x][y][z]);
                }

        voxelObject = vObject;
    }

    public void update(Vector3f refPoint) {
        currentRefPoint.set(refPoint);
        voxelObject.update(refPoint.x, refPoint.y, refPoint.z);

        updateChunks();
    }

    private void updateChunks() {
        VoxelChunkNode chunk;
        Future<VoxelChunkNode> futureChunk;
        for (int x = 0; x < chunksX; x++)
            for (int y = 0; y < chunksY; y++)
                for (int z = 0; z < chunksZ; z++) {
                    chunk = chunks[x][y][z];
                    if (chunk.needsGeneration()) {
                        if (!futureChunks.containsKey(chunk))
                            futureChunks.put(chunk, threadPool.submit(chunk));
                    } else {
                        futureChunk = futureChunks.get(chunk);
                        if (futureChunk != null && futureChunk.isDone()) {
                            futureChunks.remove(chunk);
                            if (!futureChunk.isCancelled())
                                chunk.switchGeometries();

                            if (voxelObject.getMode() == VoxelMode.LINE_2D)
                                setLineMode(chunk, Mesh.Mode.Lines);
                            else setLineMode(chunk, Mesh.Mode.Triangles);
                        }
                    }
                }
    }

    public void setMode(VoxelMode mode) {
        voxelObject.setMode(mode);
    }

    private void setLineMode(Node n, Mesh.Mode mode) {
        for (Spatial s : n.getChildren())
            if (s instanceof Node)
                setLineMode((Node) s, mode);
            else
                if (s instanceof Geometry)
                    ((Geometry) s).getMesh().setMode(mode);
    }

    public void showChunkLODs() {
        for (int x = 0; x < chunksX; x++)
            for (int y = 0; y < chunksY; y++)
                for (int z = 0; z < chunksZ; z++)
                    chunks[x][y][z].showLOD();
        setMaterial(material);
    }

    public void showChunkBounds() {
        for (int x = 0; x < chunksX; x++)
            for (int y = 0; y < chunksY; y++)
                for (int z = 0; z < chunksZ; z++)
                    chunks[x][y][z].showBound();
    }

    @Override
    public void setMaterial(Material mat) {
        material = mat;
        super.setMaterial(mat);
    }

    public Material getMaterial() {
        return material;
    }

    public VoxelObject getVoxelObject() {
        return voxelObject;
    }

    public HermiteData getData() {
        return voxelObject.getData();
    }

    public VoxelChunkNode[][][] getChunks() {
        return chunks;
    }
}