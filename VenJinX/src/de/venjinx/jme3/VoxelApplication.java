package de.venjinx.jme3;

import java.util.logging.*;

import com.jme3.app.*;
import com.jme3.app.state.*;
import com.jme3.font.BitmapFont;
import com.jme3.system.AppSettings;

public abstract class VoxelApplication extends SimpleApplication {

    private Logger log = Logger.getLogger(VoxelApplication.class.getName());

    //    protected Scenegraph scenegraph = new Scenegraph();
    protected VoxelState voxelState = new VoxelState();

    public VoxelApplication () {
        this(null);
    }

    public VoxelApplication(AppState... initialStates) {
        // Logger.getLogger("").setLevel(Level.OFF);

        log.setLevel(Level.INFO);

        settings = new AppSettings(false);
        settings.setAlphaBits(0);
        settings.setAudioRenderer("LWJGL");
        settings.setBitsPerPixel(24);
        settings.setDepthBits(24);
        settings.setEmulateMouse(false);                                        // use touch device
        settings.setEmulateMouseFlipAxis(false, false);
        settings.setFrameRate(-1);
        settings.setFrequency(60);
        settings.setFullscreen(false);
        settings.setIcons(null);
        settings.setRenderer(AppSettings.LWJGL_OPENGL_ANY);
        settings.setResolution(800, 600);
        settings.setSamples(0);
        settings.setSettingsDialogImage("/com/jme3/app/Monkey.png");
        settings.setStencilBits(0);
        settings.setStereo3D(false);
        settings.setTitle("Voxel Application");
        settings.setUseInput(true);
        settings.setUseJoysticks(false);                                        // use joystick
        settings.setVSync(false);
        showSettings = false;

        setPauseOnLostFocus(false);

        stateManager = new AppStateManager(this);
        stateManager.attach(new ResetStatsState());
        stateManager.attach(voxelState);

        if (initialStates != null)
            for (AppState a : initialStates)
                if (a != null)
                    stateManager.attach(a);

        //        rootNode = scenegraph.getRootNode();
    }

    //    /**
    //     * Retrieves the scenegraph for of this application.
    //     *
    //     * @return the scenegraph that manages all scene nodes in this application
    //     */
    //    public Scenegraph getScenegraph() {
    //        return scenegraph;
    //    }

    public BitmapFont getGUIFont() {
        return guiFont;
    }

    @Override
    public void start() {
        super.start();
        log.info("----------------Application started----------------\n");
    }

    @Override
    public void stop() {
        super.stop();
        System.out.println();
        log.info("----------------Application stopped----------------\n");
    }

    /**
     * Internal use only.
     */
    @Override
    public void requestClose(boolean esc) {
        context.destroy(false);
        System.out.println();
        log.info("----------------Application closed----------------\n");
    }
}
