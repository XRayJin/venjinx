package de.venjinx.jme3.debug;

import com.jme3.input.FlyByCamera;
import com.jme3.input.InputManager;
import com.jme3.renderer.Camera;

import de.venjinx.util.Keys;

public class DebugCam extends FlyByCamera {

    public static String[] mappings = { Keys.CAM_LOOK_UP,
                                        Keys.CAM_LOOK_LEFT,
                                        Keys.CAM_LOOK_DOWN,
                                        Keys.CAM_LOOK_RIGHT,
                                        Keys.CAM_MOVE_LEFT,
                                        Keys.CAM_MOVE_RIGHT,
                                        Keys.CAM_MOVE_FORWARD,
                                        Keys.CAM_MOVE_BACKWARD,
                                        Keys.CAM_ZOOM_IN,
                                        Keys.CAM_ZOOM_OUT,
                                        Keys.CAM_RISE,
                                        Keys.CAM_LOWER,
                                        Keys.CAM_DRAG_ROTATE,
                                        Keys.CAM_INVERTY };

    public DebugCam(Camera cam) {
        super(cam);
        moveSpeed = 5f;
    }

    @Override
    public void onAnalog(String name, float value, float tpf) {
        if (!enabled)
            return;

        if (name.equals(Keys.CAM_LOOK_LEFT)) {
            rotateCamera(value, initialUpVec);
            return;
        }
        if (name.equals(Keys.CAM_LOOK_RIGHT)) {
            rotateCamera(-value, initialUpVec);
            return;
        }
        if (name.equals(Keys.CAM_LOOK_UP)) {
            rotateCamera(-value * (invertY ? -1 : 1), cam.getLeft());
            return;
        }
        if (name.equals(Keys.CAM_LOOK_DOWN)) {
            rotateCamera(value * (invertY ? -1 : 1), cam.getLeft());
            return;
        }
        if (name.equals(Keys.CAM_MOVE_FORWARD)) {
            moveCamera(value, false);
            return;
        }
        if (name.equals(Keys.CAM_MOVE_BACKWARD)) {
            moveCamera(-value, false);
            return;
        }
        if (name.equals(Keys.CAM_MOVE_LEFT)) {
            moveCamera(value, true);
            return;
        }
        if (name.equals(Keys.CAM_MOVE_RIGHT)) {
            moveCamera(-value, true);
            return;
        }
        if (name.equals(Keys.CAM_RISE)) {
            riseCamera(value);
            return;
        }
        if (name.equals(Keys.CAM_LOWER)) {
            riseCamera(-value);
            return;
        }
        if (name.equals(Keys.CAM_ZOOM_IN)) {
            zoomCamera(-value * 10);
            return;
        }
        if (name.equals(Keys.CAM_ZOOM_OUT)) {
            zoomCamera(value * 10);
            return;
        }
    }

    @Override
    public void onAction(String name, boolean value, float tpf) {
        if (!enabled)
            return;

        if (name.equals(Keys.CAM_DRAG_ROTATE) && dragToRotate) {
            canRotate = value;
            inputManager.setCursorVisible(!value);
        }

        if (name.equals(Keys.CAM_INVERTY))
            if (!value)
                invertY = !invertY;
    }

    @Override
    public void registerWithInput(InputManager inputManager) {
        this.inputManager = inputManager;
        // WASD movement
        inputManager.addMapping(Keys.CAM_MOVE_LEFT, Keys.KEY_A);
        inputManager.addMapping(Keys.CAM_MOVE_RIGHT, Keys.KEY_D);
        inputManager.addMapping(Keys.CAM_MOVE_FORWARD, Keys.KEY_W);
        inputManager.addMapping(Keys.CAM_MOVE_BACKWARD, Keys.KEY_S);

        // mouse look
        inputManager.addMapping(Keys.CAM_LOOK_UP, Keys.MOUSE_UP);
        inputManager.addMapping(Keys.CAM_LOOK_LEFT, Keys.MOUSE_LEFT);
        inputManager.addMapping(Keys.CAM_LOOK_DOWN, Keys.MOUSE_DOWN);
        inputManager.addMapping(Keys.CAM_LOOK_RIGHT, Keys.MOUSE_RIGHT);

        // space/left-shift for rise/lower
        inputManager.addMapping(Keys.CAM_RISE, Keys.KEY_SPACE);
        inputManager.addMapping(Keys.CAM_LOWER, Keys.KEY_LSHIFT);

        // zoom in/out with wheel
        inputManager.addMapping(Keys.CAM_ZOOM_IN, Keys.WHEEL_UP);
        inputManager.addMapping(Keys.CAM_ZOOM_OUT, Keys.WHEEL_DOWN);

        // left-click-drag to rotate camera
        inputManager.addMapping(Keys.CAM_DRAG_ROTATE, Keys.L_MOUSE);

        inputManager.addListener(this, mappings);
    }
}
