package de.venjinx.jme3.debug;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapFont;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.control.BillboardControl;

import de.venjinx.util.Materials;

public class DebugMaterials {

    public static AssetManager assetManager;

    public static Material boundMat;
    public static Material normalMat;
    public static Material tangentMat;
    public static Material binormalMat;
    public static Material wireMat;
    public static Material whiteMat;
    public static Material blackMat;
    public static Material lod0Mat;
    public static Material lod1Mat;
    public static Material lod2Mat;
    public static Material lod3Mat;
    public static Material lod4Mat;
    public static Material lod5Mat;

    public static BitmapFont guiFont;
    public static BillboardControl bc = new BillboardControl();

    public static void setAssetManager(AssetManager am) {
        if (assetManager == null) {
            assetManager = am;

            boundMat = new Material(am, Materials.UNSHADED);
            boundMat.setColor("Color", ColorRGBA.Orange);

            normalMat = new Material(am, Materials.UNSHADED);
            normalMat.setColor("Color", ColorRGBA.Green);

            tangentMat = new Material(am, Materials.UNSHADED);
            tangentMat.setColor("Color", ColorRGBA.Red);

            binormalMat = new Material(am, Materials.UNSHADED);
            binormalMat.setColor("Color", ColorRGBA.Blue);

            wireMat = new Material(am, Materials.UNSHADED);
            wireMat.setColor("Color", ColorRGBA.Magenta);

            whiteMat = new Material(am, Materials.UNSHADED);
            whiteMat.setColor("Color", ColorRGBA.White);

            blackMat = new Material(am, Materials.UNSHADED);
            blackMat.setColor("Color", ColorRGBA.Black);

            lod0Mat = new Material(am, Materials.UNSHADED);
            lod0Mat.setColor("Color", ColorRGBA.Black);

            lod1Mat = new Material(am, Materials.UNSHADED);
            lod1Mat.setColor("Color", ColorRGBA.Blue);

            lod2Mat = new Material(am, Materials.UNSHADED);
            lod2Mat.setColor("Color", ColorRGBA.Cyan);

            lod3Mat = new Material(am, Materials.UNSHADED);
            lod3Mat.setColor("Color", ColorRGBA.Green);

            lod4Mat = new Material(am, Materials.UNSHADED);
            lod4Mat.setColor("Color", ColorRGBA.Yellow);

            lod5Mat = new Material(am, Materials.UNSHADED);
            lod5Mat.setColor("Color", ColorRGBA.Red);

            guiFont = am.loadFont("Interface/Fonts/Default.fnt");
        }
    }

    public static Material getRandomMaterial() {
        Material mat = new Material(assetManager, Materials.UNSHADED);
        mat.setColor("Color", ColorRGBA.randomColor());

        return mat;
    }
}
