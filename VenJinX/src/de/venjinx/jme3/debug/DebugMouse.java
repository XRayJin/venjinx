package de.venjinx.jme3.debug;

import org.lwjgl.opengl.Display;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.material.Material;
import com.jme3.math.Ray;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;

import de.venjinx.util.Util;

public class DebugMouse extends AbstractAppState {

    private SimpleApplication simpleApp;

    private Ray mouseRay = new Ray();
    private Vector2f mouse2D = new Vector2f();
    private Vector3f mouse3D = new Vector3f();
    private Vector3f hitPoint = new Vector3f();
    private CollisionResults results = new CollisionResults();
    private CollisionResult mouseHitResult;
    private Camera cam;
    private Node mouse3DNode = new Node("3D mouse");
    private Geometry cursor3D;

    private boolean show3DCursor = true;

    public DebugMouse() {
        cursor3D = Util.createPoint(0, 0, 0, .05f);
        cursor3D.setName("3D mouse cursor");

        mouse3DNode.attachChild(cursor3D);
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        simpleApp = (SimpleApplication) app;
        cam = simpleApp.getCamera();
    }

    @Override
    public void update(float tpf) {
        if (!simpleApp.getInputManager().isCursorVisible())
            mouse2D.set(Display.getWidth() / 2, Display.getHeight() / 2);
        else mouse2D.set(simpleApp.getInputManager().getCursorPosition());

        mouse3D.set(cam.getWorldCoordinates(mouse2D, 0f));

        mouseRay.setOrigin(mouse3D);
        mouseRay.setDirection(mouse3D.subtract(cam.getLocation()).normalizeLocal());

        results.clear();

        mouse3DNode.detachChild(cursor3D);
        simpleApp.getRootNode().collideWith(mouseRay, results);
        if (show3DCursor)
            mouse3DNode.attachChild(cursor3D);
        mouseHitResult = results.getClosestCollision();

        if (mouseHitResult != null)
            hitPoint.set(mouseHitResult.getContactPoint());
        else hitPoint.set(mouse3D.add(mouseRay.direction));
        cursor3D.setLocalTranslation(hitPoint);
    }

    public void setShowPointer(boolean show) {
        show3DCursor = show;
        if (show)
            mouse3DNode.attachChild(cursor3D);
        else mouse3DNode.detachChild(cursor3D);
    }

    public void setMaterial(Material mat) {
        if (mat != null)
            cursor3D.setMaterial(mat);
    }

    public Vector2f get2D() {
        return mouse2D;
    }

    public Vector3f get3D() {
        return mouse3D;
    }

    public Vector3f getHitPoint() {
        return hitPoint;
    }

    public Vector3f getDirection() {
        return mouseRay.direction;
    }

    public Ray getRay() {
        return mouseRay;
    }

    public CollisionResult getHitResult() {
        return mouseHitResult;
    }

    public CollisionResult getHitResult(Node nodeToCollide) {
        results.clear();
        nodeToCollide.collideWith(mouseRay, results);
        return results.getClosestCollision();
    }

    public Geometry getHitGeometry() {
        if (mouseHitResult != null)
            return mouseHitResult.getGeometry();
        return null;
    }

    public Node getPointer() {
        return mouse3DNode;
    }
}
