package de.venjinx.jme3.debug;

import org.lwjgl.opengl.Display;

import com.jme3.app.SimpleApplication;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.input.InputManager;
import com.jme3.material.Material;
import com.jme3.math.Ray;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;

import de.venjinx.util.Util;

public class Mouse3D {

    private static Node rootNode;
    private static InputManager im;
    private static Camera cam;

    private static Vector2f mouse2D = new Vector2f();
    private static Vector3f mouse3D = new Vector3f();
    private static Ray mouseRay = new Ray();
    private static CollisionResults results = new CollisionResults();
    private static Node mouse3DNode = new Node("3D mouse");
    private static Geometry cursor3D = Util.createPoint(0, 0, 0, .05f);

    private static boolean show3DCursor = true;

    public static void setApp(SimpleApplication app, boolean showCursor) {
        rootNode = app.getRootNode();
        im = app.getInputManager();
        cam = app.getCamera();

        setShowPointer(showCursor);
    }

    public static void setShowPointer(boolean show) {
        show3DCursor = show;
        if (show)
            mouse3DNode.attachChild(cursor3D);
        else mouse3DNode.detachChild(cursor3D);
    }

    public static void setMaterial(Material mat) {
        if (mat != null)
            cursor3D.setMaterial(mat);
    }

    public static Vector3f getDirection() {
        return get3D().subtract(cam.getLocation()).normalizeLocal();
    }

    public static Node getPointer() {
        return mouse3DNode;
    }

    public static Vector2f get2D() {
        if (!im.isCursorVisible())
            mouse2D.set(Display.getWidth() / 2, Display.getHeight() / 2);
        else mouse2D.set(im.getCursorPosition());

        return mouse2D;
    }

    public static Vector3f get3D() {
        return mouse3D.set(cam.getWorldCoordinates(get2D(), 0f));
    }

    public static CollisionResult getHitResult() {
        mouseRay.setOrigin(get3D());
        mouseRay.setDirection(mouse3D.subtract(cam.getLocation()).normalizeLocal());

        results.clear();

        mouse3DNode.detachChild(cursor3D);
        rootNode.collideWith(mouseRay, results);
        if (show3DCursor)
            mouse3DNode.attachChild(cursor3D);

        return results.getClosestCollision();
    }

    public static Geometry getHitGeometry() {
        CollisionResult result = getHitResult();
        if (result != null)
            return result.getGeometry();
        return null;
    }

    public static Vector3f getHitPoint() {
        CollisionResult result = getHitResult();
        if (result != null)
            return result.getContactPoint();
        return mouse3D.add(mouseRay.direction);
    }
}
